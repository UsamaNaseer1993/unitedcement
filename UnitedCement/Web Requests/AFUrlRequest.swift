//
//  AFUrlRequest.swift
//  Skeleton
//
//  Created by Waqas Ali on 04/09/2016.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//


import Alamofire

public enum AFUrlRequest: URLRequestConvertible {
    
    
    static let baseURLPath = Constants.serverPathConstants.baseURL

    case allProducts()
    case trucks()
    case register( email: String,  pass: String,  firstname: String,lastname: String, companyName: String,mobilenumber : String)
    case signin(email:String,password:String)
    case countries()
    case forgotPassword(email:String)
    case userVerification (email: String, password: String, code: String)
    case updatepass (email:String, oldpassword: String, newpass: String)
    case updateprofile (firstname:String, lastname: String, mobile: String, email: String, companyname: String)
    case resetpass (email: String, code: String, pass: String)
    case bannerImages()
    case addresses()
    case createAddress(latitude: Double, longitude: Double, addressDesc: String,addressDetail: String, locationName: String,city : String, country: String, countryId : Int)
    case addtoCart(id: Int, quantity: Int, bags: Int,city:String)
    case shoppingCart(shippingId: String)
    case editCart(productId: Int, quantity: Int, bags: Int, city: String)
    case redeemCoupon(coupon: String)
    case couponList()
    case allOffers()
    case addtoCartOffer(id: Int,min_quantity: Int, discount_quantity: Int, quantity: Int,bags: Int,city:String)
    case checkout(addressId: Int, type: String, shippingId: Int, deliverydate: String, status: Int, orderCity: String)
    case orderList(id: Int)
    case deleteFromCart(id: Int)
    case getProfile()
    case orderDetail(id : Int)
    case bankList()
    case bankData(orderId: Int, paymentId: Int,companybank: String, customerbank: String, customerAcc: String, transferDate: String, amountTransfer: Double, paymentRef: String)
    case paymentStatus(orderNumber : Int)
    case logout()
    case deleteAdd(addId: Int)
    case emptyCart()
    

    public func asURLRequest() throws -> URLRequest {
        
        let result: (path: String, method: HTTPMethod, parameters: [String: AnyObject]?, encodeType : ParameterEncoding, header: HTTPHeaders? ) = {
           switch self {
           case .deleteAdd(let addId):
            let params = [
                "address_id" : addId,
                ] as [String:Any]
            return (Constants.deleteAdd, .post,params as [String: AnyObject],URLEncoding.default,nil)
           case .paymentStatus(let orderNumber):
            let params = [
                "order_number" : orderNumber
                ] as [String:Any]
            return (Constants.paymentStatus, .post,params as [String: AnyObject],URLEncoding.default,nil)
           case .emptyCart():
            return (Constants.emptyCart,.post,nil,URLEncoding.default,nil)
           case .trucks():
             return (Constants.trucks,.get,nil,URLEncoding.default,nil)
           case .bankList():
            return (Constants.bankList,.get,nil,URLEncoding.default,nil)
           case .orderDetail(let id):
            let params = [
                "id" : id
            ] as [String:Any]
            return (Constants.orderDetail,.get,params as [String:AnyObject],URLEncoding.queryString,nil)
           case .getProfile():
            return (Constants.getProfile,.get,nil,URLEncoding.default,Constants.getHeader)
           case .deleteFromCart(let id):
            let params = [
                "productId" : id
                ] as [String:Any]
            return (Constants.deleteFromCart,.post,params as [String:AnyObject],URLEncoding.default,nil)
           case .logout():
            return (Constants.logout,.get,nil,URLEncoding.default,nil)
           case .orderList(let id):
            let params = [
                "id" : id
            ] as [String:Any]
            return (Constants.orderList,.get,params as [String:AnyObject],URLEncoding.queryString,nil)
           case .checkout(let addressId, let type, let shippingId, let deliverydate, let status, let orderCity):
            let params = [
            "addressId" : addressId,
                "type" : type,
                "shippingId"  : shippingId,
                "deliveryDate" : deliverydate,
                "order_city" : orderCity,
                "status" : status
            ] as [String:Any]
            return (Constants.checkout, .post,params as [String: AnyObject],URLEncoding.default,nil)
           case .addtoCartOffer(let id, let min_quantity, let discount_quantity, let quantity,let bags,let city):
            let params = [
                "id" : id,
                "minimum_quantity" : min_quantity,
                "discount_quantity" : discount_quantity,
                "quantity" : quantity,
                "bags" : bags,
                "city" : city
                ] as [String : Any]
            return (Constants.addtoCartOffer, .post, params as [String : AnyObject],JSONEncoding.default,nil)
           case .allOffers():
            return (Constants.allOffers, .get , nil,URLEncoding.default,nil)
           case .couponList():
            return (Constants.couponList, .get , nil,URLEncoding.default,nil)
           case .redeemCoupon(let coupon):
            let params = [
                "coupon" : coupon
            ] as [String:Any]
            return (Constants.couponRedeem, .post, params as [String: AnyObject],URLEncoding.default,nil)
           case .editCart(let productId, let quantity, let bags,let city):
            let params = [
                "productId" : productId,
                "quantity" : quantity,
                "bags" : bags,
                "city" : city
                ] as [String : Any]
            return (Constants.editCart, .post, params as [String : AnyObject],JSONEncoding.default,nil)
           case .shoppingCart(let shippingId):
           let params = [
                "shippingId"  :  shippingId
           ]
        return (Constants.shoppingCart, .get, params as [String : AnyObject], URLEncoding.queryString,nil)
           case .addtoCart(let id, let quantity, let bags,let city):
            let params = [
                "id" : id,
                "quantity" : quantity,
                "bags" : bags,
                "city" : city
            ] as [String : Any]
            return (Constants.addtoCart, .post, params as [String : AnyObject],JSONEncoding.default,nil)
           case .createAddress(let latitude, let longitude, let addressDesc,let addressDetail ,let locationName, let city, let country, let countryId):
            let params = [
                "latitude" : latitude ,
                "longitude" : longitude,
                "adressDesc"    : addressDesc ,
                "addressDetails" : addressDetail,
                "locationName" : locationName ,
                "city"      : city ,
                "country" : country ,
                "country_id" : countryId]
                as [String : Any]
            return (Constants.addresses, .post , params as  [String : AnyObject],URLEncoding.default,Constants.getHeader)
           case .addresses():
               return (Constants.addresses, .get , nil,URLEncoding.default,nil)
           case .bannerImages():
             return (Constants.bannerImages, .get , nil,URLEncoding.default,nil)
           case .resetpass(let email, let code, let pass):
            let params = [
                "email" : email,
                "code" : code,
                "password" : pass
                ] as [String : AnyObject]
            return (Constants.resetPassword,.post, params as [String : AnyObject],JSONEncoding.default, nil)
           case .updatepass(let email, let oldpassword, let newpass):
            let params = [
                "email" : email,
                "oldpass" : oldpassword,
                "newpass" : newpass
                ] as [String : AnyObject]
            return (Constants.updatePassword,.post, params as [String : AnyObject],JSONEncoding.default, nil)
           case .userVerification(let email, let password , let code):
            let params = [
                "email" : email,
                "password" : password,
                "code" : code
            ] as [String : AnyObject]
            return (Constants.userVerification,.post, params as [String : AnyObject],JSONEncoding.default, nil)
           case .forgotPassword(let email) :
            let params = [
                "email" : email
            ] as [String:AnyObject]
            return (Constants.forgotPassword,.post,params as [String : AnyObject],URLEncoding.default,nil)
            
           case .allProducts() :
            return (Constants.allProducts, .get , nil,URLEncoding.default,nil)
           
           case .countries() :
            return (Constants.countries, .get , nil,URLEncoding.default,nil)
            
           case .signin(let email, let password):
            let params = [
                "email"     : email,
                "password"  : password]
            return (Constants.signin, .post, params as [String : AnyObject], URLEncoding.default, nil )
           case .updateprofile(let firstname, let lastname, let mobile, let email, let companyname):
            let params = [
                "firstname" : firstname ,
                "lastname" : lastname,
                "phone"    : mobile ,
                "company" : companyname ,
                 "email"      : email]
                 as [String : String]
            return (Constants.updateProfile, .post , params as  [String : AnyObject],URLEncoding.default,nil)
           case .register(let email, let pass, let firstname,let lastname, let companyName,let mobilenumber ):
            let params = [
                "email"      : email ,
                "password"   : pass ,
                "first_name" : firstname ,
                "last_name" : lastname,
                "company_name" : companyName ,
                "mobile"    : mobilenumber] as [String : String]
            return (Constants.register, .post , params as  [String : AnyObject],URLEncoding.default,nil)
   
           case .bankData(let orderId, let paymentId, let companybank, let customerbank, let customerAcc, let transferDate, let amountTransfer, let paymentRef):
            let params =    [
                "order_id" : orderId,
                "payment_id"      : paymentId ,
                "company_bank"   : companybank ,
                "customer_bank" : customerbank ,
                "customer_account" : customerAcc,
                "transfer_date" : transferDate ,
                "amount_transfer"    : amountTransfer,
                "payment_reference" : paymentRef ] as [String : Any]
            return (Constants.bankData, .post , params as  [String : AnyObject],URLEncoding.default,nil)
            }
        }()
        
        let url = try AFUrlRequest.baseURLPath.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(result.path))
        urlRequest.httpMethod = result.method.rawValue
        urlRequest.timeoutInterval = TimeInterval(30)
        if let header = result.header{
            urlRequest.setValue(header["Authorization"], forHTTPHeaderField: "Authorization")
        }
        let encoding = result.encodeType
        return try encoding.encode(urlRequest, with: result.parameters)
    }
    
    
}
