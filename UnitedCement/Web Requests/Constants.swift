
//
//  Constants.swift
//  WebserviceTest
//
//  Created by Waqas Ali on 15/06/2016.
//  Copyright © 2016 Traffic Digital. All rights reserved.
//
import UIKit
import Alamofire

struct Constants {
    static let imageUrl = "http://myraal.com/home/"
    struct serverPathConstants {
       static let baseURL    = "https://unitedcement.herokuapp.com/"
    }
    
    static var getHeader : HTTPHeaders?
    {
        get {
            return ["Content-Type": "application/json"]
            
        }
        
    }
       static let MESSAGE = "Message"
    static let MESSAGE_AR = "رسالة"
    static let OK_AR = "حسنا"
    static let CANCEL_AR = "إلغاء"
    
    static let allProducts =  "products/getAllProducts"
    static let register = "users/register"
    static let signin = "users/signin"
    static let countries = "Countries"
    static let forgotPassword = "users/forgot"
    static let userVerification = "users/verification"
    static let updatePassword  = "users/update_password"
    static let updateProfile = "users/accountUpdate"
    static let resetPassword = "users/resetPassword"
    static let bannerImages = "users/bannerImages"
    static let addresses = "users/addresses"
    static let addtoCart = "cart/addtocart"
    static let shoppingCart = "cart/shopping-cart"
    static let editCart = "cart/edit-from-cart"
    static let couponRedeem = "cart/checkCoupon"
    static let couponList = "cart/couponList"
    static let allOffers = "products/offers"
    static let addtoCartOffer = "cart/AddOfferToCart"
    static let checkout = "cart/final-checkout"
    static let orderList = "products/myOrder"
    static let logout = "users/logout"
    static let deleteFromCart = "cart/delete-from-cart"
    static let getProfile = "users/profile"
    static let orderDetail = "products/myOrderDetails"
    static let bankList = "users/banksData"
    static let bankData = "users/bankTransferData"
    static let uploadSlip = "payments/payment-slip"
    static let paymentStatus = "cart/paymentStatus"
    static let trucks = "cart/trucks"
    static let emptyCart = "cart/empty-cart"
    static let deleteAdd = "users/deleteAddress"


    struct language {
        static let LanguageChosen = "chosenLanguage"
        static let englishLanguage = "en"
        static let arabicLanguage = "ar"
        static let urduLanguage = "ur"
        
    }

}


