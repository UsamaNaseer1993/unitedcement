//
//  printLog.swift
//  Base
//
//  Created by Waqas Ali on 06/12/2016.
//  Copyright © 2016 Hamza Khan. All rights reserved.
//

import Foundation

//func printLog(_ arg: Any...) {
//
//     print(arg)
// items.map { "*\($0)" }.joined(separator: separator)
//
//}
let showLog = true

public func print(_ items: Any..., separator: String = " ", terminator: String = "\n") {
    
    if showLog == true {
        let output = items.map { "\($0)" }.joined(separator: separator)
        Swift.print(output, terminator: terminator)
    }
    
}

//let swipeBack = UISwipeGestureRecognizer(target: self, action: #selector(response))
//swipeBack.direction = UISwipeGestureRecognizerDirection.right
//self.view.addGestureRecognizer(swipeBack)
//
//@objc func response(){
//    self.navigationController?.popViewController(animated: true)
//}

