//
//  ApiCalls.swift
//  kikef
//
//  Created by Arsalan on 4/10/17.
//  Copyright © 2017 Invision Custom Solutions Inc. All rights reserved.
//

import UIKit
import SwiftyJSON
import ObjectMapper



struct ApiCall  {
    
    //MARK:- typealias for completion used in services
    typealias compGetAllProducts = (_ statusCode : Int?,_ products : [ProductModel]?,_ bags : [BagModel]? , _ error : NSError?)->()
    typealias compGetAllOffers = (_ statusCode : Int?,_ products : [OfferModel]?,_ bags : [BagModel]? , _ error : NSError?)->()
    typealias compStrErr    = ((_ code:String?,_ error:NSError?)->())?
    typealias compStrErrMessage    = ((_ code:String?,_ message: String? ,_ error:NSError?)->())
    typealias compRegistration = (_ user : UserModel?, _ error : NSError?)->()
    typealias compUserVerification = (_ message : String?,_ error : NSError?)->()
    typealias compGetCountries = (_ code: Int?, _ countries: [CountryCityModel]?, _ error: NSError?) -> ()
    typealias compresetPass = (_ code : Int?, _ message: String?, _ error: NSError?)->()
    typealias compGetBannerImages = (_ images : [ImageModel]?,_ link : String?,_ error: NSError?)->()
    typealias compGetAddresses = (_ addresses: [AddressModel]?,_ logout : Bool?, _ error: NSError?)->()
     typealias compCouponList = (_ coupons: [CouponModel]?,_ logout : Bool?, _ error: NSError?)->()
    typealias compCreateAddress = (_ msg: String?,_ logout : Bool?,_ addressId: Int?, _ error: NSError?)->()
    typealias  compAddToCart = (_ msg: String?,_ logout : Bool?, _ error: NSError?)->()
    typealias  compPaymentStatus = (_ status: Int?, _ error: NSError?)->()
    typealias  compTrucks = (_ trucks: Int?,_ logout : Bool?, _ error: NSError?)->()
    typealias  compCheckout = (_ msg: String?,_ paymentId: Int?,_ orderId  : Int?,_ logout : Bool?, _ error: NSError?)->()
      typealias  compOrderList = (_ orders: [OrderModel]?,_ logout : Bool?, _ error: NSError?)->()
       typealias  compBankList = (_ bankList: [bankDetailModel]?,_ logout : Bool?, _ error: NSError?)->()
    typealias compShoppingCart = (_ cartProducts: [cartProductModel]?,_ logout : Bool?,_ startDate: String,_ endDate: String, _ bags: [BagModel]? ,_ last : (subtotal: NSNumber,Vat: String, total : String,city : String)?,_ error: NSError?)->()
     typealias compOrderDetail = (_ cartProducts: [OrderDetailModel]?,_ logout : Bool?,_ startDate: String,_ endDate: String,_ error: NSError?)->()
    typealias compRedeemCoupon = (_ msg : String?, _ data: Int?,_ logout: Bool? ,_ error: NSError?)->()
    typealias compLogout = (_ msg : String?, _ error: NSError?)->()
    typealias compProfile = (_ Profile: [ProfileModel]?,_ logout: Bool?, _ error: NSError?) -> ()
 
    
    
}

extension  ApiCall {
    static func getProfile(completion : @escaping compProfile) {
        AFWrapper.serviceCall(urlEnum: AFUrlRequest.getProfile(), success: {(result) in
            if result["status"] as? Int == 200 {
            
                completion(ProfileModel.map(profiles: result["data"] as! [NSDictionary]),false,nil)
            } else if result["status"] as? Int == 201 {
                completion(nil,true,nil)
            }
        }, failure: {(error) in
            completion(nil,false,error)
        })
    }
    static func emptyCart(completion : @escaping compLogout) {
        AFWrapper.serviceCall(urlEnum: AFUrlRequest.emptyCart(), success: {(result) in
            if result["status"] as? Int == 200 {
                
                completion((result["message"] as! String),nil)
            }
            else {
                completion((result["message"] as! String),nil)
            }
        }, failure: {(error) in
            completion(nil,error)
        })
    }
    
    static func getAllProducts(completion : @escaping compGetAllProducts) {
        AFWrapper.serviceCall(urlEnum: AFUrlRequest.allProducts(), success: {(result) in
            if result["status"] as? Int == 200 {
                let res = result["data"] as! NSDictionary
                    completion(200,ProductModel.map(products: res["products"] as! [NSDictionary]),BagModel.map(bags: res["bags"] as! [NSDictionary]),nil)
            }
            else {
                completion(500,nil,nil,nil)
            }
        }, failure: {(error) in
            completion(nil,nil,nil,error)
        })
    }
    static func logout(completion : @escaping compLogout) {
        AFWrapper.serviceCall(urlEnum: AFUrlRequest.logout(), success: {(result) in
            if result["status"] as? Int == 200 {
               
                completion((result["message"] as! String),nil)
            }
            else {
                completion((result["message"] as! String),nil)
            }
        }, failure: {(error) in
            completion(nil,error)
        })
    }
    static func getAllOffers(completion : @escaping compGetAllOffers) {
        AFWrapper.serviceCall(urlEnum: AFUrlRequest.allOffers(), success: {(result) in
            if result["status"] as? Int == 200 {
              //  let res = result["data"] as! NSDictionary
                if result["message"] as! String != "Currently No Offer is Available" {
                      completion(200,OfferModel.map(offers: result["productWise"] as! [[NSDictionary]]),BagModel.map(bags: result["bags"] as! [NSDictionary]),nil)
                } else {
                    if let topController = UIApplication.shared.keyWindow?.rootViewController {
                        topController.showAlert(title: Constants.MESSAGE, message: (result["message"] as! String))
                    }

                }
              
            }
            else {
                completion(500,nil,nil,nil)
            }
        }, failure: {(error) in
            completion(nil,nil,nil,error)
        })
    }
    static func createAddresses(latitude: Double, longitude: Double, addressDesc: String,addressDetail : String ,locationName: String, city: String, country: String, countryId: Int, completion : @escaping compCreateAddress) {
        AFWrapper.serviceCall(urlEnum: AFUrlRequest.createAddress(latitude: latitude, longitude: longitude, addressDesc: addressDesc,addressDetail: addressDetail ,locationName: locationName, city: city, country: country, countryId: countryId), success: {(result) in
            if result["status"] as? Int == 200 {
                if result["message"] as? String == "User must be logged in" {
                    completion(nil,true,nil,nil)
                } else {
                   completion(result["message"] as? String,false,result["address_id"] as? Int,nil)
                }
            }  else if result["status"] as? Int == 201 {
                if result["message"] as? String == "User must be logged in" {
                    completion(nil,true,nil,nil)
                }
            }
            else {
                if let topController = UIApplication.shared.keyWindow?.rootViewController {
                    topController.showAlert(title: Constants.MESSAGE, message: (result["message"] as! String))
                }
            }
        }, failure: {(error) in
            completion(nil,false,nil,error)
        })
    }
    static func addtoCart(id: Int, quantity: Int, bags: Int,city :String, completion : @escaping compAddToCart) {
        AFWrapper.serviceCall(urlEnum: AFUrlRequest.addtoCart(id: id, quantity: quantity, bags: bags,city: city), success: {(result) in
            if result["status"] as? Int == 200 {
                if result["message"] as? String == "User must be logged in" {
                    completion(nil,true,nil)
                } else {
                    completion(result["message"] as? String,false,nil)
                }
            } else if result["status"] as? Int == 201 {
                if result["message"] as? String == "User must be logged in" {
                    completion(nil,true,nil)
                }
            } else {
                if let topController = UIApplication.shared.keyWindow?.rootViewController {
                    topController.showAlert(title: Constants.MESSAGE, message: (result["message"] as! String))
                }
            }
        }, failure: {(error) in
            completion(nil,false,error)
        })
    }
    static func deleteFromCart(id: Int, completion : @escaping compAddToCart) {
        AFWrapper.serviceCall(urlEnum: AFUrlRequest.deleteFromCart(id: id), success: {(result) in
            if result["status"] as? Int == 200 {
                if result["message"] as? String == "User must be logged in" {
                    completion(nil,true,nil)
                } else {
                    completion(result["message"] as? String,false,nil)
                }
            } else if result["status"] as? Int == 201 {
                if result["message"] as? String == "User must be logged in" {
                    completion(nil,true,nil)
                }
            } else {
                if let topController = UIApplication.shared.keyWindow?.rootViewController {
                    topController.showAlert(title: Constants.MESSAGE, message: (result["message"] as! String))
                }
            }
        }, failure: {(error) in
            completion(nil,false,error)
        })
    }
    static func addtoCartOffer(id: Int, min_quantity: Int, discount_quantity: Int,quantity: Int,bags:Int,city:String, completion : @escaping compAddToCart) {
        AFWrapper.serviceCall(urlEnum: AFUrlRequest.addtoCartOffer(id: id, min_quantity: min_quantity, discount_quantity: discount_quantity, quantity: quantity,bags: bags,city: city), success: {(result) in
            if result["status"] as? Int == 200 {
                if result["message"] as? String == "User must be logged in" {
                    completion(nil,true,nil)
                } else {
                    completion(result["message"] as? String,false,nil)
                }
            } else if result["status"] as? Int == 201 {
                if result["message"] as? String == "User must be logged in" {
                    completion(nil,true,nil)
                }
            } else {
                if let topController = UIApplication.shared.keyWindow?.rootViewController {
                    topController.showAlert(title: Constants.MESSAGE, message: (result["message"] as! String))
                }
            }
        }, failure: {(error) in
            completion(nil,false,error)
        })
    }
    static func checkout(addressId: Int, type: String, shippingId: Int,deliveryDate: String,status: Int,orderCity: String, completion : @escaping compCheckout) {
        AFWrapper.serviceCall(urlEnum: AFUrlRequest.checkout(addressId: addressId, type: type, shippingId: shippingId, deliverydate: deliveryDate,status: status,orderCity: orderCity), success: {(result) in
            
            if result["status"] as? Int == 200 {
                if result["message"] as? String == "User must be logged in" {
                    completion(nil,nil,nil,true,nil)
                } else {
                    completion(result["message"] as? String,result["payment_id"] as? Int,result["order_id"] as? Int,false,nil)
                }
            } else if result["status"] as? Int == 201 {
                if result["message"] as? String == "User must be logged in" {
                    completion(nil,nil,nil,true,nil)
                }
            } else {
                completion(result["message"] as? String,nil,nil,false,nil)

//                if let topController = UIApplication.shared.keyWindow?.rootViewController {
//                    topController.showAlert(title: Constants.MESSAGE, message: (result["message"] as! String))
//                }
                
            }
        }, failure: {(error) in
            completion(nil,nil,nil,false,error)
        })
    }
    static func orderList(userId: Int, completion : @escaping compOrderList) {
        AFWrapper.serviceCall(urlEnum: AFUrlRequest.orderList(id: userId), success: {(result) in
            if result["status"] as? Int == 200 {
                if result["message"] as? String == "User must be logged in" {
                    completion(nil,true,nil)
                } else if result["message"] as? String == "No previous orders" {
                    if let topController = UIApplication.shared.keyWindow?.rootViewController {
                        topController.showAlert(title: Constants.MESSAGE, message: (result["message"] as! String))
                    }
                }
                else {
                    completion(OrderModel.map(orders: (result["data"] as? [NSDictionary])!),false,nil)
                }
            } else if result["status"] as? Int == 201 {
                if result["message"] as? String == "User must be logged in" {
                    completion(nil,true,nil)
                }
            } else {
                if let topController = UIApplication.shared.keyWindow?.rootViewController {
                    topController.showAlert(title: Constants.MESSAGE, message: (result["message"] as! String))
                }
            }
        }, failure: {(error) in
            completion(nil,false,error)
        })
    }
    static func bankList(completion : @escaping compBankList) {
        AFWrapper.serviceCall(urlEnum: AFUrlRequest.bankList(), success: {(result) in
            if result["status"] as? Int == 200 {
                completion(bankDetailModel.map(orders: (result["data"] as? [[String:Any]])!),false,nil)
            } else if result["status"] as? Int == 201 {
                if result["message"] as? String == "User must be logged in" {
                    completion(nil,true,nil)
                }
            } else {
                if let topController = UIApplication.shared.keyWindow?.rootViewController {
                    topController.showAlert(title: Constants.MESSAGE, message: (result["message"] as! String))
                }
            }
        }, failure: {(error) in
            completion(nil,false,error)
        })
    }
    static func paidSlip(image: UIImage ,paymentId : String, completion : @escaping (Int?,NSError?)->()){
        AFWrapper.requestUploadImageWithParamsAndToken(image, fileName: "image", params: ["payment_id":paymentId], url: "\(Constants.serverPathConstants.baseURL)\(Constants.uploadSlip)", success: { (result) in
            completion(result["status"] as! Int,nil)
        }) { (error) in
          
        }
    }
    static func trucks(completion : @escaping compTrucks) {
        AFWrapper.serviceCall(urlEnum: AFUrlRequest.trucks(), success: {(result) in
            if result["status"] as? Int == 200 {
                if result["message"] as? String == "User must be logged in" {
                    completion(nil,true,nil)
                } else {
                    completion(result["trucks"] as? Int,false,nil)
                }
            }  else if result["status"] as? Int == 201 {
                if result["message"] as? String == "User must be logged in" {
                    completion(nil,true,nil)
                }
            }
            else {
                if let topController = UIApplication.shared.keyWindow?.rootViewController {
                    topController.showAlert(title: Constants.MESSAGE, message: (result["message"] as! String))
                }
            }
        }, failure: {(error) in
            completion(nil,false,error)
        })
    }
    static func editCart(id: Int, quantity: Int, bags: Int,city:String, completion : @escaping compAddToCart) {
        AFWrapper.serviceCall(urlEnum: AFUrlRequest.editCart(productId: id, quantity: quantity, bags: bags,city: city), success: {(result) in
            if result["status"] as? Int == 200 {
                if result["message"] as? String == "User must be logged in" {
                    completion(nil,true,nil)
                } else {
                    completion(result["message"] as? String,false,nil)
                }
            }  else if result["status"] as? Int == 201 {
                if result["message"] as? String == "User must be logged in" {
                    completion(nil,true,nil)
                }
            }
            else {
                if let topController = UIApplication.shared.keyWindow?.rootViewController {
                    topController.showAlert(title: Constants.MESSAGE, message: (result["message"] as! String))
                }
            }
        }, failure: {(error) in
            completion(nil,false,error)
        })
    }
    static func paymentStatus(orderNumber: Int, completion : @escaping compPaymentStatus) {
        AFWrapper.serviceCall(urlEnum: AFUrlRequest.paymentStatus(orderNumber: orderNumber), success: {(result) in
            if result["status"] as? Int == 200 {
               completion(200,nil)
            }  else if result["status"] as? Int == 201 {
               completion(201,nil)
            }
            else {
                if let topController = UIApplication.shared.keyWindow?.rootViewController {
                    topController.showAlert(title: Constants.MESSAGE, message: (result["message"] as! String))
                }
            }
        }, failure: {(error) in
            completion(nil,error)
        })
    }
    static func deleteAdd(addId: Int, completion : @escaping compPaymentStatus) {
        AFWrapper.serviceCall(urlEnum: AFUrlRequest.deleteAdd(addId: addId), success: {(result) in
            if result["status"] as? Int == 200 {
                completion(200,nil)
            }  else if result["status"] as? Int == 201 {
                completion(201,nil)
            }
            else {
                if let topController = UIApplication.shared.keyWindow?.rootViewController {
                    topController.showAlert(title: Constants.MESSAGE, message: (result["message"] as! String))
                }
            }
        }, failure: {(error) in
            completion(nil,error)
        })
    }
    static func bankData(orderId: Int,id: Int, companybank: String, customerBank: String,custAcc:String,transferDate: String,amountTransfer: Double, paymentRef :String , completion : @escaping compAddToCart) {
        AFWrapper.serviceCall(urlEnum: AFUrlRequest.bankData(orderId: orderId, paymentId: id, companybank: companybank, customerbank: customerBank, customerAcc: custAcc, transferDate: transferDate, amountTransfer: amountTransfer, paymentRef: paymentRef), success: {(result) in
            if result["status"] as? Int == 200 {
                if result["message"] as? String == "User must be logged in" {
                    completion(nil,true,nil)
                } else {
                    completion(result["message"] as? String,false,nil)
                }
            }  else if result["status"] as? Int == 201 {
                if result["message"] as? String == "User must be logged in" {
                    completion(nil,true,nil)
                }
            }
            else {
                if let topController = UIApplication.shared.keyWindow?.rootViewController {
                    topController.showAlert(title: Constants.MESSAGE, message: (result["message"] as! String))
                }
            }
        }, failure: {(error) in
            completion(nil,false,error)
        })
    }
    static func redeemCoupon(coupon: String, completion : @escaping compRedeemCoupon) {
        AFWrapper.serviceCall(urlEnum: AFUrlRequest.redeemCoupon(coupon: coupon), success: {(result) in
            if result["status"] as? Int == 200 {
                if result["message"] as? String == "User must be logged in" {
                    completion(nil,nil,true,nil)
                } else {
                    completion(result["message"] as? String,result["data"] as? Int,false,nil)
                }
            }  else if result["status"] as? Int == 201 {
                if result["message"] as? String == "User must be logged in" {
                    completion(nil,nil,true,nil)
                }
            }
            else {
                if let topController = UIApplication.shared.keyWindow?.rootViewController {
                    topController.showAlert(title: Constants.MESSAGE, message: (result["message"] as! String))
                }
            }
        }, failure: {(error) in
            completion(nil,nil,false,error)
        })
    }
    static func shoppingCart(shippingId: String, completion : @escaping compShoppingCart) {
        AFWrapper.serviceCall(urlEnum: AFUrlRequest.shoppingCart(shippingId: shippingId), success: {(result) in
            if result["status"] as? Int == 200 {
                if result["message"] as? String == "User must be logged in" {
                    completion(nil,true,"","",nil,nil,nil)
                } else {
                    if let message = result["message"] as? String,
                        message == "Empty Cart" ||
                        message == "cart is empty"
                        {
                        if let topController = UIApplication.shared.keyWindow?.rootViewController {
                            var msg =  "Empty Cart"
                            var title = Constants.MESSAGE
                            if Defaults.isArabicLanguage {
                                msg = "عربة فارغة"
                                title = Constants.MESSAGE_AR
                            }
                            topController.showAlert(title: title, message: msg)
                            completion(nil,false,result["message"] as! String,"",nil,nil,nil)
                        }
                    } else {

                        let startDate =   result["startDateForDelivery"] as! String
                        let endDate =   result["endDateForDelivery"] as! String
                        completion(cartProductModel.map(cartProducts: result["cartProducts"] as! [NSDictionary]),false,startDate ,endDate 
                            ,BagModel.map(bags: result["Bags"] as! [NSDictionary]),(result["SubTotal"],result["VAT"],result["totalPrice"],result["City"]) as? (subtotal: NSNumber, Vat: String, total: String,city :String),nil)
                    }
              
                }
                }
            else if result["status"] as? Int == 201 {
                if result["message"] as? String == "User must be logged in" {
                    completion(nil,true,"","",nil,nil,nil)
                }
            }
            else {
                if let topController = UIApplication.shared.keyWindow?.rootViewController {
                    topController.showAlert(title: Constants.MESSAGE, message: (result["message"] as! String))
                }
            }
        }, failure: {(error) in
           completion(nil,false,"","",nil,nil,error)
        })
    }

static func orderDetail(id: Int, completion : @escaping compOrderDetail) {
    AFWrapper.serviceCall(urlEnum: AFUrlRequest.orderDetail(id: id), success: {(result) in
        if result["status"] as? Int == 200 {
            if result["message"] as? String == "User must be logged in" {
                completion(nil,true,"","",nil)
            } else {
                if result["message"] as? String == "No previous orders" {
                    if let topController = UIApplication.shared.keyWindow?.rootViewController {
                        topController.showAlert(title: Constants.MESSAGE, message: (result["message"] as! String))
                      
                    }
                } else {
                let startDate =   result["bill_address"] as! [NSDictionary]
                let endDate =   result["ship_address"] as! [NSDictionary]
                    var billAdd: NSDictionary  = [:]
                    var shipAdd : NSDictionary = [:]
                    
                    for start in startDate {
                        billAdd = start as! NSDictionary
                    }
                    for start in endDate {
                        shipAdd = start as! NSDictionary
                    }
                completion(OrderDetailModel.map(cartProducts: result["data"] as! [NSDictionary]),false,billAdd["address1"] as? String ?? "",shipAdd["address1"] as? String ?? ""
                    ,nil)
                }
            }
        }   else if result["status"] as? Int == 201 {
            if result["message"] as? String == "User must be logged in" {
                completion(nil,true,"","",nil)
            } else {
     
                    if let topController = UIApplication.shared.keyWindow?.rootViewController {
                        topController.showAlert(title: Constants.MESSAGE, message: (result["message"] as! String))

                    }
            }
        }
        else {
            if let topController = UIApplication.shared.keyWindow?.rootViewController {
                topController.showAlert(title: Constants.MESSAGE, message: (result["message"] as! String))
            }
        }
    }, failure: {(error) in
        completion(nil,false,"","",error)
    })
}
    static func couponList(completion : @escaping compCouponList) {
        AFWrapper.serviceCall(urlEnum: AFUrlRequest.couponList(), success: {(result) in
            if result["status"] as? Int == 200 {
                if result["message"] as? String == nil{
                    completion(CouponModel.map(coupons: result["message"] as! [NSDictionary]),false,nil)
               
                } else {
                    completion(nil,true,nil)
                }
            } else if result["status"] as? Int == 201 {
                if result["message"] as? String == "User must be logged in" {
                    completion(nil,true,nil)
                }
            } else {
                if let topController = UIApplication.shared.keyWindow?.rootViewController {
                    topController.showAlert(title: Constants.MESSAGE, message: (result["message"] as! String))
                }
            }
        }, failure: {(error) in
            completion(nil,false,error)
        })
    }
    
    static func getAddresses(completion : @escaping compGetAddresses) {
        AFWrapper.serviceCall(urlEnum: AFUrlRequest.addresses(), success: {(result) in
            if result["status"] as? Int == 200 {
                if result["message"] as? String == nil{
//                    if result["data"] as! [NSDictionary] != [] {
                        completion(AddressModel.map(dictionary: result["data"] as! [NSDictionary]),false,nil)
//                    }
//                    else
//                    {
//                        completion([],nil,nil)                   }
                } else {
                    completion(nil,true,nil)
                }
            } else if result["status"] as? Int == 201 {
                if result["message"] as? String == "User must be logged in" {
                    completion(nil,true,nil)
                }
            } else {
                if let topController = UIApplication.shared.keyWindow?.rootViewController {
                    topController.showAlert(title: Constants.MESSAGE, message: (result["message"] as! String))
                }
            }
        }, failure: {(error) in
            completion(nil,false,error)
        })
    }
    static func getBannerImages(completion : @escaping compGetBannerImages) {
        AFWrapper.serviceCall(urlEnum: AFUrlRequest.bannerImages(), success: {(result) in
            if result["status"] as? Int == 200 {
                completion(ImageModel.map(images: result["data"] as! [NSDictionary]),(result["link"] as! String),nil)
            }
            else {
                if let topController = UIApplication.shared.keyWindow?.rootViewController {
                    topController.showAlert(title: Constants.MESSAGE, message: (result["message"] as! String))
                }
            }
        }, failure: {(error) in
            completion(nil,nil,error)
        })
    }
    
    static func getAllCountries(completion : @escaping compGetCountries) {
        AFWrapper.serviceCall(urlEnum: AFUrlRequest.countries(), success: {(result) in
            if result["status"] as? Int == 200 {
               completion(200,CountryCityModel.map(conCityDictionary: result["data"] as! [NSDictionary]),nil)
            }
            else {
                if let topController = UIApplication.shared.keyWindow?.rootViewController {
                    topController.showAlert(title: Constants.MESSAGE, message: (result["message"] as! String))
                }
            }
            
        }, failure: {(error) in
            completion(nil,nil,error)
        })
    }
    
    static func register(firstName: String, lastName:String ,password: String,  email: String, mobile: String,company_name: String, c_password: String, completion: @escaping compRegistration ) {
        AFWrapper.serviceCall(urlEnum: AFUrlRequest.register(email: email, pass: password, firstname: firstName, lastname: lastName, companyName: company_name, mobilenumber: mobile), success: { (result) in
            print(result)
            if result["status"] as? Int == 200 {
                completion(UserModel(fromDictionary: result["user"] as! NSDictionary),nil)
            }
            else if result["status"] as? Int == 422 {
                if let topController = UIApplication.shared.keyWindow?.rootViewController {
                    topController.showAlert(title: Constants.MESSAGE, message: (result["message"] as! String))
                }
            }
            else {
                if let topController = UIApplication.shared.keyWindow?.rootViewController {
                    topController.showAlert(title: Constants.MESSAGE, message: (result["message"] as! String))
                }
            }  
        }) { (NSError) in
            completion(nil,NSError)
        }
    }
    
    static func signin(email: String,userPass: String, completion: @escaping compRegistration ) {
        AFWrapper.serviceCall(urlEnum: AFUrlRequest.signin(email: email, password: userPass), success: { (result) in
            if result["status"] as? Int == 200 {
                completion(UserModel(fromDictionary: result["user"] as! NSDictionary), nil)
            }
            else {
                if let topController = UIApplication.shared.keyWindow?.rootViewController, (result["status"] as? Int != 500) {
                    topController.showAlert(title: Constants.MESSAGE, message: (result["message"] as! String))
                }
            }
            
        }) { (NSError) in
            completion(nil,NSError)
        }
    }
    
    static func updateProfile(firstname: String, lastname: String, mobile: String,email: String, companyname: String , completion : @escaping compUserVerification) {
        AFWrapper.serviceCall(urlEnum: AFUrlRequest.updateprofile(firstname: firstname, lastname: lastname, mobile: mobile, email: email, companyname: companyname), success: { (result) in
            if result["status"] as? Int == 200 {
                completion((result["message"] as! String),nil)
            }
            else {
                if let topController = UIApplication.shared.keyWindow?.rootViewController {
                    topController.showAlert(title: Constants.MESSAGE, message: (result["message"] as! String))
                }
            }
        }) { (error) in
            completion(nil,error)
        }
    }
    static func resetPassword(email: String, code: String, password: String, completion : @escaping compresetPass) {
        AFWrapper.serviceCall(urlEnum: AFUrlRequest.resetpass(email: email, code: code, pass: password), success: { (result) in
            if result["status"] as? Int == 200 {
                completion(200,(result["message"] as! String),nil)
            }
            else if  result["status"] as? Int == 201 {
                completion(201,(result["message"] as! String),nil)
            }
                
            else {
                if let topController = UIApplication.shared.keyWindow?.rootViewController {
                    topController.showAlert(title: Constants.MESSAGE, message: (result["message"] as! String))
                }
            }
        }) { (error) in
            completion(nil,nil,error)
        }
    }
    
    static func updatePassword(email: String, oldpassword: String, newpassword: String, completion : @escaping compUserVerification) {
        AFWrapper.serviceCall(urlEnum: AFUrlRequest.updatepass(email: email, oldpassword: oldpassword, newpass: newpassword ), success: { (result) in
            if result["status"] as? Int == 200 {
                completion((result["message"] as! String),nil)
            }
            else {
                if let topController = UIApplication.shared.keyWindow?.rootViewController {
                    topController.showAlert(title: Constants.MESSAGE, message: (result["message"] as! String))
                }
            }
        }) { (error) in
            completion(nil,error)
        }
    }
    
    static func userVerification(email: String, password: String, code: String, completion : @escaping compUserVerification) {
        AFWrapper.serviceCall(urlEnum: AFUrlRequest.userVerification(email: email, password: password, code: code), success: { (result) in
            if result["status"] as? Int == 200 {
                completion((result["message"] as! String),nil)
            }
         }) { (error) in
            completion(nil,error)
        }
    }
    
    static func forgotPassword(email: String, completion: @escaping compUserVerification ) {
        AFWrapper.serviceCall(urlEnum: AFUrlRequest.forgotPassword(email: email), success: { (result) in
            if result["status"] as? Int == 200 {
                completion((result["message"] as! String) ,nil)
            }
            else {
                if let topController = UIApplication.shared.keyWindow?.rootViewController {
                    topController.showAlert(title: Constants.MESSAGE, message: (result["message"] as! String))
                }
            }
            
        } , failure: { (NSError) in
            completion(nil,NSError)
        })
    }
    
    
    //    static func contactUs(name: String, email: String, phone: String, message: String, completion: compStrErr) {
    //        AFWrapper.serviceCall(urlEnum: AFUrlRequest.contactUs(name: name, email: email, phone: phone, message: message), success: { (JSON) in
    //            print(JSON)
    //            if let status = JSON["status"].int, status == 200{
    //                let msg = JSON["message"].string
    //                completion!(msg,nil)
    //            }else{
    //                completion!("500",nil)
    //            }
    //
    //        }) { (NSError) in
    //            completion!(nil,NSError)
    //        }
    //    }
    //
    //    static func accountDetail(bank_name: String, account_title: String, account_number: String, iban: String, completion: compStrErr) {
    //        AFWrapper.serviceCall(urlEnum: AFUrlRequest.bankDetail(bank_name: bank_name, account_title: account_title, account_number: account_number, iban: iban), success: { (JSON) in
    //            print(JSON)
    //            if let status = JSON["status"].int, status == 200{
    //                let msg =   JSON["message"].string
    //
    //
    //
    ////                 let model = Mapper<BankDetailsModel>().map(JSONObject: JSON.dictionaryObject)
    //
    //
    //                completion!(msg,nil)
    //            }else{
    //                completion!("500",nil)
    //            }
    //
    //        }) { (NSError) in
    //            completion!(nil,NSError)
    //        }
    //    }
    //
    
    //
    //    static func getProfileDetail(completion: completionUserModel ) {
    //        AFWrapper.serviceCall(urlEnum: AFUrlRequest.getProfileDetail(), success: { (json) in
    //            print(json)
    //            if let status = json["status"].int, status == 200{
    //                let msg = json["message"].string
    //
    //                let model = Mapper<UserModel>().map(JSONObject: json.dictionaryObject)
    //
    //                completion!(model,nil)
    //            }else{
    //                completion!(nil,nil)
    //            }
    //
    //        }) { (error) in
    //            completion!(nil,error)
    //        }
    //    }
    //    static func getOrderList(completion: completionOrdersFetch ) {
    //        AFWrapper.serviceCall(urlEnum: AFUrlRequest.getOrderList(), success: { (json) in
    //            print(json)
    //            if let status = json["status"].int, status == 200{
    //                let msg = json["message"].string
    //
    //                let model = Mapper<OrderListModel>().map(JSONObject: json.dictionaryObject)
    //
    //                completion!(model,nil)
    //            }else{
    //                completion!(nil,nil)
    //            }
    //
    //        }) { (error) in
    //            completion!(nil,error)
    //        }
    //    }
    //
    //    static func getOrderHistory(completion: completionOrdersHistoryFetch ) {
    //        AFWrapper.serviceCall(urlEnum: AFUrlRequest.getOrderHistory(), success: { (json) in
    //            print(json)
    //            if let status = json["status"].int, status == 200{
    //                let msg = json["message"].string
    //
    //                let model = Mapper<OrderHistoryModel>().map(JSONObject: json.dictionaryObject)
    //
    //                completion!(model,nil)
    //            }else{
    //                completion!(nil,nil)
    //            }
    //
    //        }) { (error) in
    //            completion!(nil,error)
    //        }
    //    }
    //
    //    static func reqForgotPassword(email: String,userPass: String, completion: @escaping compStrErrMessage ) {
    //        AFWrapper.serviceCall(urlEnum: AFUrlRequest.reqForgotPassword(email: email, password: userPass), success: { (json) in
    //            print(json)
    //            if let status = json["status"].int, status == 200{
    //                let msg = json["message"].string
    //
    //
    //                completion("200",msg,nil)
    //            }else{
    //                completion("500",json["message"].string,nil)
    //            }
    //
    //        }) { (NSError) in
    //            completion(nil,nil,NSError)
    //        }
    //    }
    //    static func editProfile(firstName: String, lastName:String ,  email: String, mobile: String, drivingLicenseNumber: String,  driving_license_expiry: String,company: String, city: String,  profile_image: String,id_image: String, license_image: String,  completion: @escaping compStrErrMessage ) {
    //        AFWrapper.serviceCall(urlEnum: AFUrlRequest.editProfile(firstName: firstName, lastName: lastName, email: email, mobile: mobile, drivingLicenseNumber: drivingLicenseNumber, driving_license_expiry: driving_license_expiry, company: company, city: city, profile_image: profile_image, id_image: id_image, license_image: license_image),  success: { (json) in
    //            print(json)
    //            if let status = json["status"].int, status == 200{
    //                let msg = json["message"].string
    //
    //
    //                completion("200",msg,nil)
    //            }else{
    //                completion("500",json["message"].string,nil)
    //            }
    //
    //        }) { (NSError) in
    //            completion(nil,nil,NSError)
    //        }
    //    }
    //    static func getOrderDetail(id:String,completion: completionOrderDetail) {
    //        AFWrapper.serviceCall(urlEnum: AFUrlRequest.getOrderDetail(id: id), success: { (json) in
    //            print(json)
    //            if let status = json["status"].int, status == 200{
    //                let msg = json["message"].string
    //
    //                let model = Mapper<OrderDetailModel>().map(JSONObject: json.dictionaryObject)
    //
    //                completion!(model,nil)
    //            }else{
    //                completion!(nil,nil)
    //            }
    //
    //        }) { (error) in
    //            completion!(nil,error)
    //        }
    //    }
    //
    //    static func getStores(completion: completionStoreFetch) {
    //        AFWrapper.serviceCall(urlEnum: AFUrlRequest.fetchStore(), success: { (json) in
    //            print(json)
    //            if let status = json["status"].int, status == 200{
    //                let msg = json["message"].string
    //
    //                let model = Mapper<FetchStoreModel>().map(JSONObject: json.dictionaryObject)
    //
    //                completion!(model,nil)
    //            }else{
    //                completion!(nil,nil)
    //            }
    //
    //        }) { (error) in
    //            completion!(nil,error)
    //        }
    //    }
    //
}




