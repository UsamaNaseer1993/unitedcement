//
//  AlamofireRequestHelper.swift
//  WebserviceTest
//
//  Created by Waqas Ali on 21/07/2016.
//  Copyright © 2016 Traffic Digital. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import NVActivityIndicatorView
import ObjectMapper
import MobileCoreServices


//struct Knetwork {
//static let status = "status"
//static let Failed = "Failed"
//}

enum Knetwork : String {
    case status = "status"
    case Failed = "Failed"
}


class AFWrapper: NSObject , NVActivityIndicatorViewable{
    
    static var viewsDictionary = Dictionary<String,AnyObject>()
    static var constraints = [NSLayoutConstraint]()
    
    class func requestGETURL(_ strURL: String, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void) {
        Alamofire.request(strURL).responseJSON { (responseObject) -> Void in
            
           // print(responseObject)
            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
    
    
 
    
class func requestPOSTURL(_ strURL : String, params : [String : AnyObject]?, headers : [String : String]?, success:@escaping (JSON) -> Void, failure:@escaping (Error) -> Void){
    
    let size = CGSize(width: 50, height:50)
    let presenter = PresenterViewControllerr()
     presenter.startAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 6)!)

    
        Alamofire.request(strURL, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (responseObject) -> Void in
            presenter.stopAnimating()
            
            if responseObject.result.isSuccess {
                let resJson = JSON(responseObject.result.value!)
                success(resJson)
            }
            else if responseObject.result.isFailure {
                let error : Error = responseObject.result.error!
                failure(error)
            }
        }
    }
    
    class func requestUploadImageWithParamsAndToken(_ img:UIImage? , fileName:String, params: [String: String]? = nil , url:String , success:@escaping(NSDictionary) -> Void , failure:@escaping(Error) -> Void) {
      
        
//        let authorization = ["Authorization": UserModel.currentUser!.getToken]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            if let imageData = img?.jpegData(compressionQuality: 0.5) {
                multipartFormData.append(imageData, withName: fileName, fileName: "\(NSDate()).jpeg", mimeType: "image/jpeg")
            }
            if let params = params{
            for (key, value) in params {
                
                multipartFormData.append((value).data(using: String.Encoding.utf8)!, withName: key)
            }
            }
            
            
        }, usingThreshold: SessionManager.multipartFormDataEncodingMemoryThreshold, to: url, method: .post, headers: nil) { (result) in
            switch result {
            case .success(let upload, _, _):
                
                upload.uploadProgress(closure: { (Progress) in
                    print("Upload Progress: \(Progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    //self.delegate?.showSuccessAlert()
//                    print(response.request)  // original URL request
//                    print(response.response) // URL response
//                    print(response.data)     // server data
//                    print(response.result)   // result of response serialization
                    //                        self.showSuccesAlert()
                    //self.removeImage("frame", fileExtension: "txt")
                    if let json = response.result.value{
                  //      print("JSON: \(json)")
                       
                        success(json as! NSDictionary)
                    }
                }
                
            case .failure(let encodingError):
                //self.delegate?.showFailAlert()
                print(encodingError)
                failure(encodingError)
            }
        }
        
        
        
      
        
       
    }
    

    
    
class func serviceCall(urlEnum: URLRequestConvertible,  success:@escaping (NSDictionary) -> Void, failure:@escaping (NSError) -> Void) {
        
         let size = CGSize(width: 50, height:50)
         let presenter = PresenterViewControllerr()
        
        presenter.startAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 6)!)
       
        Alamofire.request(urlEnum).responseJSON { (responseObject) -> Void in
            
           // print(responseObject)
            
           presenter.stopAnimating()
            
            if responseObject.result.isSuccess {
                let resJson  = responseObject.result.value! as! NSDictionary
//                if resJson[Knetwork.status.rawValue].stringValue == Knetwork.Failed.rawValue {
//                     success(resJson)
//                    UIApplication.inputViewController()?.showAlert(title: Constants.MESSAGE, message: resJson["statusDesc"].string ?? "error bro error", completion: {
//                        print("is this working?")
//                    })



             //   }else{
                success(resJson)
                    
                   
             //   }
            }
            if responseObject.result.isFailure {
               let error : NSError = responseObject.result.error! as NSError
                if let err = responseObject.result.error as NSError? , err.code == -1009 {
                    // no internet connection
                    failure(error)
                  
                    if let topController = UIApplication.shared.keyWindow?.rootViewController {
                        topController.showAlert(title: "Internet not Connected", message: KInternetCheck )

                    }
                } else {
                    if let topController = UIApplication.shared.keyWindow?.rootViewController {
                        topController.showAlert(title: "Server not Responding", message: error.localizedDescription )
                        
                    }
                     failure(error)
                }
                
              //--ww  failure(error)
            }
        }
    }
    class func serviceCallJson(urlEnum: URLRequestConvertible,  success:@escaping (JSON) -> Void, failure:@escaping (NSError) -> Void) {
        
        let size = CGSize(width: 50, height:50)
        let presenter = PresenterViewControllerr()
        
        presenter.startAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 6)!)
        
        Alamofire.request(urlEnum).responseJSON { (responseObject) -> Void in
            
            // print(responseObject)
            
            presenter.stopAnimating()
            
            if responseObject.result.isSuccess {
                let resJson  = responseObject.result.value! as! JSON
                //                if resJson[Knetwork.status.rawValue].stringValue == Knetwork.Failed.rawValue {
                //                     success(resJson)
                //                    UIApplication.inputViewController()?.showAlert(title: Constants.MESSAGE, message: resJson["statusDesc"].string ?? "error bro error", completion: {
                //                        print("is this working?")
                //                    })
                
                
                
                //   }else{
                success(resJson)
                
                
                //   }
            }
            if responseObject.result.isFailure {
                let error : NSError = responseObject.result.error! as NSError
                if let err = responseObject.result.error as NSError? , err.code == -1009 {
                    // no internet connection
                    failure(error)
                    
                    if let topController = UIApplication.shared.keyWindow?.rootViewController {
                        topController.showAlert(title: "Internet not Connected", message: KInternetCheck )
                        
                    }
                } else {
                    if let topController = UIApplication.shared.keyWindow?.rootViewController {
                        topController.showAlert(title: "Server not Responding", message: error.localizedDescription )
                        
                    }
                    failure(error)
                }
                
                //--ww  failure(error)
            }
        }
    }


    class  func serviceCallWithOptionalLoader(urlEnum: URLRequestConvertible, showLoader :Bool,  success:@escaping (JSON) -> Void, failure:@escaping (NSError) -> Void ) {
         let presenter = PresenterViewControllerr()
       if showLoader == true {
            let size = CGSize(width: 50, height:50)
           presenter.startAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 6)!)
        }
        
       Alamofire.request(urlEnum).responseJSON { (responseObject) -> Void in
            
            print(responseObject)
            
            
            presenter.stopAnimating()
            
            if responseObject.result.isSuccess {
                let resJson  = JSON(responseObject.result.value!)
                if resJson[Knetwork.status.rawValue].stringValue == Knetwork.Failed.rawValue {
                    success(resJson)
                    
                    Alert.showAlertMsgWithTitle(Constants.MESSAGE, msg: resJson["statusDesc"].stringValue, btnActionTitle: "OK", viewController: nil, completionAction: { (Void) in
                        if resJson["statusDesc"].stringValue == "Invalid Session!" {
                            
                             let delegate = UIApplication.shared.delegate as! AppDelegate
                            
//                            let navCon = leftMenu.getMainViewController(sideDrawer: delegate.drawerController!)
                            
//                            navigation.setViewControllerArray(vcIdentifier: "LoginViewController", navCon: navCon)
                            
                           
                        
                        
                        }
                    })
                }else{
                    success(resJson)
                    
                    
                }
            }
            if responseObject.result.isFailure {
                let error : NSError = responseObject.result.error! as NSError
                if let err = responseObject.result.error as NSError? , err.code == -1009 {
                    // no internet connection
                    
                    print("hey bro! you r not connected to internet")
                } else {
                    // other failures
                }
                
                failure(error)
            }
        }
    }
   
    class  func serviceCallWithOptionalLoaderAndToken(urlEnum: URLRequestConvertible, showLoader :Bool? = nil,  success:@escaping (JSON) -> Void, failure:@escaping (NSError) -> Void ) {
        let presenter = PresenterViewControllerr()
        if showLoader == true {
            let size = CGSize(width: 50, height:50)
            presenter.startAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 6)!)
        }
        
     

        Alamofire.request(urlEnum).responseJSON { (responseObject) -> Void in
            
            print(responseObject)
            
            
            presenter.stopAnimating()
            
            if responseObject.result.isSuccess {
                let resJson  = JSON(responseObject.result.value!)
                if resJson[Knetwork.status.rawValue].stringValue == Knetwork.Failed.rawValue {
                    success(resJson)
                    
                    Alert.showAlertMsgWithTitle(Constants.MESSAGE, msg: resJson["statusDesc"].stringValue, btnActionTitle: "OK", viewController: nil, completionAction: { (Void) in
                        if resJson["statusDesc"].stringValue == "Invalid Session!" {
                            
                            let delegate = UIApplication.shared.delegate as! AppDelegate
                            
                            //                            let navCon = leftMenu.getMainViewController(sideDrawer: delegate.drawerController!)
                            
                            //                            navigation.setViewControllerArray(vcIdentifier: "LoginViewController", navCon: navCon)
                            
                            
                            
                            
                        }
                    })
                }else{
                    success(resJson)
                    
                    
                }
            }
            if responseObject.result.isFailure {
                let error : NSError = responseObject.result.error! as NSError
                if let err = responseObject.result.error as NSError? , err.code == -1009 {
                    // no internet connection
                    
                    print("hey bro! you r not connected to internet")
                } else {
                    // other failures
                }
                
                failure(error)
            }
        }
    }
    
//    class func imageUploadWithData
    
    
}


