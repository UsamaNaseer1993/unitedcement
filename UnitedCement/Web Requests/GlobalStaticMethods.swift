//
//  GlobalStaticMethods.swift
//  Skeleton
//
//  Created by  Traffic MacBook Pro on 5/24/16.
//  Copyright © 2016 My Macbook Pro. All rights reserved.
//

import UIKit
import Foundation
import SystemConfiguration
//import AMLocalized
class DeviceUtil {
    
    
    
    
    
    
    /* ======================================================================================
     Enum for Device
     ========================================================================================= */
    
    
    enum  Device: Int {
        case iPhone4 ,
        iPhone5,
        iPhone6 ,
        iPhone6Plus ,
        iPadMini,
        iPad,
        iPadPro,
        Unknown
        
    }
    
    
    
    
    
    /* ======================================================================================
     Static variable to get screen size which is orientation independent and it is "let" so only
     first time call's geteSize(private method). In this geteSize call's only first time.
     
     value == CGSize
     ===================================================================================== */
    static let size : CGSize = DeviceUtil.getSize()
    
    
    /* ======================================================================================
     Static variable to get device type according to screen height it is "let" so only
     first time call's getDeviceType(private method). In this getDeviceType call's only first time.
     
     i.e if current device height is 960 then return iphone 4
     
     value == Device Enum
     ===================================================================================== */
    
    static let deviceType : Device = DeviceUtil.getDeviceType()
    
    
    
    
    /* ======================================================================================
     
     Private static method to get screen size
     
     Parameter == nil;
     Return == CGSize
     
     ===================================================================================== */
    private static func getSize() -> CGSize {
        

        
        var size : CGSize = CGSize(width: UIScreen.main.nativeBounds.size.width/UIScreen.main.scale, height: UIScreen.main.nativeBounds.size.height/UIScreen.main.scale)
        
        // For iPhone 6 Plus
        
        if(size.height == 640 && size.width  == 360){
            size = CGSize(width : 414, height : 736)
        }
        
        return size
    }
    
    
    /* ======================================================================================
     
     Private static method to get Device Type
     
     Parameter == nil;
     Return == Device Enum
     
     ===================================================================================== */
    
    private static func getDeviceType() -> Device
    {
        
        let height = UIScreen.main.nativeBounds.size.height
        
        switch height {
        case  960:
            return .iPhone4
        case  1136:
            return .iPhone5
        case  1334:
            return .iPhone6
        case  2208:
            return .iPhone6Plus
        case  1024:
            return .iPadMini
        case  2048:
            return .iPad
        case  2732:
            return .iPadPro
        default:
            return .Unknown;
        }
    }
    
    
    /* ======================================================================================
     
     Static method to get Device Language
     
     Parameter == nil;
     Return ==  NSString    i.e "en-US"
     
     
     ===================================================================================== */
    
    static func getDeviceLanguage() -> String
    {
        let pre = NSLocale.preferredLanguages[0]
        
        return pre;
    }
    
    
    /* ======================================================================================
     
     Static method to get Device Orienttation is in LandscapeLeft or LandscapeRight
     
     Parameter == nil;
     Return ==  BOOL   
     
     i.e "Orienttation == .LandscapeLeft"
     return true
     
     
     ===================================================================================== */
    
    static func isLandscape() -> Bool
    {
        let orientation : UIInterfaceOrientation = UIApplication.shared.statusBarOrientation;
        
        if (orientation == .landscapeLeft || orientation == .landscapeRight)
        {
            return true
        }
        return false
    }
}












class GlobalStaticMethods {
    //NSArray *ControllersArray
    
    
       static func getHeaderFooterHeight() -> CGFloat{
        
        if GlobalStaticMethods.isPhone() {
            return 56.0
        }
        else{
            if  GlobalStaticMethods.isPadPro(){
                return 100.0
            }
            else{
                return 85.0
            }
        }
    }
    
    
    
    static func removeSpecialCharacters(string: String)->String?{
        return string.replacingOccurrences(of: "+", with: "").replacingOccurrences(of: "#", with: "").replacingOccurrences(of: "*", with: "").replacingOccurrences(of: ":", with: "").replacingOccurrences(of: "<", with: "").replacingOccurrences(of: ">", with: "").replacingOccurrences(of: " ", with: "")
    }
    
    
    static func isValidNumber(number : String) -> Bool{
        
        
        var myNumber  = number
        
        if myNumber.contains("+") {
          
            myNumber = GlobalStaticMethods.removeSpecialCharacters(string: myNumber)!
            
        }
        
        
        let badCharacters = NSCharacterSet.decimalDigits.inverted
        
        



        if myNumber.rangeOfCharacter(from: badCharacters) == nil {
            return true
        } else {
            return false
        }
        //        if number.characters.count < 6 {
        //            return false
        //        }
        //        let PHONE_REGEX = "^\\d{3}-\\d{3}-\\d{4}$"
        //        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        //        let result =  phoneTest.evaluateWithObject(number)
        //        return result
    }
    
    
//    static func isPhoneNumber(number : String) -> Bool{
//        
//        let charcter  = NSCharacterSet(charactersIn: "+0123456789").inverted
//        var filtered:String!
//        let inputString : [String] = number.components(separatedBy: charcter)
//        filtered = inputString.components(joinWithSeparator: ",")
//        return  number == filtered
//        
//    }
    
    static func isValidPassword(password : String) -> Bool{
        if password.characters.count < 8{
            return false
        }
        let passwordRegEx = "^(?=.*[A-Z])(?=.*[0-9])(?=.*[a-z].*[a-z].*[a-z]).{8,16}$"
        //                let passwordRegEx = "^(?=.*[A-Z])(?=.*[!@#$&*])(?=.*[0-9])(?=.*[a-z].*[a-z].*[a-z]).{8,14}$"

        let passwordTest = NSPredicate(format: "SELF MATCHES %@", passwordRegEx)
        let result = passwordTest.evaluate(with: password)
        return result
    }
    
    
    
    static func isValidEmail(email : String) -> Bool{
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    
    static func isLettersOnly(value : String) -> Bool{
        for chr in value.characters {
            if (!(chr >= "a" && chr <= "z") && !(chr >= "A" && chr <= "Z") ) {
                return false
            }
        }
        return true
    }
    
    static func isLettersANDSpacesOnly(value : String) -> Bool{
        let letterSpaceRegEx = "[a-zA-Z][a-zA-Z ]+"
        let letterTest = NSPredicate(format: "SELF MATCHES %@", letterSpaceRegEx)
        let result = letterTest.evaluate(with: value)
        return result
    }
    
    static func getTabsHeight() -> CGFloat{
        
        if GlobalStaticMethods.isPhone() {
            return 44.0
        }
        else{
            if  GlobalStaticMethods.isPadPro(){
                return 60.0
            }
            else{
                return 55.0
            }
        }
        
        
    }
    
    static func getDeviceTypeStr() -> String {
        let str = language.getCurrentLanguage()

        if GlobalStaticMethods.isPad(){
            return "iPad-\(str)"
        }
        else{
            return "iPhone-\(str)"
        }
    }
    
    static func showAlertWithTitle( title:String, message:String, vc:UIViewController) {
        
        let alertVC = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertVC.addAction(okAction)
        
        
        DispatchQueue.main.async{
            
            vc.present(alertVC, animated: true, completion: nil)
            // self.presentViewController(alertVC, animated: true, completion: nil)
            
        }
        
    }
    
    
    // MARK: - trimStr
    //    +(void)showMsg:(NSString*)msg withTitle:(NSString*)title;
    
    static func getCountriesList()->[String]{
        
        
        let countriesArr = ["Afghanistan",
                            "Albania",
                            "Algeria",
                            "Andorra",
                            "Angola",
                            "Antigua and Barbuda",
                            "Argentina",
                            "Armenia",
                            "Aruba",
                            "Australia",
                            "Austria",
                            "Azerbaijan",
                            "Bahamas",
                            "Bahrain",
                            "Bangladesh",
                            "Barbados",
                            "Belarus",
                            "Belgium",
                            "Belize",
                            "Benin",
                            "Bhutan",
                            "Bolivia",
                            "Bosnia and Herzegovina",
                            "Botswana",
                            "Brazil",
                            "Brunei",
                            "Bulgaria",
                            "Burkina Faso",
                            "Burma",
                            "Burundi",
                            "Cambodia",
                            "Cameroon",
                            "Canada",
                            "Cape Verde",
                            "Central African Republic",
                            "Chad",
                            "Chile",
                            "China",
                            "Colombia",
                            "Comoros",
                            "Congo, Democratic Republic of the",
                            "Congo, Republic of the",
                            "Costa Rica",
                            "Cote d'Ivoire",
                            "Croatia",
                            "Cuba",
                            "Curacao",
                            "Cyprus",
                            "Czech Republic",
                            "Denmark",
                            "Djibouti",
                            "Dominica",
                            "Dominican Republic",
                            "East Timor",
                            "Ecuador",
                            "Egypt",
                            "El Salvador",
                            "Equatorial Guinea",
                            "Eritrea",
                            "Estonia",
                            "Ethiopia",
                            "Fiji",
                            "Finland",
                            "France",
                            "Gabon",
                            "Gambia",
                            "Georgia",
                            "Germany",
                            "Ghana",
                            "Greece",
                            "Grenada",
                            "Guatemala",
                            "Guinea",
                            "Guinea-Bissau",
                            "Guyana",
                            "Haiti",
                            "Holy See",
                            "Honduras",
                            "Hong Kong",
                            "Hungary",
                            "Iceland",
                            "India",
                            "Indonesia",
                            "Iran",
                            "Iraq",
                            "Ireland",
                            "Israel",
                            "Italy",
                            "Jamaica",
                            "Japan",
                            "Jordan",
                            "Kazakhstan",
                            "Kenya",
                            "Kiribati",
                            "Korea, North",
                            "Korea, South",
                            "Kosovo",
                            "Kuwait",
                            "Kyrgyzstan",
                            "Laos",
                            "Latvia",
                            "Lebanon",
                            "Lesotho",
                            "Liberia",
                            "Libya",
                            "Liechtenstein",
                            "Lithuania",
                            "Luxembourg",
                            "Macau",
                            "Macedonia",
                            "Madagascar",
                            "Malawi",
                            "Malaysia",
                            "Maldives",
                            "Mali",
                            "Malta",
                            "Marshall Islands",
                            "Mauritania",
                            "Mauritius",
                            "Mexico",
                            "Micronesia",
                            "Moldova",
                            "Monaco",
                            "Mongolia",
                            "Montenegro",
                            "Morocco",
                            "Mozambique",
                            "Namibia",
                            "Nauru",
                            "Nepal",
                            "Netherlands",
                            "Netherlands Antilles",
                            "New Zealand",
                            "Nicaragua",
                            "Niger",
                            "Nigeria",
                            "North Korea",
                            "Norway,",
                            "Oman,",
                            "Pakistan",
                            "Palau",
                            "Palestinian Territories",
                            "Panama",
                            "Papua New Guinea",
                            "Paraguay",
                            "Peru",
                            "Philippines",
                            "Poland",
                            "Portugal",
                            "Qatar",
                            "Romania",
                            "Russia",
                            "Rwanda",
                            "Saint Kitts and Nevis",
                            "Saint Lucia",
                            "Saint Vincent and the Grenadines",
                            "Samoa",
                            "San Marino",
                            "Sao Tome and Principe",
                            "Saudi Arabia",
                            "Senegal",
                            "Serbia",
                            "Seychelles",
                            "Sierra Leone",
                            "Singapore",
                            "Sint Maarten",
                            "Slovakia",
                            "Slovenia",
                            "Solomon Islands",
                            "Somalia",
                            "South Africa",
                            "South Korea",
                            "South Sudan",
                            "Spain",
                            "Sri Lanka",
                            "Sudan",
                            "Suriname",
                            "Swaziland",
                            "Sweden",
                            "Switzerland",
                            "Syria",
                            "Taiwan",
                            "Tajikistan",
                            "Tanzania",
                            "Thailand",
                            "Timor-Leste",
                            "Togo",
                            "Tonga",
                            "Trinidad and Tobago",
                            "Tunisia",
                            "Turkey",
                            "Turkmenistan",
                            "Tuvalu",
                            "Uganda",
                            "Ukraine",
                            "United Arab Emirates",
                            "United Kingdom",
                            "Uruguay",
                            "Uzbekistan",
                            "Vanuatu",
                            "Venezuela",
                            "Vietnam",
                            "Yemen",
                            "Zambia","Zimbabwe"]
        
        return countriesArr
        
    }
    
    
    static func isPhone()->Bool{
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.phone{
            
            return true
        }
        else
        {
            return   false
            
        }
    }
    static func isPad()->Bool{
        
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad{
            
            return true
        }
        else
        {
            return   false
            
        }
    }
    
    static func isPadMini()-> Bool{
    
//        case  1024:
//        return .iPadMini
    
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.pad{
            
            
            if GlobalStaticMethods.getDeviceType() == DeviceUtil.Device.iPadMini {
              
                
            return true
                
            }
            else{
            
            return false
        }
            
        }
      return false
    
    }
    
    
    static func isPadPro()->Bool{
        
        
        if (UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad &&
            (UIScreen.main.bounds.size.height == 1366 || UIScreen.main.bounds.size.width == 1366)) {
            return true
        }
        return false
        
        
    }
    
     static func getDeviceType() -> DeviceUtil.Device
    {
        
        let height = UIScreen.main.nativeBounds.size.height
        
        switch height {
        case  960:
            return .iPhone4
        case  1136:
            return .iPhone5
        case  1334:
            return .iPhone6
        case  2208:
            return .iPhone6Plus
        case  1024:
            return .iPadMini
        case  2048:
            return .iPad
        case  2732:
            return .iPadPro
        default:
            return .Unknown;
        }
    }

    
//    static func setAttributedLabel(value : String , range : String , lblFontSize : CGFloat) -> NSMutableAttributedString{
//        
//        let main_string = value
//        let string_to_color = range
//
//        let range = (main_string as NSString).range(of: string_to_color)
//        let myAttribute = [ NSFontAttributeName: UIFont(name: "OpenSans-Light", size: lblFontSize - 2)! ]
//
//        let attributedString = NSMutableAttributedString(string:main_string )
//        attributedString.addAttributes(myAttribute , range: range)
//        attributedString.addAttribute(NSForegroundColorAttributeName, value: UIColor (hexString:"#848484")  , range: range)
//        attributedString.addAttribute(NSStrikethroughStyleAttributeName, value: 1, range: range)
//
//        
//        
//        return attributedString
//        
//    }
    
    
    
    
    
}

class Singleton {
    static let sharedInstance = Singleton()
    var currentProjectEnum :Int = 0
    var isPendingApproval : Bool = false
    var projectEnum : RequestType!
    var formEnum : Forms!
    var tabsEnum : tabs!
    var currentUser : userTypes!
    var courseVariatio : courseDetailVariations!
    
    var projectDataDictionary : NSArray!
    
    var arrHospitals = [AnyObject]()
    var arrEmails = Array<Dictionary<String,[AnyObject]>>()
    
    
    //Font
    var fontSizeSmall : Int = 0
    var fontSizeMedium : Int = 0
    var fontSizeLarge : Int = 0
    var fontSizeXLarge : Int = 0
    var fontPrimary : String  = ""
    var fontSecondary : String = ""
    
    //Color
    var colorRed : String = ""
    var colorBlue : String = ""
    var colorGreen : String = ""
    
    //Button
    var buttonSize : Int = 0
    
    //Image
    var imageSplash : String = ""
    
    enum pushNotificationType : Int {
        
     case Message = 1
     case CourseSuggested
     case EnrollmentApproval
        
        
    }
    
    
    var PNSType : pushNotificationType = .Message
    
    
    var selctedCategoryID : String! = ""
    
    
    /////////////////////////////  Push Notification Related Variables //////////////////////////////////
    
    var deviceToken : String = ""
    
    var  isComeFromLocalNotification : Bool = false
    
    
    var pushNotificationObj : Dictionary<String, AnyObject>!
    
    var localNotificationObj : Dictionary<String, AnyObject>!
    
    var isInValidUserCheckRequired : Bool = false
    
    
    ///////////////////////////////////////////////////////////////
    
    init() {
        
        //  projectDataDictionary =  plist .loadDataFromPlist("RequestVCType")
        
        
    }
    
    func loadConfig()
    {
        let arr =  plist.loadDataFromPlist(plistName: "BasePlist")
        debugPrint(arr)
        
        var fontDict = [String : Any]()
        var colorDict = [String : Any]()
        var buttonDict = [String : Any]()
        var imageDict = [String : Any]()
        
        fontDict = arr[0]["Font"] as! [String : Any]
        colorDict = arr[0]["Color"] as! [String : Any]
        buttonDict = arr[0]["Button"] as! [String : Any]
        imageDict = arr[0]["Image"] as! [String : Any]
        let fontSize = fontDict["FontSize"] as! [String : Any]
    
        self.fontPrimary = fontDict["FontPrimary"] as! String
        self.fontSecondary = fontDict["FontSecondary"] as! String
        self.fontSizeSmall = fontSize["FontSmall"] as! Int
        self.fontSizeMedium = fontSize["FontMedium"] as! Int
        self.fontSizeLarge = fontSize["FontLarge"] as! Int
        self.fontSizeLarge = fontSize["FontXLarge"] as! Int
    
        self.colorRed = colorDict["ColorRed"] as! String
        self.colorGreen = colorDict["ColorGreen"] as! String
        self.colorBlue = colorDict["ColorBlue"] as! String
    
        self.buttonSize = buttonDict["ButtonSize"] as! Int
        self.imageSplash = imageDict["Splash"] as! String
    }
    
}

class imageClass {
    static  func getImageFromURL(urlStr : String)->UIImage?{
        
        let imageURL = URL(string: urlStr)
    
        let image =  UIImage(data: NSData(contentsOf: imageURL!)! as Data)
        
        
        return image
        
        
    }
    
//    static func convertImageToBase64(image : UIImage) -> String {
//        
//        let imageData = UIImagePNGRepresentation(image)! as Data
//        
//        let strBase64:String = imageData.base
//        return strBase64
//        
//    }
    
    static func convertBase64ToImage(str : String)->UIImage{
        
        //   var strBase64 = str
        
        //        strBase64 = strBase64.stringByReplacingOccurrencesOfString("-", withString: "+", options: NSStringCompareOptions.LiteralSearch, range: nil)
        //        strBase64 = strBase64.stringByReplacingOccurrencesOfString( "_" , withString: "/", options: NSStringCompareOptions.LiteralSearch, range: nil)
        
        let dataDecoded:NSData = NSData(base64Encoded: str, options: NSData.Base64DecodingOptions(rawValue: 0))!
        let decodedimage:UIImage = UIImage(data: dataDecoded as Data)!
        
        return decodedimage
    }
    static func ResizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / image.size.width
        let heightRatio = targetSize.height / image.size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width : size.width * heightRatio, height : size.height * heightRatio)
        } else {
            newSize = CGSize(width : size.width * widthRatio, height :  size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x : 0, y :  0, width: newSize.width, height : newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    func screenShotViewWithSaveOption(view : UIView!) {
        //Create the UIImage
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        //Save it to the camera roll
        UIImageWriteToSavedPhotosAlbum(image!, nil, nil, nil)
    }
    func screenShotViewImage(view : UIView!) -> UIImage{
        //Create the UIImage
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        //Save it to the camera roll
        return image!
    }
    
    
}
class stringsClass {
    
    
    static func trimStr(str : String) ->String{

        return str .trimmingCharacters(in: .whitespacesAndNewlines)
        
    }
    
    
    
    
    
    
    
    static func sanitizeStr (str : String) -> String{
        
        
        let notAllowedCharacters = NSCharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789+ ").inverted
        
        let resultSTR = str.components(separatedBy: notAllowedCharacters).joined(separator: "")
        
        print(resultSTR)
        
        return resultSTR
        
    }
    
    static func containSpecialChars(str : String)->Bool{
        
        let notAllowedCharacters = NSCharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ ").inverted
        
        let resultSTR = str.components(separatedBy: notAllowedCharacters).joined(separator: "")
        
        print(resultSTR)
        
        if resultSTR.characters.count  == str.characters.count
        {
            return false;
        }
        else
        {
            return true;
        }
        
        
    }
    
    static func containOnlyNumbers(str : String)->Bool{
        let notAllowedCharacters = NSCharacterSet(charactersIn: "01234567890").inverted
        
        let resultSTR = str.components(separatedBy : notAllowedCharacters).joined(separator: "")
        
        print(resultSTR)
        
        if resultSTR.characters.count  == str.characters.count
        {
            return false;
        }
        else
        {
            return true;
        }
        
    }
    
    static func formatNumberAsCurrency(number : Int64)-> String{
        
        
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal
        
        let str = numberFormatter .string(from: NSNumber(value: number))
        
        
        
        return str!
        
    }
    
    static func isEmptyString(str : String) ->String{
        
        return     str .trimmingCharacters(in: .whitespacesAndNewlines)
        
    }
    
    static func stringWithoutWhitespaces(str : String)->String{
        
        
        let words   = str.components(separatedBy: .whitespacesAndNewlines)
        
        let nospacestring = words .joined(separator: "")
        
        return nospacestring
        
    }
    
    
    
    
}
class reachability{
    
//    class func isConnectedToNetwork() -> Bool {
//        var zeroAddress = sockaddr_in()
//        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
//        zeroAddress.sin_family = sa_family_t(AF_INET)
//        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
//            SCNetworkReachabilityCreateWithAddress(nil, UnsafePointer($0))
//        }
//        var flags = SCNetworkReachabilityFlags()
//        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
//            return false
//        }
//        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
//        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
//        return (isReachable && !needsConnection)
//    }
}
class language{
    
    
    static func ifLanguageChosen()->Bool{
        
        let defaults = UserDefaults.standard
        
        
        
        
        if ((defaults .value(forKey: Constants.language .LanguageChosen)) != nil)
        {
            return true;
        }
        
        return false;
        
    }
    
    static func setLanguage( type : ChooseLanguageTypeEnum){
        
        var chosenLang  : String!
        
        if type ==  ChooseLanguageTypeEnum.kLanguageEnglish
        {
            chosenLang = Constants.language.englishLanguage
            
        }
        else if type == ChooseLanguageTypeEnum.kLanguageArabic
        {
            chosenLang = Constants.language.arabicLanguage
        }
        else {
            chosenLang = Constants.language.urduLanguage

        }
        
//        LocalizationSystem.sharedLocal().setLanguage(chosenLang)

        let userDefaults =   UserDefaults.standard
        
        userDefaults .set(chosenLang, forKey: Constants.language.LanguageChosen)
        
        userDefaults .synchronize()
        
    }
    
    
    static func ifAppLanguageIs(language :String)->Bool{
        if (UserDefaults.standard.value(forKey: Constants.language.LanguageChosen) as! String == language)
        {
            return true;
        }
        
        return false;
    }
    static func getCurrentLanguage()->String{
        
         //return "ur"
        //        
                 if (UserDefaults.standard.value(forKey: Constants.language.LanguageChosen) != nil){
                
                    return  UserDefaults.standard.value(forKey: Constants.language.LanguageChosen) as! String
                }
        //
        return "en"
    } 
    
    
    //    static func checkIfSystemLanguageChanged(){
    //        
    //    }
    
    
}
/*
extension UIViewController {
    
    class var storyboardID : String{
        
        return"\(self)"
    }
    
    static func instantiate(fromAppStoryboard appStoryboard : viewController.AppStoryboard) -> Self {
        return appStoryboard.viewController(viewControllerClass: self)
    }

}

class viewController : UIViewController{
    enum AppStoryboard : String {
        
        case HelloViewController
        
        var instance : UIStoryboard {
            return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
        }
        func viewController<T : UIViewController>(viewControllerClass : T.Type) -> T {
            
            let storyboardID = (viewControllerClass as UIViewController.Type).storyboardID
            return instance.instantiateViewController(withIdentifier: storyboardID) as! T
        }
        
        func initialViewController() -> UIViewController? {
            return instance.instantiateInitialViewController()
        }

    }
    
  
}
*/

/*
 class navigation{
    
    
    var ControllersArray : NSArray!

    static let sharedInstance = navigation()
    
    
    //    func navigation(){
    //        
    //    }
    
    
    
    

    
    
    static func goToViewController(viewControllerIdentifier : String , sideDrawer : MMDrawerController){
        let sb = navigation.getStoryBoardForController(identifier: viewControllerIdentifier)!.storyboardObject
        let homeVC = sb.instantiateViewController(withIdentifier: viewControllerIdentifier)
        let navCon = sideDrawer.centerViewController as! MMNavigationController
        
        navCon.pushViewController(homeVC, animated: true)
        
    }
    
    
    static func PopToVC(viewControllerIdentifier : String , sideDrawer : MMDrawerController){
        
        let navCon = sideDrawer.centerViewController as! MMNavigationController
        
        let sb = navigation.getStoryBoardForController(identifier: viewControllerIdentifier)?.storyboardObject
        
        
        //   let desiredVC = sb!.instantiateViewControllerWithIdentifier(viewControllerIdentifier)
        
        //        if sideDrawer.visibleLeftDrawerWidth > 0 {
        //            closeDrawer(sideDrawer)
        //
        //        }
        let controllers = navCon.childViewControllers
        let countControllers = controllers.count
        
        let className = Bundle.main.infoDictionary!["CFBundleName"] as! String + "." + viewControllerIdentifier
        
        if navCon.visibleViewController!.isKind(of: NSClassFromString(className)!){
            
            
            return
            
        }
        
        for i in 0..<countControllers {
            
            if controllers[i].isKind(of : NSClassFromString(className)!)
            {
                
                
                NSLog("controller found at index %d", i)
                
                
                navCon.popToViewController(controllers[i], animated: true)
                return;
                
            }
            
            
            
            
        }
    }
    
    
    static func setViewControllerArray(vcIdentifier : String , navCon : UINavigationController){
        
        let sb = getStoryBoardForController(identifier: vcIdentifier)?.storyboardObject
        let vc = sb?.instantiateViewController(withIdentifier: vcIdentifier)
        
        navCon.setViewControllers([vc!], animated: true)
        
    }
    static func getMainStoryboardForDevice()->UIStoryboard{
        
        
        let devStr = GlobalStaticMethods.getDeviceTypeStr()
        let boardName = "Main-\(devStr)"
        
        return UIStoryboard(name: boardName, bundle: nil)
        
        
        
        
        
    }
    
    static func setRootViewControllerObj(controller : UIViewController){
        
        
        UIApplication.shared.keyWindow?.rootViewController = controller
    }
    
    
    static func PopToViewControllerObj(controller : UIViewController){
        
        
        UIApplication.shared.keyWindow?.rootViewController?.popoverPresentationController
        
        
    }
    static func pushViewContolerObjOnNavController(controller : UIViewController , navController : UINavigationController) {
        
        
        
    }
    
    
    static func getStoryboardName()->String{
        let devStr = GlobalStaticMethods.getDeviceTypeStr()
        
        return devStr
        
    }
    
    
    
    static func getStoryBoardForController(identifier:String)->(storyboardObject :UIStoryboard , storyBoardName :String)?{
        
        
        
        let boardName : String = ""
        
        if sharedInstance.ControllersArray == nil{
            
            let plistName = "controllers"
                     
            let plistControllers = Bundle.main.path(forResource: plistName, ofType: ".plist")
            
       //     let contents = NSDictionary(contentsOfFile: plistControllers!) as? [String : AnyObject]


            sharedInstance.ControllersArray = NSArray(contentsOfFile: plistControllers!)
            
            
        }
        
        let predicate = NSPredicate(format: "name LIKE[c] %@", identifier)
        let filteredArr = sharedInstance.ControllersArray .filtered(using: predicate)
        
        
//        let filteredArr = sharedInstance.ControllersArray.filter { predicate.evaluate(with: $0) };
//        print("names = ,\(filteredArr)")
        
        
        
        //  let filteredArr = sharedInstance.ControllersArray.filtered(using: predicate) as! [String]
        
        
        if filteredArr.count > 0{
            
//            let str = filteredArr[0]["sid"] as! String
//            boardName = "\(str)-\(navigation.getStoryboardName())"
            
        }
        else{
            print("Controller not found in any storyboard")
            //            break
            
            
            
            return nil
            
        }
        
        
        return (UIStoryboard(name: boardName, bundle: nil) , boardName)
        
    }
    
    static func getStoryboad() ->UIStoryboard{
        
        
        let boardName = "\(navigation.getStoryboardName())"
        return UIStoryboard(name: boardName, bundle: nil)
    }
    
    
    
    
}

*/

class navigation{
    
    static let sharedInstance = navigation()
    
    var ControllersArray : NSArray!
    
    
    //    func navigation(){
    //
    //    }
    
    static func logout(){
        
    }
    
//    static func popToRootViewControllerMain(animated: Bool) {
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        _ = appDelegate.navigationController?.popToRootViewController(animated: animated)
//    }
//    
//    static func pushOrPopViewControllerMain(viewControllerIdentifier : String , animation : Bool) {
//        
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        let navCon = appDelegate.navigationController
//        
//        let sb = navigation.getStoryBoardForController(identifier: viewControllerIdentifier)?.storyboardObject
//        
//        let controllers = navCon?.viewControllers
//        let countControllers = controllers?.count ?? 0
//        
//        let className = Bundle.main.infoDictionary!["CFBundleName"] as! String + "." + viewControllerIdentifier
//        
//        for i in 0..<countControllers {
//            if (controllers?[i].isKind(of: NSClassFromString(className)!))!
//            {
//                NSLog("controller found at index %d", i)
//            
//                _ = navCon?.popToViewController((controllers?[i])!, animated: true)
//                return;
//            }
//            
//            if (i == (countControllers-1))
//            {
//                let desiredVC = sb!.instantiateViewController(withIdentifier: viewControllerIdentifier)
//                navCon?.pushViewController(desiredVC, animated: true)
//            }
//        }
//    }
    
//    static func goToViewControllerMain(viewControllerIdentifier : String , animation : Bool){
//
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        
//        let sb = navigation.getStoryBoardForController(identifier: viewControllerIdentifier)!.storyboardObject
//        let homeVC = sb.instantiateViewController(withIdentifier: viewControllerIdentifier)
//        
//        appDelegate.navigationController?.pushViewController(homeVC, animated: animation)
//    }
//
//    static func goToViewControllerMain(viewController : UIViewController , animation : Bool){
//        
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.navigationController?.pushViewController(viewController, animated: animation)
//    }
//
//    static func getCurrentActiveTab()-> tabsEnum{
//        
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//            return tabsEnum(rawValue: appDelegate.tabBarController?.selectedIndex ?? 0)!//appDelegate.tabBarController?.selectedIndex ?? 0
//    }
//    static func goToViewController(viewControllerIdentifier : String , currentTab : tabsEnum, animation : Bool){
//        
//        let sb = navigation.getStoryBoardForController(identifier: viewControllerIdentifier)!.storyboardObject
//        let homeVC = sb.instantiateViewController(withIdentifier: viewControllerIdentifier)
//        let navCon = self.findCurrentNavCon(tab: currentTab)
//        navCon.pushViewController(homeVC, animated: animation)
//    }
//    
//    static func goToViewControllerUpdated(viewControllerIdentifier : String , animation : Bool){
//        let sb = navigation.getStoryBoardForController(identifier: viewControllerIdentifier)!.storyboardObject
//        let desiredVC = sb.instantiateViewController(withIdentifier: viewControllerIdentifier)
//        let selectedTab = getCurrentActiveTab()
//        let navCon = self.findCurrentNavCon(tab: selectedTab)
//        navCon.pushViewController(desiredVC, animated: true)
//        
//    }
//    static func goToViewControllerWithObject(viewControllerIdentifier : String , currentTab : tabsEnum, animation : Bool, obj : AnyObject , vcObj : UIViewController){
//        
//        let sb = navigation.getStoryBoardForController(identifier: viewControllerIdentifier)!.storyboardObject
//        let homeVC = sb.instantiateViewController(withIdentifier: viewControllerIdentifier)
//        let navCon = self.findCurrentNavCon(tab: currentTab)
//        navCon.pushViewController(homeVC, animated: animation)
//    }

    
//    static func findCurrentNavCon(tab: tabsEnum)->UINavigationController{
//        
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        let controllers = appDelegate.navigationController?.childViewControllers
//        let conCount    = controllers?.count ?? 0
//        
//        for i in 0...conCount-1 {
//            if controllers?[i] is CustomTabBarController {
//                
//                let navCon = controllers![i] as! CustomTabBarController
//                guard navCon.viewControllers?.count ?? 0 >= tab.rawValue else {
//                    debugPrint("Tab index is greater than controllers in tabbar")
//                    break
//                }
//                
//                return navCon.viewControllers![tab.rawValue] as! UINavigationController
//                break
//            }
//        }
//        
//        return UINavigationController()
//    }
//    
//    static func PopToVC(viewControllerIdentifier : String , sideDrawer : MMDrawerController){
//        
//        let navCon = sideDrawer.centerViewController as! MMNavigationController
//        
//        //       let sb = navigation.getStoryBoardForController(viewControllerIdentifier)?.storyboardObject
//        
//        
//        //   let desiredVC = sb!.instantiateViewControllerWithIdentifier(viewControllerIdentifier)
//        
//        //        if sideDrawer.visibleLeftDrawerWidth > 0 {
//        //            closeDrawer(sideDrawer)
//        //
//        //        }
//        let controllers = navCon.childViewControllers
//        let countControllers = controllers.count
//        
//        let className = Bundle.main.infoDictionary!["CFBundleName"] as! String + "." + viewControllerIdentifier
//        
//        if navCon.visibleViewController!.isKind(of: NSClassFromString(className)!){
//            
//            
//            return
//            
//        }
//        
//        for i in 0..<countControllers {
//            
//            if controllers[i].isKind(of: NSClassFromString(className)!)
//            {
//                
//                
//                NSLog("controller found at index %d", i)
//                
//                
//                navCon.popToViewController(controllers[i], animated: true)
//                return;
//                
//            }
//            
//        }
//    }
//    
//    
//    
//    static func pushOrPop(viewControllerIdentifier : String , sideDrawer : MMDrawerController){
//        
//        let navCon = sideDrawer.centerViewController as! MMNavigationController
//        
//        let sb = navigation.getStoryBoardForController(identifier: viewControllerIdentifier)?.storyboardObject
//        
//        
//        let desiredVC = sb!.instantiateViewController(withIdentifier: viewControllerIdentifier)
//        
//        if sideDrawer.visibleLeftDrawerWidth > 0 {
//            leftMenu.closeDrawer(sideDrawer: sideDrawer)
//            
//        }
//        let controllers = navCon.childViewControllers
//        let countControllers = controllers.count
//        
//        let className = Bundle.main.infoDictionary!["CFBundleName"] as! String + "." + viewControllerIdentifier
//        
//        
//        //        if ((navCon.visibleViewController?.isKindOfClass(NSClassFromString(className)!)) != nil) {
//        //            return
//        //
//        //        }
//        
//        
//        
//        for i in 0..<countControllers {
//            
//            
//            if controllers[i].isKind(of: NSClassFromString(className)!)
//            {
//                
//                
//                NSLog("controller found at index %d", i)
//                
//                
//                navCon.popToViewController(controllers[i], animated: true)
//                return;
//                
//            }
//            
//            if (i == (countControllers-1))
//            {
//                
//                navCon .pushViewController(desiredVC, animated: true)
//            }
//            
//            
//            
//        }
//    }
//    
//    static func pushOrPopObj(viewController : UIViewController , sideDrawer : MMDrawerController){
//        
//        let navCon = sideDrawer.centerViewController as! MMNavigationController
//        
//        
//        
//        
//        let desiredVC = viewController
//        
//        if sideDrawer.visibleLeftDrawerWidth > 0 {
//            leftMenu.closeDrawer(sideDrawer: sideDrawer)
//            
//        }
//        let controllers = navCon.childViewControllers
//        let countControllers = controllers.count
//        
//        let className = Bundle.main.infoDictionary!["CFBundleName"] as! String + "." + desiredVC.restorationIdentifier!
//        
//        if navCon.visibleViewController!.isKind(of: NSClassFromString(className)!){
//            return
//            
//        }
//        
//        for i in 0..<countControllers {
//            
//            if controllers[i].isKind(of: NSClassFromString(className)!)
//            {
//                
//                
//                NSLog("controller found at index %d", i)
//                
//                
//                navCon.popToViewController(desiredVC, animated: true)
//                return;
//                
//            }
//            
//            if (i == (countControllers-1))
//            {
//                
//                navCon .pushViewController(desiredVC, animated: true)
//            }
//            
//            
//            
//        }
//    }
    
    static func setViewControllerArray(vcIdentifier : String , navCon : UINavigationController){
        
        let sb = getStoryBoardForController(identifier: vcIdentifier)?.storyboardObject
        let vc = sb?.instantiateViewController(withIdentifier: vcIdentifier)
        
        navCon.setViewControllers([vc!], animated: true)
        
    }
    static func getMainStoryboardForDevice()->UIStoryboard{
        
        
        let devStr = GlobalStaticMethods.getDeviceTypeStr()
        let boardName = "Main-\(devStr)"
        
        return UIStoryboard(name: boardName, bundle: nil)
        
        
        
        
        
    }
    
    static func setRootViewControllerObj(controller : UIViewController){
        
        
        UIApplication.shared.keyWindow?.rootViewController = controller
    }
    
    
    static func PopToViewControllerObj(controller : UIViewController){
        
        
        UIApplication.shared.keyWindow?.rootViewController?.popoverPresentationController
        
        
    }
//    static func pushViewContolerObjOnNavController(controller : UIViewController , navController : UINavigationController) {
//        
//        
//    }
    
    
    static func getStoryboardName()->String{
        let devStr = GlobalStaticMethods.getDeviceTypeStr()
        
        return devStr
        
    }
    
    
    
    static func getStoryBoardForController(identifier:String)->(storyboardObject :UIStoryboard , storyBoardName :String)?{
        
        
        
        var boardName : String = ""
        var arr = NSArray()
    
        
        if sharedInstance.ControllersArray == nil{
            
            var str = language.getCurrentLanguage()

            let plistControllers = Bundle.main.path(forResource: "controllers-\(str)", ofType: ".plist")
            
           // sharedInstance.ControllersArray
              arr = NSArray(contentsOfFile: plistControllers!)!
        }
        
        let predicate = NSPredicate(format: "name LIKE[c] %@", identifier)
        let filteredArr = arr.filtered(using: predicate)
        
        
        if filteredArr.count > 0{
            let dict = filteredArr[0] as! [String : String]
            let str = dict["sid"]!
//            let str = (filteredArr[0] as AnyObject).value("sid") as! String
            boardName = "\(str)-\(navigation.getStoryboardName())"
            
        }
        else{
            print("Controller not found in any storyboard")
            //            break
            
            
            
            return nil
            
        }
        
        
        return (UIStoryboard(name: boardName, bundle: nil) , boardName)
        
    }
    
    static func getStoryboad() ->UIStoryboard{
        
        
        let boardName = "\(navigation.getStoryboardName())"
        return UIStoryboard(name: boardName, bundle: nil)
    }
    
    
    
    
    
}


class Alert {
    static func showAlertMsgWithTitle(_ title : String , msg : String , btnActionTitle : String , viewController : UIViewController?, completionAction: @escaping (Void) -> Void ) -> Void{
        
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: btnActionTitle, style: .default) { (action) in
            
            completionAction(())
        }
        
        
        alertController .addAction(alertAction)
        if viewController != nil {
            viewController! .present(alertController, animated: true, completion: nil)
        }else{
            
            UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
        }

        
    }
//
//    
//
}

class plist{

    static func loadDataFromPlist (plistName : String) -> [[String : Any]]{
      
        
        if let path = Bundle.main.path(forResource: plistName, ofType: "plist") {
            
            //If your plist contain root as Array
            if let array = NSArray(contentsOfFile: path) as? [[String: Any]] {
                
                return array
            }
            
            ////If your plist contain root as Dictionary
            if let dic = NSDictionary(contentsOfFile: path) as? [String: Any] {
             
                return [dic]
            }
        }
    
    
        return [[:]]
}
    
    
    static func getCurrentProjectTitle()->String{
        let singleton = Singleton.sharedInstance
        
        switch singleton.currentProjectEnum {
            
        case RequestType.ApprovalRejectedVC.rawValue:
            return "Approval Rejected"
        case RequestType.ApprovedVC.rawValue:
            return "Approve"
        case RequestType.ReassignVC.rawValue:
            return "Re-Assign"
        case RequestType.RequestInfoVC.rawValue:
            return "Request Information"
            
        default:
            return "Approve"
        }
    }
    
    
    static func viewSettingsAsPerProject(  projectEnumm : RequestType)->NSDictionary{
        
        //   let single = Singleton() // yeh ais waja se nahi lia cause it was recalling the whole class again
        let single = Singleton.sharedInstance
        let dict : NSDictionary =  single.projectDataDictionary[projectEnumm.rawValue] as! NSDictionary
        
        
        
        return dict
    }
    
    
    
}

//class leftMenu{
//    
//    static let sharedInstance = leftMenu()
//    
//    var centerContainer: MMDrawerController?
//    
//    static func getMenuWidth()->CGFloat{
//        
//        if GlobalStaticMethods.isPhone() {
//            return 280
//        }
//        else{
//            if GlobalStaticMethods.isPadPro() {
//                return 568//0.6763285 * UIScreen.mainScreen().bounds.width
//            }
//            else{
//                
//                return 0.6763285 * UIScreen.main.nativeBounds.width
//            }
//        }
//        
//    }
//    
//    static func getMainViewController(sideDrawer : MMDrawerController)-> MMNavigationController{
//        
//        return sideDrawer.centerViewController as! MMNavigationController
//        
//        
//        
//    }
//    static func closeDrawer(sideDrawer : MMDrawerController){
//        
//        
//        sideDrawer.toggle(.left, animated: true, completion: nil)
//        
//        // sideDrawer.toggleDrawerSide(.Right, animated: true, completion: nil)
//    }
//    
//    static func leftMenuPanGestureDisableEnable(vc : UIViewController, sideDrawer : MMDrawerController, containsBackImage : Bool){
//        
//        if containsBackImage {
//            sideDrawer.openDrawerGestureModeMask = []
//            
//        }
//        else{
//            sideDrawer.openDrawerGestureModeMask = .all
//        }
//        
//        // navigation.PopToViewControllerObj(vc)
//    }
//    
//    
//    
//}

    


enum ChooseLanguageTypeEnum : Int{
    case kLanguageEnglish = 1
    case kLanguageArabic
    case kLanguageUrdu

}

enum LAError : Int {
    case AuthenticationFailed
    case UserCancel
    case UserFallback
    case SystemCancel
    case PasscodeNotSet
    case TouchIDNotAvailable
    case TouchIDNotEnrolled
}


enum RequestType : Int {
    case ApprovedVC = 0
    case ApprovalRejectedVC
    case RequestInfoVC
    case ReassignVC
}

enum Forms : String {
    case Popba = "pobpa"
    case Ppc = "ppc"
    case ProjectAccruals = "projectAccruals"
    case PurchaseRequisition = "purchaseRequisition"
    case Rfc = "rfc"
    case Vo = "vo"
    case SalaryCertificateApproval = "salaryCertificate"
    case ProjectStatusChangeApproval = "projectStatusChangeApproval"
    case LeaveAbsence = "leaveAbsence"
    case LeaveApproval = "leaveApproval"
    case Journal = "journal"
    case iExpenseApproval = "iExpenseApproval"
    case BudgetApproval = "budgetApproval"
}


enum tabs : String {
    case detail = "details"
    case items = "items"
    case attachments = "attachments"
    case approval = "approval"
    case epsDetail = "eps detail"
    
    
}

enum cellTypes : Int {
    case titleCell = 0
    case imagesCell
    case descriptionCell
    case subDescriptionCell
    case doubleDescriptionCell
    case detailDescriptionCell
    case attachmentCell
    case epsDetail
    
}

enum userTypes : Int {
    case labManager = 1
    case labTechnician = 2
    case labSupervisor = 3
    case Instructor = 9
}

enum tabsEnum : Int {
    case tabHome = 0
    case tabHotOffers
    case tabCart
    case tabSearch
    case tabProfile
}

enum courseDetailVariations : Int{
    //    case first = 0 //1,2,3,,5,8 //academy
    //    case second  //1,2,4,5,8//academy
    //    case third // 1,2,3,6,8 with my curriculum in footer //curriculum
    //    case fourth // 1,2,3,6,8 with academy course
    //    case fifth //1,2,5(only enrol),8 //academy
    //    case  six // 1,2,5(only enrol),video,8(one button take assesment) //academy
    //    case seven // 1,2,5(only enroll),8 with my curriculum in footer
    //    //   case eight  // 1,2,5(only enroll),8 with my academy in footer //academy
    
    
    case inClassBasicNotEnrolled = 1 //1,2,3,6,8    in-class basic variation when not enrolled
    case inClassRequestForEnrolled=2// 1,2,3,5,8 in-class location selected variation when not enrolled .... Pre Material Disable
    case inClassEnrolledApproved = 3 // 1,2,4,5,8 in-class enrolled .... Pre Material Disable
    case inClassLocationSelected = 4 // 1,2,3,5,8 in-class location selected variation when not enrolled .. Post Material Disable
    
    
    case onlineClassBasicNotEnrolled //1 , 2 , 5( Enroll button Only ) ,8
    case onlineClassEnrolled // 1,2,5(only Enroll),7,8
}


enum statusCode : Int {
    
    case success = 1000
    case invalidSession = 1001
    case noRecordFound = 1002
    case notAvailableForUserType = 1003
    case updatePassword = 1004
    case invalidRequest = 1006
    case passwordEmpty = 1007
    case uniqueIdentifier = 1008
    case sessionToken  = 1009
    case addedToFav = 1010
    case removedFromFav = 1011
    case alreadyAddedToFav = 1012
}

enum statusCodeMessages : String{
    case success = "Success"
    case invalidSession = "Invalid Session ID"
    case noRecordFound = "No record found"
    case notAvailableForUserType = "Not available for this usertype"
    case updatePassword = "Password needs to be updated"
    case invalidRequest = "Invalid request"
    case passwordEmpty = "password is empty"
    case uniqueIdentifier = "Unique identifier should not be empty"
    case sessionToken  = "sessionToken is missing"
    case addedToFav = "Property added to favourite list."
    case removedFromFav = "Property removed from favourite list."
    case alreadyAddedToFav = "Property alraedy added to favourite list."
}



enum registerPropertyFieldTypeEnum : Int{
    case kTxtField = 1
    case kDropDown
    case kButton
}

enum txtFieldTypeEnum : Int{
    case name = 0
    case email = 1
    case password = 2
    case phone = 3
    case optional = 4
}

enum courseTypeEnum : Int{
    case online = 1
    case inClass = 2
    
}

enum  CellType: String {
    case priceCellHeader = "PriceHeaderCell",
    brandCell       = "brandCell",
    priceCell       = "priceCell",
    colorCell       = "colorCell",
    productCell     = "productCell",
    saveOrderCell   = "saveOrderCell",
    Unknown
    
}


enum  GetIndexTableView: String {
    case price = "price",
    brand   = "brand",
    size   = "size",
    color   = "color",
    discount   = "discount",
    
    Unknown
    
}

enum getCollectionIndex : Int {
    
    case price = 0
    case brand
    case size
    case color
    case discount
    
}

enum getTypes : Int {
    
    case privacyPolicy = 0
    case termsCondition
    case tutorial

    
}




class registerPropertyModelStruct {
    var cellType : registerPropertyFieldTypeEnum
    var keyboardType : UIKeyboardType
    var validationType : txtFieldTypeEnum
    var value : String
    var placeholder : String
    var leftImage : String
    var rightImage : String
    var txtfieldValue : String
    var descriptionLabel : String
    
    
    init(cellType: registerPropertyFieldTypeEnum, keyboardType: UIKeyboardType, validationType: txtFieldTypeEnum, value:String, placeholder: String, leftImage : String, rightImage : String, txtfieldValue : String, descriptionLabel : String){
        self.cellType = cellType
        self.keyboardType = keyboardType
        self.validationType = validationType
        self.value = value
        self.placeholder = placeholder
        self.leftImage = leftImage
        self.rightImage = rightImage
        self.txtfieldValue = txtfieldValue
        self.descriptionLabel = descriptionLabel
        
    }
}

class myProfileModelClass {
    var cellType : registerPropertyFieldTypeEnum
    var keyboardType : UIKeyboardType
    var validationType : txtFieldTypeEnum
    var value : String
    var placeholder : String
    var leftImage : String
    var rightImage : String
    var txtfieldValue : String
    var lblTip : String
    var userInteraction : Bool
    
    init(cellType: registerPropertyFieldTypeEnum, keyboardType: UIKeyboardType, validationType: txtFieldTypeEnum, value:String, placeholder: String, leftImage : String, rightImage : String, txtfieldValue : String, lblTip : String, userInteraction: Bool ){
        self.cellType = cellType
        self.keyboardType = keyboardType
        self.validationType = validationType
        self.value = value
        self.placeholder = placeholder
        self.leftImage = leftImage
        self.rightImage = rightImage
        self.txtfieldValue = txtfieldValue
        self.lblTip = lblTip
        self.userInteraction = userInteraction
    }
}



class attendanceModelClass {
    var courseType : Int?
    var title : String
    var courseID : Int
    var shortDescription : String?
    var locationID : Int?
    var timeDate : String?
    var location : String?
    var longDescription : String?

    
    
    init(courseType : Int , title : String , courseID : Int , shortDescription : String){
        
        
        self.courseType = courseType
        self.title = title
        self.courseID = courseID
        self.shortDescription = shortDescription
        
        
    }
    
    init(courseType : Int , title : String , courseID : Int , location : String , locationID : Int , timeDate : String){
        
        
        self.courseType = courseType
        self.title = title
        self.courseID = courseID
        self.location = location
        self.locationID = locationID
        self.timeDate =  timeDate
        
        
    }
    
    init(courseType : Int , title : String , courseID : Int , location : String , locationID : Int , longDescription : String){
        
        
        self.title = title
        self.courseID = courseID
        self.location = location
        self.locationID = locationID
        self.longDescription =  longDescription
        
        
    }

}


/*class courseDetailModelStruct {
 var status : String
 var statusCode : String
 var statusMessage : String
 var courseId : String
 var title : String
 var shortDescription : String
 var courseType : String
 var totalCount : String
 var locId : String
 var locText : String
 var locTimeDate : String
 var locLat : String
 var locLong : String
 
 var locAttending : String
 var locInstructor : String
 var locClassSize : String
 var locHospresp : String
 var locCourseType : String
 var locCourseDuration : String
 var expired : String
 var description : String
 var enrolled : String
 var showAssessment : String
 var showFeedback : String
 var certificate : String
 var videoThumbnail : String
 var videoLink : String
 var disablePreMaterial : String
 var disablePostMaterial : String
 var preMaterial : String
 var postMaterial : String
 
 
 init(status : String , statusMessage : String , statusCode : String , courseId : String, title : String , shortDescription : String, courseType : String , totalCount : String , locId : String , locText: String , locTimeDate: String , locLat : String , locLong: String ,  locAttending : String , locInstructor : String , locClassSize : String , locHospresp : String , locCourseType : String , locCourseDuration : String , expired : String , description : String , enrolled : String , showAssessment : String , showFeedback : String , certificate : String , videoThumbnail : String , videoLink : String , disablePreMaterial  : String , disablePostMaterial : String , preMaterial : String , postMaterial : String  ){
 
 
 
 self.status = status
 self.statusMessage = statusMessage
 self.statusCode = statusCode
 self.courseId = courseId
 self.title = title
 self.shortDescription = shortDescription
 self.courseType = courseType
 self.totalCount = totalCount
 self.locId = locId
 self.locText = locText
 self.locTimeDate = locTimeDate
 //        self.status = status
 //        self.status = status
 //        self.status = status
 //        self.status = status
 //        self.status = status
 //        self.status = status
 //        self.status = status
 //        self.status = status
 //        self.status = status
 //        self.status = status
 //        self.status = status
 //        self.status = status
 //        self.status = status
 //        self.status = status
 //        self.status = status
 
 
 
 }
 }
 
 DONOT DELETE THIS
 
 
 */


//func isValidate() -> Bool {
//    
//    if txtCoachName.text == "" {
//        Common.showAlert("Alert", message: "Please enter your name")
//        return false
//    }
//    //        if Common.isAlphabets(txtCoachName.text!) == false {
//    //            Common.showAlert("Alert", message: "Please enter valid name")
//    //            return false
//    //        }
//    
//    if txtEmail.text == "" {
//        Common.showAlert("Alert", message: "Please enter your email")
//        return false
//    }
//    
//    if Common.validateEmail(txtEmail.text!) == false {
//        Common.showAlert("Alert", message: "Please enter valid email")
//        return false
//    }
//    
//    if txtPhone.text == "" {
//        Common.showAlert("Alert", message: "Please enter your contact number")
//        return false
//    }
//    
//    if Common.validatePhone(txtPhone.text!) == false {
//        Common.showAlert("Alert", message: "Please enter valid contact number")
//        return false
//    }
//    if txtPhone.text?.characters.count < 8 {
//        Common.showAlert("Alert", message: "Please enter valid contact number")
//        return false
//    }
//    
//    
//    
//    return true
//}
