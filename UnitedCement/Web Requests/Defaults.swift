//
//  Defaults.swift
//  sadalia
//
//  Created by Farooq on 7/3/18.
//  Copyright © 2018 Farooq. All rights reserved.
//

import Foundation

struct Defaults{
    static let userDefaults = UserDefaults.standard
    
    static func setLanguage(isArabic:Bool){
        userDefaults.set(isArabic, forKey: "isArabic")
        userDefaults.synchronize()
    }
    
    static var isArabicLanguage :Bool {
        if let lang = Defaults.userDefaults.value(forKey: "isArabic") as? Bool{
            return lang
        }
        return false
    }
//    static func saveUserModel(user: UserModel){
//        let encodedData: Data = NSKeyedArchiver.archivedData(withRootObject: user)
//        userDefaults.set(encodedData, forKey: "userData")
//        userDefaults.synchronize()
//    }
//    static func getUser()-> UserModel?{
//        let decoded  = Defaults.userDefaults.object(forKey: "userData") as! Data
//        let decodedData = NSKeyedUnarchiver.unarchiveObject(with: decoded) as! UserModel
//        return decodedData
//    }
//    static func userLoggedIn() -> Bool{
//        if let decoded  = Defaults.userDefaults.object(forKey: "userData") as? Data{
//            if (NSKeyedUnarchiver.unarchiveObject(with: decoded) as? UserModel) != nil{
//                return true
//            }else{
//                return false
//            }
//        }else{
//            return false
//        }
//
//    }
//    static func clearUser() {
//        UserModel.currentUser = nil
//        UserDefaults.standard.removeObject(forKey: "userData")
//    }
}
