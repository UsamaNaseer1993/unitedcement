//
//  AppManager.swift
//  Spark
//
//  Created by Invision-MacBookPro-Farooq on 01/11/2018.
//  Copyright © 2018 Mubeen Raza Qazi. All rights reserved.
//

import UIKit
enum StatusError: Int{
    case notAuthorized = 400
    func desc()->String{
        switch self{
        case .notAuthorized:
            return "You are not authorized to login at this time."
        }
    }
}

class AppManager: NSObject {
    static var shared = AppManager()
    private override init() {}
    var window:UIWindow?? {
        return UIApplication.shared.delegate?.window
    }
    var storyboard:UIStoryboard{
        return UIStoryboard(name: "Main", bundle: .main)
    }
    
//    func handleTokenExpiry(){
//        UIApplication.inputViewController()?.showAlert(title: "Session Expired!", message: "Please login again.", completion: {
//            self.showLogin()
//        })
//
//
//    }
    func holdSplash(){
        let launchStoryBoard = UIStoryboard(name: "LaunchScreen", bundle: nil)
        let splash = launchStoryBoard.instantiateViewController(withIdentifier: "SplashViewController")
        window??.rootViewController = splash
    }
    func showMain(){
        let signinVC = storyboard.instantiateViewController(withIdentifier: "CompanyListViewController")
        
        let mainVC = storyboard.instantiateInitialViewController() as! UINavigationController
        mainVC.viewControllers.append(signinVC)
        
        window??.rootViewController = mainVC
        
        
    }
    
    func showLogin(){
//        clear(window: window)
        let signinVC = storyboard.instantiateViewController(withIdentifier: "LoginViewController")
        let mainVC = storyboard.instantiateViewController(withIdentifier: "MainNavigationController") as! UINavigationController
        window??.rootViewController = mainVC
    }
    
    func clear(window: UIWindow??) {
        window??.subviews.forEach { $0.removeFromSuperview() }
    }
    
//    func checkSession(){
//        if let user = Defaults.getUser(){
//            let loginData = Defaults.getEmailPass()
//            APICall.requestLogin(email: loginData.email, password: loginData.pass, success: { (success) in
//                self.showMain()
//            }, failure: { (err) in
//                self.showLogin()
//            })
//
//        }else{
//            showLogin()
//        }
//    }
    
}

