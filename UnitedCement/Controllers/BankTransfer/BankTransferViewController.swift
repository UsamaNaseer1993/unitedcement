//
//  BankTransferViewController.swift
//  UnitedCement
//
//  Created by Usama Naseer on 12/11/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit
import SideMenu

class BankTransferViewController: UIViewController {
    
    @IBOutlet weak var IbanTextField: UITextField!
    @IBOutlet weak var accountNumberTextField: UITextField!
    @IBOutlet weak var accountNameTextField: UITextField!
    @IBOutlet weak var IbanLb: UILabel!
    @IBOutlet weak var accountNumberLb: UILabel!
    @IBOutlet weak var AccountNameLb: UILabel!
    @IBOutlet weak var arrowButton:UIButton!
    @IBOutlet weak var sideMenuButton:UIButton!
    @IBOutlet weak var chooseUCICBankTextField: UITextField!
    @IBOutlet weak var custBankTextField:UITextField!
    @IBOutlet weak var custAccNumberTextField:UITextField!
    @IBOutlet weak var transferDateTextField:UITextField!
    @IBOutlet weak var amountTransferedTextField:UITextField!
    @IBOutlet weak var payRefTextField:UITextField!
    @IBOutlet weak var uploadPaidBandSlipButton:UIButton!
    @IBOutlet weak var bankTransferLabel: UILabel!
    @IBOutlet weak var yourOrderLabel: UILabel!
    @IBOutlet weak var proceedButton: UIButton!
    @IBOutlet weak var BankInstructionLabel: UILabel!
    @IBOutlet weak var pleaseTransferLabel: UILabel!
    @IBOutlet weak var chooseUCIClabel: UILabel!
    @IBOutlet weak var customerBankLabel: UILabel!
    @IBOutlet weak var custAccNumLabel: UILabel!
    @IBOutlet weak var transferDateLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var payRefLabel: UILabel!
    var paymentId : Int?
    var order : Bool?
    var orderId : Int?
    var bankList : [bankDetailModel]?
    var amount : Double?
    var datePicker = UIDatePicker()
    @IBOutlet weak var bankSlipImage: UIImageView!
    var bankname : [String] = []
    var pickerView = UIPickerView()
    override func viewWillAppear(_ animated: Bool) {
        UILabel.appearance().font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle(rawValue: "Arabic")).withSize(14)
        super.viewWillAppear(true)
        EngToArabic()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        callBankList()
        // Do any additional setup after loading the view.
    }
    
    func EngToArabic()
    {
        if Defaults.isArabicLanguage
        {
            bankTransferLabel.text = "تحويل مصرفي"
            BankInstructionLabel.text = "تعليمات التحويل المصرفي"
            pleaseTransferLabel.text = "يرجى تحويل المبلغ الإجمالي إلى الحساب المصرفي التالي"
            chooseUCIClabel.text = "بنك شركة الاسمنت المتحدة"
            chooseUCIClabel.textAlignment = .right
            AccountNameLb.text = "أسم الحساب"
            AccountNameLb.textAlignment = .right
            accountNumberLb.text = "رقم الحساب"
            accountNumberLb.textAlignment = .right
            IbanLb.text =  "رقم ايبان"
            IbanLb.textAlignment = .right
            customerBankLabel.text = "اسم المحول"
            customerBankLabel.textAlignment = .right
            custAccNumLabel.text = "حساب مصرفي العميل"
            custAccNumLabel.textAlignment = .right
            transferDateLabel.text = "تاريخ التحويل"
            transferDateLabel.textAlignment = .right
            amountLabel.text = "المبلغ المحول"
            amountLabel.textAlignment = .right
            payRefLabel.text = "رقم الإيصال "
            payRefLabel.textAlignment = .right
            uploadPaidBandSlipButton.setImage(nil, for: .normal)
            uploadPaidBandSlipButton.backgroundColor = UIColor(named: "DarkBlue")
            uploadPaidBandSlipButton.setTitle("ارفاق نسخة من ايصال البنك (اختياري)", for: .normal)
            proceedButton.setImage(nil, for: .normal)
            proceedButton.setTitle("تقدم", for: .normal)
            proceedButton.backgroundColor = UIColor(named: "DarkBlue")
            yourOrderLabel.text = "يرجو اكمال الدفع ليتم شحن البضاعة"
          //  uploadPaidBandSlipButton.setTitle("تحميل زلة البنك", for: .normal)
           // proceedButton.setTitle("تقدم", for: .normal)
        }
        
    }
    
    func callBankList() {
        amountTransferedTextField.text = amount?.converToString
        ApiCall.bankList { (banks, logout, error) in
            guard logout == false else {
                return
            }
            for bank in banks ?? [] {
                if Defaults.isArabicLanguage {
                       self.bankname.append(bank.bankName_Ar)
                } else {
                       self.bankname.append(bank.bankName)
                }
             
            }
            self.bankList = banks
            if Defaults.isArabicLanguage {
                self.chooseUCICBankTextField.text = self.bankList?[0].bankName_Ar
                self.chooseUCICBankTextField.textAlignment = .right
                self.accountNameTextField.text = self.bankList?[0].accountName_Ar
                self.accountNameTextField.textAlignment = .right
            } else {
                self.chooseUCICBankTextField.text = self.bankList?[0].bankName
                self.accountNameTextField.text = self.bankList?[0].accountName
            }
            
            self.accountNumberTextField.text = self.bankList?[0].accountNumber
            self.IbanTextField.text = self.bankList?[0].iban
           
            
        }
    }
    func initViews() {
        showDatePicker()
        setupmenu()
        pickerView.dataSource = self
        pickerView.delegate = self
        chooseUCICBankTextField.text = "Select Account"
        chooseUCICBankTextField.inputView = pickerView
    }
    @IBAction func arrowButtonTapped(_sender:UIButton)
    {
        if order == true {
            popBack(1)
             self.tabBarController?.selectedIndex = 0
        } else {
             popBack(1)
        }
        
    }
    @IBAction func sideMenuButtonTapped(_sender:UIButton) {
        present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
    }
    @IBAction func uploadPaidBankSlipButtonTapped(_sender:UIButton) {
        ImagePickerManager().pickImage(self){ image in
            //here is the image
            UILabel.appearance().font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle(rawValue: "Arabic")).withSize(14)
            self.bankSlipImage.image = image
            
        }
    }
    func chckTrucks(){
        ApiCall.trucks { (trucks, logout, error) in
            if logout == true {
                self.presentMain()
            } else {
                if trucks != 0 {
                    if let tabItems = self.tabBarController?.tabBar.items {
                        let tabItem = tabItems[1]
                        tabItem.badgeValue = trucks?.converToString
                    }
                } else {
                    if let tabItems = self.tabBarController?.tabBar.items {
                        let tabItem = tabItems[1]
                        tabItem.badgeValue = nil
                    }
                }
            }
        }
    }
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        // formatter.dateFormat = "EEEE, MMM d, yyyy"
        formatter.dateFormat = "yyyy-MM-dd"
        transferDateTextField.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    func showDatePicker(){
        //Formate Date
        
        datePicker.datePickerMode = .date
        //            datePicker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        transferDateTextField.inputAccessoryView = toolbar
        transferDateTextField.inputView = datePicker
        
        
    }
    
    @IBAction func proceedAction(_ sender: Any) {
        //    switchScreen(identifier: "BankTransfer2ViewController")
        if
            chooseUCICBankTextField.text != "Select Account",
//            custBankTextField.text != "",
//            custAccNumberTextField.text != "",
            transferDateTextField.text != "",
            amountTransferedTextField.text != "",
            payRefTextField.text != "" {
            
            if bankSlipImage.image != nil {
                
                ApiCall.paidSlip(image: bankSlipImage.image!, paymentId: ((self.paymentId?.converToString)!)) { (status, error) in
                    if status == 200 {
                        print(status ?? "") 
                    }
                }
            }
            
            ApiCall.bankData(orderId: orderId!,id: paymentId!, companybank: chooseUCICBankTextField.text!, customerBank: custBankTextField.text!, custAcc: custAccNumberTextField.text!, transferDate: transferDateTextField.text!, amountTransfer: Double(amountTransferedTextField.text!)!, paymentRef: payRefTextField.text!) { (msg, logout, error) in
                guard logout == false else {
                    self.presentMain()
                    return
                }
                var title = Constants.MESSAGE
                var msg = "Payment Details Updated Succcesfully"
                if Defaults.isArabicLanguage {
                    title = Constants.MESSAGE_AR
                    msg = "تم تحديث تفاصيل الدفع بنجاح"
                }
                self.showAlert(title: title, message: msg, completion: {
                    //self.tabBarController?.selectedIndex = 0
                 // self.chckTrucks()
                    self.popBack(1)
                    self.switchScreen(identifier: "myorders")
                })
            }
        } else {
            var title = Constants.MESSAGE
            var msg = "Please fill out all fields"
            if Defaults.isArabicLanguage {
                title = Constants.MESSAGE_AR
                msg = "يرجى ملء جميع الحقول"
            }
            self.showAlert(title: title, message: msg)
        }
    }
}

extension BankTransferViewController :  UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return bankname.count
        
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return bankname[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if Defaults.isArabicLanguage {
            self.chooseUCICBankTextField.textAlignment = .right
            self.chooseUCICBankTextField.text = self.bankList?[row].bankName_Ar
            self.accountNameTextField.textAlignment = .right
            self.accountNameTextField.text = self.bankList?[row].accountName_Ar
        } else {
            self.chooseUCICBankTextField.text = self.bankList?[row].bankName
            self.accountNameTextField.text = self.bankList?[row].accountName
        }
        accountNumberTextField.text = self.bankList?[row].accountNumber
        IbanTextField.text = self.bankList?[row].iban
        self.view.endEditing(false)
    }
    
}
