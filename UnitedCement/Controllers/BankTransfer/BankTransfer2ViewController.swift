//
//  BankTransfer2ViewController.swift
//  UnitedCement
//
//  Created by Usama Naseer on 12/11/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit

class BankTransfer2ViewController: UIViewController {
    
    
    @IBOutlet weak var arrowButton:UIButton!
    @IBOutlet weak var sideMenuButton:UIButton!
    @IBOutlet weak var cardHolderNameTextField:UITextField!
    @IBOutlet weak var cardNumberTextField:UITextField!
    @IBOutlet weak var securityPinTextField:UITextField!
    @IBOutlet weak var expiryDateTextField:UITextField!
    @IBOutlet weak var cardHolderLabel: UILabel!
    @IBOutlet weak var BankTransferLabel: UILabel!
    @IBOutlet weak var cardNumLabel: UILabel!
    @IBOutlet weak var securityPinLabel: UILabel!
    @IBOutlet weak var expiryLabel: UILabel!
    @IBOutlet weak var payButton:UIButton!
    @IBOutlet weak var bankTransferInstruction: UILabel!
    @IBOutlet weak var PleaseEnterLabel: UILabel!
    @IBOutlet weak var YourOrderLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        EngToArabic()
    }
    func EngToArabic()
    {
        if Defaults.isArabicLanguage
        {
            BankTransferLabel.text = "تحويل مصرفي"
            bankTransferInstruction.text = "تعليمات التحويل المصرفي"
            PleaseEnterLabel.text = "يرجى إدخال التفاصيل التالية"
            cardHolderLabel.text = "إسم صاحب البطاقة"
            cardHolderLabel.textAlignment = .right
            cardNumLabel.text = "رقم البطاقة"
            cardNumLabel.textAlignment = .right
            securityPinLabel.text = "دبوس الأمان"
            securityPinLabel.textAlignment = .right
            expiryLabel.text = "تاريخ الانتهاء"
            expiryLabel.textAlignment = .right
            YourOrderLabel.text = "لن يتغير طلبك حتى نتلقى الدفع"
            payButton.setTitle("دفع", for: .normal)
        }
    }
    @IBAction func arrowButtonTapped(_sender:UIButton)
    {
        popBack(1)
    }
    @IBAction func sideMenuButtonTapped(_sender:UIButton)
    {}
    @IBAction func payButtonTapped(_sender:UIButton)
    {}
    
    
}
