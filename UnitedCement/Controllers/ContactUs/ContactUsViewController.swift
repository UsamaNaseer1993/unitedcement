//
//  ContactUsViewController.swift
//  UnitedCement
//
//  Created by Usama Naseer on 12/11/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit
import SideMenu

class ContactUsViewController: UIViewController {
    @IBOutlet weak var contactLabel: UILabel!
  
    @IBOutlet weak var phnLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var webLabel: UILabel!
    @IBOutlet weak var phoneOneBtn: UIButton!
    @IBOutlet weak var phoneTwoBtn: UIButton!
    @IBOutlet weak var websiteBtn: UIButton!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UILabel.appearance().font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle(rawValue: "Arabic")).withSize(14)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        engToArabic()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupmenu()
        // Do any additional setup after loading the view.
    }
    
    func engToArabic()
    {
        if Defaults.isArabicLanguage
        {
            contactLabel.text = "اتصل"
            phnLabel.text = "هاتف"
            emailLabel.text = "البريد الإلكتروني"
            webLabel.text = "موقع الكتروني"
            
        }
    }
  
    func phoneCall( phoneNumber:String) {
        
        if let callURL:URL = URL(string: phoneNumber) {
            
            let application:UIApplication = UIApplication.shared
            
            if (application.canOpenURL(callURL)) {
                let alert = UIAlertController(title: "Your Title", message: "Do you want call that number?", preferredStyle: .alert)
                let callAction = UIAlertAction(title: "Call", style: .default, handler: { (action) in
                    application.open(callURL, options: [:], completionHandler: nil)
                })
                let noAction = UIAlertAction(title: "No", style: .cancel, handler: { (action) in
                    print("Canceled Call")
                })
                alert.addAction(callAction)
                alert.addAction(noAction)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func sideMenu(_ sender: Any) {
        present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
    }
    @IBAction func phoneOneBtnTapped(_ sender: Any) {
        
        if Defaults.isArabicLanguage{
            self.showAlert(title: "تؤكد", message: "هل تريد الاتصال بهذا الرقم؟")
        }
        else
        {
        self.showAlert(title: "Confirm", message: "Do you want to call this Number")
        phoneCall(phoneNumber: "+966 55 863 1475")
//        guard let url = URL(string: "+966 55 863 1475") else {return}
//        UIApplication.shared.open(url)
    }
}
    @IBAction func phoneTwoBtnTapped(_ sender: Any) {
        if Defaults.isArabicLanguage{
            self.showAlert(title: "تؤكد", message: "هل تريد الاتصال بهذا الرقم؟")
        }
        else
        {
        self.showAlert(title: "Confirm", message: "Do you want to call this Number")
        guard let url = URL(string: "+966 12 676 8999") else {return}
        UIApplication.shared.open(url, options: [:])
        }
    }
    @IBAction func webSiteBtnTapped(_ sender: Any) {
        
        if Defaults.isArabicLanguage{
            self.showAlert(title: "تؤكد", message: "افتح هذا الموقع؟")
        }
        else{
        self.showAlert(title: "Confirm", message: "Open this website?")
        if let url = URL(string: "www.unitedcement.com.sa") {
            UIApplication.shared.open(url, options: [:])
        }
    }
    }
    
}
