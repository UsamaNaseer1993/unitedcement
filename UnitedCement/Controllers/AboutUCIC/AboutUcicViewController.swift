//
//  AboutUcicViewController.swift
//  UnitedCement
//
//  Created by Usama Naseer on 12/11/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit
import SideMenu

class AboutUcicViewController: UIViewController {
    
    @IBOutlet weak var aboutLabel: UILabel!
    @IBOutlet weak var msgCEOLabel: UILabel!
    @IBOutlet weak var descrptionBlueLabel: UILabel!
    @IBOutlet weak var descriptionWhiteLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupmenu()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func backbuttonAction(_ sender: Any) {
        popBack(1)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        EngToArabic()
    }
    
    func EngToArabic()
    {
        if Defaults.isArabicLanguage
        {
            aboutLabel.text = "حول UCIC"
            msgCEOLabel.text = "رسالة من الرئيس التنفيذي"
            descrptionBlueLabel.text = "شركة المتحدة للأسمنت الصناعية تم تأسيس شركة سعودية عام 2013 م تحت رقم تجاري (4030256672). تمتلك الشركة المتحدة للأسمنت الصناعية أحدث مصنع تم بناؤه في المملكة العربية السعودية وفي المنطقة الغربية وقريبًا من مكة المكرمة. الطاقة الإنتاجية هي 2 مليون طن من الاسمنت في العام الماضي"
            descriptionWhiteLabel.text = "حصلت شركة المتحدة للأسمنت الصناعية (UCIC) على الامتياز الذي ستدعمه حكومة خادم الحرمين الشريفين ، من خلال الحصول على ترخيص بناء التنازل في مدينة الليث لتصنيع أنواع مختلفة من الأسمنت التي تتصادف مع العالم."
            
        }
    }
    
}
