//
//  SettingViewController.swift
//  UnitedCement
//
//  Created by Usama Naseer on 12/11/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit
import SideMenu

class SettingViewController: UIViewController {
    
    @IBOutlet weak var homeButton:UIButton!
    @IBOutlet weak var updateProfileButton:UIButton!
    @IBOutlet weak var updatePassButton:UIButton!
    @IBOutlet weak var settingLabel: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        EngToArabic()
    }
    func EngToArabic()
    {
        if Defaults.isArabicLanguage
        {
            settingLabel.text = "ضبط"
            updateProfileButton.setTitle("تحديث الملف", for: .normal)
            updatePassButton.setImage(UIImage(named: "upPassArabic"), for: .normal)
//            updatePassButton.setTitle("تطوير كلمة السر", for: .normal)
        }
    }
    @IBAction func homeButtonTapped(_sender:UIButton)
    {
        self.popBack(1)
    }
    
    @IBAction func updateProfileButtonTapped(_sender:UIButton)
    {
        switchScreen(identifier: "UpdateProfileViewController")
    }
    @IBAction func updatePassButtonTapped(_sender:UIButton)
    {
        switchScreen(identifier: "UpdatePasswordViewController")
    }
    
}
