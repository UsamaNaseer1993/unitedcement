//
//  UpdateProfileViewController.swift
//  UnitedCement
//
//  Created by Usama Naseer on 12/11/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit

class UpdateProfileViewController: UIViewController {
    
    @IBOutlet weak var arrowButton:UIButton!
    @IBOutlet weak var sideMenuButton:UIButton!
    @IBOutlet weak var firstNameTextField:UITextField!
    @IBOutlet weak var lastNameTextField:UITextField!
    @IBOutlet weak var mobileTextField:UITextField!
    @IBOutlet weak var emailTextField:UITextField!
    @IBOutlet weak var companyNameTextField:UITextField!
    @IBOutlet weak var updateProfileLabel: UILabel!
    @IBOutlet weak var updateProfileButton: UIButton!
    var profiles : [ProfileModel] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ApiCall.getProfile { (profiles, logout, error) in
            if logout! {
                self.presentMain()
            } else {
                self.profiles = profiles ?? []
                self.firstNameTextField.text = self.profiles[0].firstname
                self.lastNameTextField.text = self.profiles[0].lastname
                self.emailTextField.text = self.profiles[0].email
                self.mobileTextField.text = self.profiles[0].phone
                self.companyNameTextField.text = self.profiles[0].company
            }
        }
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        EngToArabic()
    }
    
    func EngToArabic()
    {
        if Defaults.isArabicLanguage{
            updateProfileLabel.text = "تحديث الملف"
            updateProfileButton.setImage(UIImage(named: "prfileArabic"), for: .normal)
          //  updateProfileButton.setTitle("تحديث الملف", for: .normal)
            firstNameTextField.placeholder = "الاسم الاول"
            firstNameTextField.textAlignment = .right
            lastNameTextField.placeholder = "الكنية"
            lastNameTextField.textAlignment = .right
            mobileTextField.placeholder = "رقم الهاتف المحمول"
            mobileTextField.textAlignment = .right
            emailTextField.placeholder = "البريد الإلكتروني"
            emailTextField.textAlignment = .right
            companyNameTextField.placeholder = "اسم الشركة"
            companyNameTextField.textAlignment = .right
        }
    }
    @IBAction func arrowButtonTapped(_sender:UIButton)
    { popBack(1) }
    @IBAction func sideMenuButtonTapped(_sender:UIButton)
    {}
    @IBAction func updateProfileButtonTapped(_sender:UIButton)
    { if (firstNameTextField.text?.isBlank)!  ||
        (lastNameTextField.text?.isBlank)! ||
        (mobileTextField.text?.isBlank)! ||  (emailTextField.text?.isBlank)! {
        showAlert(title: Constants.MESSAGE, message: "Fields cannot be empty")
    } else {
        ApiCall.updateProfile(firstname: firstNameTextField.text!, lastname: lastNameTextField.text!, mobile: mobileTextField.text!, email: emailTextField.text!, companyname: companyNameTextField.text!) { (msg, error) in
            if error == nil  && msg != nil {
                self.showAlert(title: Constants.MESSAGE, message: msg, completion: {
                    self.popBack(2)
                })
            }
            else {
                print(error!.description)
            }
        }
        }
    }
}
