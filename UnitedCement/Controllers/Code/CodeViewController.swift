//
//  CodeViewController.swift
//  UnitedCement
//
//  Created by Usama Naseer on 12/11/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit
import KWVerificationCodeView

class CodeViewController: UIViewController{
    
    var email: String?
    @IBOutlet weak var verificationCode: KWVerificationCodeView!
    @IBOutlet weak var entercodeLabel:UILabel!
    @IBOutlet weak var codeLabel:UILabel!
    @IBOutlet weak var textView:UIView!
    @IBOutlet weak var submitButton:UIButton!
    @IBOutlet weak var sideMenuButton:UIButton!
    @IBOutlet weak var arrowButton:UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        engToArabic()
    }
    
    func engToArabic()
    {
        if Defaults.isArabicLanguage
        {
            codeLabel.text = "الشفرة"
            entercodeLabel.text = "ادخل رمزك"
            submitButton.setTitle("إرسال",for: .normal)
        }
    }
    
    @IBAction func arrowButtonTapped(_sender:UIButton) {
        self.popBack(1)
    }
    
    @IBAction func submitButtonTapped(_sender:UIButton) {
        // switchScreen(identifier: "ResetPasswordViewController")
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc : ResetPasswordViewController = mainStoryboard.instantiateViewController(withIdentifier: "ResetPasswordViewController") as! ResetPasswordViewController
        vc.email = email
        vc.code = verificationCode.getVerificationCode()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}
