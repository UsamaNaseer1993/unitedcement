//
//  PageViewController.swift
//  UnitedCement
//
//  Created by Usama Naseer on 18/01/2019.
//  Copyright © 2019 Usama Naseer. All rights reserved.
//

import UIKit

class PageViewController: UIPageViewController {
    var orderedViewControllers : [UIViewController] = []
    var  index : Int = 0
    var timerTest: Timer? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        timerTest = Timer.scheduledTimer(timeInterval: 4, target: self, selector: #selector(changeSlide), userInfo: nil, repeats: true)
        
        orderedViewControllers.append(self.newColoredViewController(name: "productpage2"))
        orderedViewControllers.append(self.newColoredViewController(name: "productpage3"))
         orderedViewControllers.append(self.newColoredViewController(name: "productpage4"))
         orderedViewControllers.append(self.newColoredViewController(name: "productpage5"))
        orderedViewControllers.append(self.newColoredViewController(name: "LoginOrRegisterViewController"))
        
        dataSource = self
        
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: true,
                               completion: nil)
        }
        // Do any additional setup after loading the view.
    }
  
    @objc func changeSlide()
    {
        let pageController = self
        print(pageController.orderedViewControllers[0])
        guard index < orderedViewControllers.count - 1  else {
            timerTest?.invalidate()
            timerTest = nil
            return
        }
        
        index += 1
        print("Change Slide \(index)")
        pageController.setViewControllers([orderedViewControllers[index]], direction: .forward, animated: true, completion: nil)
       
    }
   
  
    
    private func newColoredViewController(name: String) -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil) .
            instantiateViewController(withIdentifier: "\(name)")
    }
    
 
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = self.orderedViewControllers.lastIndex(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        index = previousIndex
        print("Previous Slide \(index)")
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.lastIndex(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        index = nextIndex
        print("Next Slide \(index)")
        return orderedViewControllers[nextIndex]
    }
    
  
    
}
