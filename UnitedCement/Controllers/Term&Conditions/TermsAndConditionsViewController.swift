//
//  TermsAndConditionsViewController.swift
//  UnitedCement
//
//  Created by Usama Naseer on 25/01/2019.
//  Copyright © 2019 Usama Naseer. All rights reserved.
//

import UIKit

class TermsAndConditionsViewController: UIViewController {
    @IBOutlet weak var termsAndConditionLabel: UILabel!
    
    @IBOutlet weak var termsLabel: UILabel!
    @IBOutlet weak var thebuyerLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        isArabic()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func backButton(_ sender: Any) {
        popBack(1)
    }
  
    func isArabic()
    {
        if Defaults.isArabicLanguage
        {
            termsAndConditionLabel.text = "الأحكام والشروط"
            termsAndConditionLabel.textAlignment = .center
            termsLabel.text = "الأحكام والشروط"
//            thebuyerLabel.text = "1-يقر المشتري بأن المعلومات التي قدمها صحيحة ودقيقة وكافية لتنفيذ الأمر." +
//                +"2- يقر المشتري بأهليته القانونية للتعاقد والشراء من البائع."
//                + "3- يقر المشتري بأن بطاقة الائتمان المستخدمة في عملية الشراء مسجلة باسمه."
//
//            + "4- يقر المشتري بأن البائع له الحق في مراجعة الأسعار وتعديلها حسب ما تقتضيه الشروط."
//       +    "5. يجب على المشتري التأكد من توفر الوسائل اللازمة لتسليم المنتج بسرعة ويكون مسؤولا وحدهم عن أي أضرار أو تأخير."
//        "6. يجب على المشتري أن يعلم البائع على الفور ، من خلال موقعه على الإنترنت ، باهتمامه بإلغاء الطلب. يحتفظ البائع بالحق في رفض الإلغاء إذا غادر الناقل المصنع أو في طريقه إلى نقطة التسليم."
            thebuyerLabel.text = "1- يقر المشتري بأن المعلومات التي قدمها صحيحة ودقيقة وكافية لتنفيذ الأمر.2- يقر المشتري بأهليته القانونية للتعاقد والشراء من البائع. 3-يقر المشتري بأن بطاقة الائتمان المستخدمة في عملية الشراء مسجلة باسمه.4- يقر المشتري بأن البائع له الحق في مراجعة الأسعار وتعديلها حسب ما تقتضيه الشروط.5. يجب على المشتري التأكد من توفر الوسائل اللازمة لتسليم المنتج بسرعة ويكون مسؤولا وحدهم عن أي أضرار أو تأخير. 6. يجب على المشتري أن يعلم البائع على الفور ، من خلال موقعه على الإنترنت ، باهتمامه بإلغاء الطلب. يحتفظ البائع بالحق في رفض الإلغاء إذا غادر الناقل المصنع أو في طريقه إلى نقطة التسليم."
        }
    }

}
