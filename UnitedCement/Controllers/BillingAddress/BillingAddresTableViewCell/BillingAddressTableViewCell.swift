//
//  BillingAddressTableViewCell.swift
//  UnitedCement
//
//  Created by Usama Naseer on 09/12/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit

class BillingAddressTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var addressTextField: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
