//
//  BillingAddress3ViewController.swift
//  UnitedCement
//
//  Created by Usama Naseer on 12/11/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit
import SideMenu

class BillingAddress3ViewController: UIViewController,passAddressDelegate {
    
    var delegate : passAddressDelegate?
    var addressArray : [AddressModel]?
    @IBOutlet weak var arrowButton:UIButton!
    @IBOutlet weak var sideMenuButton:UIButton!
    @IBOutlet weak var addAddressButton:UIButton!
    @IBOutlet weak var billingAddressLabel: UILabel!
    
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupmenu()
        tableView.dataSource = self
        tableView.delegate = self
        ApiCall.getAddresses { (addresses,logout, error)  in
            if error == nil {
                if logout! {
                    
                    self.presentMain()
                    
                } else {
                    if (addresses?.isEmpty)!  {
                        
                        self.tableViewHeight.constant = CGFloat(0)
                        self.tableView.isHidden = true
                        self.tableView.layoutIfNeeded()
                    }
                    else {
                        self.tableView.isHidden = false
                        self.addressArray = addresses
                        self.tableViewHeight.constant = CGFloat(300)
                        self.tableView.layoutIfNeeded()
                        self.tableView.reloadData()
                    }
                    
                }
            }
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        entoArab()
    }
    func entoArab()
    {
        if Defaults.isArabicLanguage
        {
            billingAddressLabel.text = "عنوان الفواتير"
//            addAddressButton.setImage(UIImage(named: "AddaddArabic"), for: .normal)

            addAddressButton.setImage(nil, for: .normal)
            addAddressButton.setTitle("إضافة عنوان في القائمة", for: .normal)
        }
    }
    
    
    @IBAction func arrowButtonTapped(_sender:UIButton)
    {
        popBack(1)
    }
    @IBAction func sideMenuButtonTapped(_sender:UIButton)
    {
        present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
        
    }
    @IBAction func addAddressButtonTapped(_sender:UIButton)
    {
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc : BillingAddress2ViewController = mainStoryboard.instantiateViewController(withIdentifier: "BillingAddress2ViewController") as! BillingAddress2ViewController
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func locationData(name: String,addressId: Int ,location: String) {
        delegate?.locationData(name: name,addressId: addressId, location: location)
    }
    
}

extension BillingAddress3ViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.addressArray?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "shippingcell") as! BillingAddressTableViewCell
        
        cell.addressTextField?.text = "\(self.addressArray![indexPath.row].address2 ?? "") \n\(self.addressArray![indexPath.row].address1 ?? "")"
//        ,\n\(self.addressArray![indexPath.row].city ?? ""),\n\(self.addressArray![indexPath.row].country ?? "")"
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        delegate?.locationData(name: "billing",addressId: addressArray![indexPath.row].id, location:  "\(addressArray![indexPath.row].address1 ?? ""), \(addressArray![indexPath.row].city ?? ""), \( addressArray![indexPath.row].country ?? "")")
        popBack(1)
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
        
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            //self.tableArray.remove(at: indexPath.row)
            // print("Deleted \(indexPath.row)")
            ApiCall.deleteAdd(addId: self.addressArray![indexPath.row].id, completion: { (status, error) in
                if status == 200 {
                    self.addressArray?.remove(at: indexPath.row)
                    self.tableView.reloadData()
                    self.tableView.layoutIfNeeded()
                }
            })
            
          
            // tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
}
