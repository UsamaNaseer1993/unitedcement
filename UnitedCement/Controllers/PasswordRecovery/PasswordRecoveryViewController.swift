//
//  PasswordRecoveryViewController.swift
//  UnitedCement
//
//  Created by Usama Naseer on 12/11/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit

class PasswordRecoveryViewController: UIViewController {
    
    @IBOutlet weak var pwdrecvryLabel:UILabel!
    @IBOutlet weak var textfield:UITextField!
    @IBOutlet weak var sideMenuButton:UIButton!
    @IBOutlet weak var arrowButton:UIButton!
    @IBOutlet weak var submitButton:UIButton!
    @IBOutlet weak var DescriptionLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        engToArabic()
    }
    
    func engToArabic()
    {
        if Defaults.isArabicLanguage
        {
            pwdrecvryLabel.text = "استعادة كلمة السر"
            submitButton.setTitle("إرسال", for: .normal)
            textfield.placeholder = "أدخل بريدك الالكتروني"
            textfield.textAlignment = .right
            DescriptionLabel.text = "يرجى إدخال بريد إلكتروني صالح في الحقل أدناه والضغط على زر إرسال. يتم إنشاء بريد إلكتروني تلقائي لعنوان البريد الإلكتروني المحدد مع تعليمات إعادة تعيين كلمة المرور"
            DescriptionLabel.textAlignment = .right
            
        }
    }
    @IBAction func sideMenuButtonTapped(_sender:UIButton) {
        
    }
    @IBAction func arrowButtonTapped(_sender:UIButton) {
        self.popBack(1)
    }
    @IBAction func submitButtonTapped(_sender:UIButton) {
        if textfield.text?.isBlank == false && (textfield.text?.isEmail)! {
            ApiCall.forgotPassword(email: textfield.text!) { (message, error) in
                if error == nil {
                    self.showAlert(title: "", message: KCodeSent) {
                        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                        let vc : CodeViewController = mainStoryboard.instantiateViewController(withIdentifier: "CodeViewController") as! CodeViewController
                        vc.email = self.textfield.text
                        self.navigationController?.pushViewController(vc, animated: true)
                        //                        self.switchScreen(identifier: "CodeViewController")
                    }
                }
                else {
                    print(error!)
                }
            }
            //            showAlert(title: "", message: KCodeSent) {
            //                self.switchScreen(identifier: "CodeViewController")
            //            }
        }
        else if (textfield.text?.isBlank)! {
            showAlert(title: Constants.MESSAGE, message: KFieldsNotEmpty)
        }
        else if textfield.text?.isBlank == false && textfield.text?.isEmail == false {
            showAlert(title: Constants.MESSAGE, message: KEnterValidEmail)
            
        }
    }
    
    
}
