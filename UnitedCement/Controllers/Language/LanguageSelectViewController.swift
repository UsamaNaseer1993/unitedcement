//
//  LanguageSelectViewController.swift
//  UnitedCement
//
//  Created by Usama Naseer on 12/11/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit

class LanguageSelectViewController: UIViewController {
    
    @IBOutlet weak var engButton:UIButton!
    @IBOutlet weak var arabicButton:UIButton!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
      
        
        // Do any additional setup after loading the view.
    }
    @IBAction func engButtonTapped(_sender:UIButton!)
    {
        UserDefaults.standard.set(1, forKey: KLanguageKey)
        Defaults.setLanguage(isArabic: false)
        UIView.appearance().semanticContentAttribute = .forceLeftToRight
        switchScreen(identifier: "PageViewController")
        //        let vc = self.storyboard?.instantiateViewController(withIdentifier: "productpage2")
        //        self.present(vc!,animated: true, completion: nil)
        //        Defaults.setLanguage(isArabic: false)
        //
        
    }
    @IBAction func arabicButtonTapped(_sender:UIButton!)
    {
        UserDefaults.standard.set(2, forKey: KLanguageKey)
        //        let vc = self.storyboard?.instantiateViewController(withIdentifier: "productpage2")
        //        self.present(vc!,animated: true, completion: nil)
        Defaults.setLanguage(isArabic: true)
        UIView.appearance().semanticContentAttribute = .forceRightToLeft
         switchScreen(identifier: "PageViewController")
    }
}
