//
//  MyOrdersViewController.swift
//  UnitedCement
//
//  Created by Majeed Bhai on 10/22/18.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit

class MyOrdersViewController: UIViewController {
    var orders : [OrderModel] = []
   
    @IBOutlet weak var detail: UILabel!
    
    @IBOutlet weak var status: UILabel!
    
    @IBOutlet weak var orderNum: UILabel!
    @IBOutlet weak var ordersLabel: UILabel!
    @IBOutlet weak var tableView:UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        guard let userId = UserDefaults.standard.object(forKey: "userId") as? Int else {
            return
        }
        
        ApiCall.orderList(userId: userId ) { (orders, logout, error) in
            if logout! {
                self.presentMain()
            } else {
                guard let orders = orders else {
                    return
                }
                self.orders = orders
                self.tableView.reloadData()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if Defaults.isArabicLanguage {
            orderNum.text = "رقم الطلب"
            status.text = "الحالة"
            detail.text = "تفاصيل"
        }
     
        super.viewWillAppear(animated)
    }
    
    
    
    @IBAction func backButton(_ sender: Any) {
        popBack(1)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension MyOrdersViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.orders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableViewCell") as! TableViewCell
        
        if Defaults.isArabicLanguage
        {
            orderNum.text = "رقم الطلب"
            status.text = "الحالة"
            detail.text = "تفاصيل"
//            let orderImage = UIImage(named: "OrderArabic")
//            self.orderNum.image = orderImage
//           // orderNum = UIImageView(image: orderImage)
//            let dateImage = UIImage(named: "dateArabic")
//            self.date.image = dateImage
//            let amountImage = UIImage(named: "amountarabic")
//            self.amount.image = amountImage
//            let detailImage = UIImage(named: "DetailArabic")
//            self.detail.image = detailImage

           cell.detailsBtn.setImage(nil, for: .normal)
            ordersLabel.text = "طلب"
            cell.detailsBtn.setTitle("عرض", for: .normal)
            cell.detailsBtn.backgroundColor = UIColor(named: "DarkBlue")
            cell.orderNumLabel.text = self.orders[indexPath.row].orderNumber ?? "XYZ"
         //   cell.amountLabel.text = String(self.orders[indexPath.row].orderAmount ?? 0) ?? "XYZ"
            cell.dateLabel.text = self.orders[indexPath.row].orderStatus_Ar
            
            cell.detail = {
                let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                let vc : OrderDetailViewController = mainStoryboard.instantiateViewController(withIdentifier: "OrderDetailViewController") as! OrderDetailViewController
                vc.id = self.orders[indexPath.row].id
                self.navigationController?.pushViewController(vc, animated: true)
            }
          //  cell.dateLabel.text = self.orders[indexPath.row].orderStarted ?? "XYZ"
            return cell
        }
        else
        {

            cell.orderNumLabel.text = self.orders[indexPath.row].orderNumber ?? "XYZ"
//            cell.amountLabel.text = String(self.orders[indexPath.row].orderAmount ?? 0) ?? "XYZ"
            cell.dateLabel.text = self.orders[indexPath.row].orderStatus
            cell.detail = {
                let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                let vc : OrderDetailViewController = mainStoryboard.instantiateViewController(withIdentifier: "OrderDetailViewController") as! OrderDetailViewController
                vc.id = self.orders[indexPath.row].id
                self.navigationController?.pushViewController(vc, animated: true)
            }
         //   cell.dateLabel.text = self.orders[indexPath.row].orderStarted ?? "XYZ"
            return cell
        }
    }
}
