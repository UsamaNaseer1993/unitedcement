//
//  TableViewCell.swift
//  UnitedCement
//
//  Created by Majeed Bhai on 10/22/18.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    @IBOutlet weak var orderNumLabel: UILabel!
    @IBOutlet weak var detailsBtn: UIButton!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    var detail : (() -> ())?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func detailsBtnTap(_ sender: UIButton) {
        detail!()
    }
}
