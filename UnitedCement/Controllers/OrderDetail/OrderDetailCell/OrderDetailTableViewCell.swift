//
//  OrderDetailTableViewCell.swift
//  UnitedCement
//
//  Created by Usama Naseer on 19/12/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit

class OrderDetailTableViewCell: UITableViewCell {

    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var totalPrice: UILabel!
    @IBOutlet weak var bags: UILabel!
    @IBOutlet weak var quantity: UILabel!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var noOfTrucksLabel: UILabel!
    @IBOutlet weak var totalPriceLabel: UILabel!

    @IBOutlet weak var unitPrice: UILabel!
    @IBOutlet weak var unitPriceLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
