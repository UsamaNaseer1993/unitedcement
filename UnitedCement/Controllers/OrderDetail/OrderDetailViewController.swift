//
//  OrderDetailViewController.swift
//  UnitedCement
//
//  Created by Usama Naseer on 20/12/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit

class OrderDetailViewController: UIViewController {
    
    @IBOutlet weak var dateLb: UILabel!
    
    @IBOutlet weak var heightForTableView: NSLayoutConstraint!
    @IBOutlet weak var shippingAddBtn: UIButton!
    @IBOutlet weak var billingAddBtn: UIButton!
    var id : Int?
    @IBOutlet weak var checkouTypeLb: UITextField!
    @IBOutlet weak var GrandTotalLb: UILabel!
    @IBOutlet weak var VatLb: UILabel!
    @IBOutlet weak var couponImgLb: UIImageView!
    @IBOutlet weak var couponLb: UILabel!
    @IBOutlet weak var subtotalLb: UILabel!
    @IBOutlet weak var deliveryDateLb: UILabel!
    @IBOutlet weak var orderDetailsLabel: UILabel!
    @IBOutlet weak var billingAddLabel: UILabel!
    @IBOutlet weak var shippingLabel: UILabel!
    @IBOutlet weak var dlvrdteLable: UILabel!
    @IBOutlet weak var subtotalLabel: UILabel!
    @IBOutlet weak var addCouponLabel: UILabel!
    @IBOutlet weak var grandTotalLabel: UILabel!
    @IBOutlet weak var checkOutLabel: UILabel!
    @IBOutlet weak var paidButton: UIButton!
    var orders : [OrderDetailModel]? = []
    
    @IBOutlet weak var order_status_lb: UILabel!
    @IBOutlet weak var order_Num_lb: UILabel!
    @IBOutlet weak var order_Status: UILabel!
    @IBOutlet weak var order_Number: UILabel!
    @IBOutlet weak var VATLab: UILabel!
    @IBOutlet weak var addPaymentDetailsButton: UIButton!
    @IBOutlet weak var tableVie: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        ApiCall.orderDetail(id: id ?? 0) { (orders, logout, billing, shipping, error) in
            if error == nil {
                if logout! {
                    self.presentMain()
                } else {
                    self.dateLb.text = self.getCurrentDate()
                    self.orders = orders ?? []
                    self.couponLb.text = self.orders?[0].coupon
                    if self.orders?[0].coupon != "" {
                        self.couponLb.textAlignment = .center
                          self.couponImgLb.isHidden = false
                    } else {
                         self.couponImgLb.isHidden = true
                    }
                    self.order_Number.text = self.orders?[0].orderNumber
                    if Defaults.isArabicLanguage {
                        self.order_Status.text = self.orders?[0].arabic_status
                    } else {
                        self.order_Status.text = self.orders?[0].order_status
                    }
                    
                    self.shippingAddBtn.setTitle(shipping, for: .normal)
                    self.billingAddBtn.setTitle(billing, for: .normal)
                    self.subtotalLb.text = self.orders?[0].subtotal.converToString
                    self.VatLb.text =  "\(Double(truncating: self.orders?[0].subtotal as! NSNumber) * 0.05)"
                    self.VatLb.textAlignment = .center
                    self.GrandTotalLb.textAlignment = .center
                    self.subtotalLb.textAlignment = .center
                    self.GrandTotalLb.text = self.orders?[0].grandTotal.converToString
                    if Defaults.isArabicLanguage {
                    if self.orders?[0].type == "CREDIT / DEBIT CARD " || self.orders?[0].type == "بطاقة الائتمانية" {
                        self.checkouTypeLb.text = "بطاقة الائتمانية"
                    } else if self.orders?[0].type == "BANK TRANSFER" || self.orders?[0].type == "التحويل المصرفي"   {
                        self.checkouTypeLb.text = "التحويل المصرفي"
                    }
                    } else {
                        self.checkouTypeLb.text = self.orders?[0].type
                    }
                   
                  
                    if self.checkouTypeLb.text == "BANK TRANSFER" {

                        if Defaults.isArabicLanguage {
                            if(self.order_Status.text != "قيد الانتظار")
                            {
                                self.paidButton.setTitle("معالجة", for: .normal)
                                self.paidButton.setTitleColor(.blue, for: .normal)
                                self.paidButton.isUserInteractionEnabled = false
                                
                            }
                            else{
                                self.paidButton.backgroundColor = UIColor(named: "DarkBlue")
                                self.paidButton.setTitle("أدخل تفاصيل الدفع", for: .normal)
                            }
                        } else if(self.order_Status.text != "Pending" ){
                                self.paidButton.setTitle("Processing", for: .normal)
                                self.paidButton.setTitleColor(.blue, for: .normal)
                                self.paidButton.isUserInteractionEnabled = false
                            }
                            else
                            {
                             self.paidButton.backgroundColor = UIColor(named: "DarkBlue")
                             self.paidButton.setTitle("Add Payment Details", for: .normal)
                        }
                    }
                    
                    else {
                        self.paidButton.setBackgroundImage(UIImage(named: "paid"), for: .normal)
                        self.paidButton.setTitle("", for: .normal)
                    }
                    self.deliveryDateLb.text = self.orders?[0].orderDeliveryDate
                    self.tableVie.reloadData()
                    
                    
                    self.tableVie.layoutIfNeeded()
                    if self.orders?.count == 1 {
                        self.heightForTableView.constant = CGFloat(120)
                    } else {
                        self.heightForTableView.constant = CGFloat(220)
                    }
                }
            }
        }
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        engToArabic()
       // dateLb.text = self.getCurrentDate()
    }
    func engToArabic()
    {
        if Defaults.isArabicLanguage
        {
            order_Num_lb.text = "رقم الطلب"
            
            order_status_lb.text = "الحالة"
            orderDetailsLabel.text = "تفاصيل الطلب"
            dateLb.text = "الأربعاء ، 10 أكتوبر ، 2018"
            billingAddLabel.text = "عنوان الفواتير"
            billingAddBtn.setTitle("اختر العنوان", for: .normal)
            shippingLabel.text = "عنوان الشحن"
            shippingAddBtn.setTitle("اختر العنوان", for: .normal)
            subtotalLabel.text = "المجموع"
           
            addCouponLabel.text = "كوبون"
            checkOutLabel.text = "طريقة الدفع"
            VATLab.text = "الضريبة المضافة"
            grandTotalLabel.text =  "الإجمالي"
            
            
           
            dlvrdteLable.text = "تاريخ التسليم"
            dlvrdteLable.textAlignment = .center
          //  checkouTypeLb.text = "نوع الدفع"
            checkouTypeLb.textAlignment = .center
            paidButton.setTitle("دفع", for: .normal)
            
        }
    }
    
    @IBAction func addPaymentDetails(_ sender: Any) {
        if self.checkouTypeLb.text == "BANK TRANSFER" {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc : BankTransferViewController = mainStoryboard.instantiateViewController(withIdentifier: "BankTransferViewController") as! BankTransferViewController
            vc.orderId = id
            vc.amount =  self.orders?[0].grandTotal
            vc.paymentId = self.orders?[0].paymentId
            vc.order = false
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func backBtnAction(_ sender: Any) {
        self.popBack(1)
    }
 
    
}

extension OrderDetailViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if Defaults.isArabicLanguage
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrderDetailTableViewCell") as! OrderDetailTableViewCell
            if indexPath.row == 0
            {
                cell.productName.text = "اسمنت بورتلاندي عادي"
            }
            else if indexPath.row == 1
            {
                cell.productName.text = "اسمنت مقاوم للكبريتات"
            }
            else if indexPath.row == 2
            {
                cell.productName.text = "اسمنت بورتلاندي بوزولاني"
            }
            cell.noOfTrucksLabel.text = "الكمية"
            cell.quantityLabel.text = "عدد الشاحنات"
            cell.totalPriceLabel.text = "سعر الوحدة"
            cell.unitPriceLabel.text = "سعر الإجمالي"
            cell.productName.text = self.orders?[indexPath.row].arabicName
            cell.bags.text = self.orders?[indexPath.row].bags.converToString
            cell.quantity.text = self.orders?[indexPath.row].trucks.converToString
            cell.unitPrice.text =  "\(self.orders?[indexPath.row].grandTotal.converToString ?? "") ر.س"
             cell.totalPrice.text =  "\(self.orders?[indexPath.row].productPrice.converToString ?? "") ر.س"
            if indexPath.row == 0 {
                cell.cellView.backgroundColor = UIColor(named: "cell1")
            }
            else if indexPath.row == 1 {
                cell.cellView.backgroundColor = UIColor(named: "cell2")
            }
            else {
                cell.cellView.backgroundColor = UIColor(named: "cell3")
            }
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "OrderDetailTableViewCell") as! OrderDetailTableViewCell
            cell.productName.text = self.orders?[indexPath.row].name
            if let bags = self.orders?[indexPath.row].bags {
                cell.bags.text = bags.converToString
            }
            
            cell.quantity.text = self.orders?[indexPath.row].trucks.converToString
          //  cell.totalPrice.text = self.orders?[indexPath.row].grandTotal.converToString
            cell.unitPrice.text =  self.orders?[indexPath.row].grandTotal.converToString
            cell.totalPrice.text =  self.orders?[indexPath.row].productPrice.converToString
            if indexPath.row == 0 {
                cell.cellView.backgroundColor = UIColor(named: "cell1")
            }
            else if indexPath.row == 1 {
                cell.cellView.backgroundColor = UIColor(named: "cell2")
            }
            else {
                cell.cellView.backgroundColor = UIColor(named: "cell3")
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
}
