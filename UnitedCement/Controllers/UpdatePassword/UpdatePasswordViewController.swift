//
//  UpdatePasswordViewController.swift
//  UnitedCement
//
//  Created by Usama Naseer on 12/11/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit

class UpdatePasswordViewController: UIViewController {
    
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var oldPassTextField: UITextField!
    @IBOutlet weak var newPassTextField: UITextField!
    @IBOutlet weak var updatePassLabel: UILabel!
    @IBOutlet weak var updatePassButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        engToArabic()
    }
    func engToArabic()
    {
        if Defaults.isArabicLanguage
        {
            emailTextField.placeholder = "البريد الإلكتروني"
            emailTextField.textAlignment = .right
            oldPassTextField.placeholder = "كلمة المرور القديمة"
            oldPassTextField.textAlignment = .right
            newPassTextField.placeholder = "كلمة السر الجديدة"
            newPassTextField.textAlignment = .right
            updatePassButton.setImage(UIImage(named: "upPassArabic"), for: .normal)
            //updatePassButton.setTitle("تطوير كلمة السر", for: .normal)
            updatePassLabel.text = "تطوير كلمة السر"
            
        }
    }
    @IBAction func homeButtonTapped(_sender:UIButton)
    {
        popBack(1)
    }
    @IBAction func sideMenuButtonTapped(_sender:UIButton)
    {}
    @IBAction func updatePassButtonTapped(_sender:UIButton) {
        if (emailTextField.text?.isBlank)!  ||
            (oldPassTextField.text?.isBlank)! ||
            (newPassTextField.text?.isBlank)! {
            showAlert(title: Constants.MESSAGE, message: "Fields cannot be empty")
        } else {
            ApiCall.updatePassword(email: (emailTextField.text)!, oldpassword: oldPassTextField.text!, newpassword: newPassTextField.text!, completion: {(msg, error) in
                if error == nil  && msg != nil {
                    self.showAlert(title: Constants.MESSAGE, message: msg, completion: {
                        self.popBack(2)
                    })
                }
                else {
                    print(error!.description)
                }
            })
            
            
            
        }
    }
}
