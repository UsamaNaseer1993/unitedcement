//
//  SideMenuTableViewCell.swift
//  UnitedCement
//
//  Created by Usama Naseer on 26/11/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var labelOutlet : UILabel!
    @IBOutlet weak var imageOutlet: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
