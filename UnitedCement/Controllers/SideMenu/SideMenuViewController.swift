//
//  SideMenuViewController.swift
//  UnitedCement
//
//  Created by Usama Naseer on 26/11/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit

class SideMenuViewController: UITableViewController {
  let items : [String] = [
    "myorders", "setting", "aboutus", "coupons","aboutucic", "logout"
  ]
  
    override func viewDidLoad() {
        super.viewDidLoad()

      let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 150, height: 40))
      imageView.contentMode = .scaleAspectFit
      
      let image = UIImage(named: "logo@7x")
      imageView.image = image
      
      navigationItem.titleView = imageView
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SideMenuViewController  {
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 6
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
 
      let cell = tableView.dequeueReusableCell(withIdentifier: items[indexPath.row]) as! SideMenuTableViewCell
      return cell
   
  }
  
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
   
    if indexPath.row == 5 {
        ApiCall.logout { (msg, error) in
            if error == nil {
                self.presentMain()
            }
        }
    } else {
        switchScreen(identifier: items[indexPath.row])
    }
//    let vc = self.storyboard?.instantiateViewController(withIdentifier: items[indexPath.row]) as! UIViewController
//    self.present(vc, animated: true, completion: nil)
  }
}
