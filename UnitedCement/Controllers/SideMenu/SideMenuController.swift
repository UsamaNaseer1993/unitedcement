//
//  SideMenuController.swift
//  UnitedCement
//
//  Created by Muhammad Irfan on 1/2/19.
//  Copyright © 2019 Usama Naseer. All rights reserved.
//

import UIKit

class SideMenuController: UIViewController {
    var buttonn = UIButton()
    var buttonn1 = UIButton()
    
    var label = UILabel()
    var label1 = UILabel()
    
    var English = Bool()
    var Arabic = Bool()
    let menuOptionEnglish : [String] = [
        "MYORDERS", "SETTING", "COUPONS","ABOUTUS","LANGUAGE", "LOGOUT"
    ]
    let menuOption : [String] = [
        "myorders", "setting", "coupons","aboutucic","language", "logout"
    ]
    let images = ["Order 1x","setting 1x","home-1x","about-us","lang 1x","Logout 1x"]
    
    @IBOutlet weak var tableView: UITableView!
    
//       let menuOptionArabic : [String] = ["الخروج","لغة","معلومات عنا","كوبونات","ضبط","طلباتي"]
    let menuOptionArabic : [String] =  ["طلباتي","الإعدادات","كوبونات","معلومات عنا","لغة","خروج"]
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         initViews()
        tableView.reloadData()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 150, height: 40))
        imageView.contentMode = .scaleAspectFit
        
        let image = UIImage(named: "logo@7x")
        imageView.image = image
        
        navigationItem.titleView = imageView
        // Do any additional setup after loading the view.
    }
    func initViews() {
                English = false
                Arabic = false
        
                if Defaults.isArabicLanguage
                {
                    buttonn = UIButton(frame: CGRect(x: 200, y: 40, width: 30, height: 30))
                }
                else
                {
                buttonn = UIButton(frame: CGRect(x: 20, y: 40, width: 30, height: 30))
                }
                buttonn.backgroundColor = .clear
                buttonn.tag = 1
                buttonn.setImage(UIImage(named: "unchecked"), for: UIControl.State.normal)
                buttonn.setImage(UIImage(named: "checked"), for: UIControl.State.selected)
                buttonn.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        
                if Defaults.isArabicLanguage
                {
                    buttonn1 = UIButton(frame: CGRect(x: 200, y: 80, width: 30, height: 30))
                }
                else
                {
                    buttonn1 = UIButton(frame: CGRect(x: 20, y: 80, width: 30, height: 30))
        
                }
               // buttonn1 = UIButton(frame: CGRect(x: 200, y: 80, width: 30, height: 30))
                // buttonn1.backgroundColor = .clear
                buttonn1.tag = 2
                buttonn1.setImage(UIImage(named: "unchecked"), for: UIControl.State.normal)
                buttonn1.setImage(UIImage(named: "checked"), for: UIControl.State.selected)
                buttonn1.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        
                label = UILabel(frame: CGRect(x: 60, y: 40, width: 200, height: 30))
                label1 = UILabel(frame: CGRect(x: 60, y: 80, width: 200, height: 30))
        
                if Defaults.isArabicLanguage
                {
                    label = UILabel(frame: CGRect(x: 0, y: 40, width: 200, height: 30))
                    label1 = UILabel(frame: CGRect(x: 0, y: 80, width: 200, height: 30))
                label.text = "الإنجليزية"
                label1.text = "العربية"
                }
                else
                {
                    label.text = "English"
                    label1.text = "Arabic"
                }
    }
    
        @objc func buttonAction(sender: UIButton!) {
    
            switch sender.tag {
            case 1:
    
                if buttonn.isSelected == true{
                    buttonn.isSelected = false
                    English = false
                    Arabic = false
                }
                else{
                    buttonn.isSelected = true
                    buttonn1.isSelected = false
                    English = true
                    Arabic = false
                }
    
                break
    
            case 2:
    
                if buttonn1.isSelected == true{
                    buttonn1.isSelected = false
                    English = false
                    Arabic = false
                }
                else{
                    buttonn1.isSelected = true
                    buttonn.isSelected = false
                    English = false
                    Arabic = true
                }
    
                break
    
            default:
                break
            }
    
    
        }
    
        func showAleert(){
            var msg = "Select Location"
            var select = "Select"
            var cancel = "Cancel"
            if Defaults.isArabicLanguage {
                msg = "اختار اللغة"
                select = "تحديد"
                cancel = "إلغاء"
            }
            let alertController = UIAlertController(title: "\(msg)\n\n\n\n", message: nil, preferredStyle: UIAlertController.Style.alert)
    
    
            let margin:CGFloat = 10.0
            let rect = CGRect(x: margin, y: margin, width: alertController.view.bounds.size.width - margin * 4.0, height: 100)
            let customView = UIView(frame: rect)
    
           // customView.backgroundColor = .white
    
            customView.addSubview(buttonn)
            customView.addSubview(buttonn1)
    
            customView.addSubview(label)
            customView.addSubview(label1)
    
            alertController.view.addSubview(customView)
            let ExitAction = UIAlertAction(title: select, style: .default, handler: {(alert: UIAlertAction!) in
    
                if self.English == true {
                    Defaults.setLanguage(isArabic: false)
                    UIView.appearance().semanticContentAttribute = .forceLeftToRight
                    self.switchScreen(identifier: "TabbarViewController")
                }
                if self.Arabic == true{
                    Defaults.setLanguage(isArabic: true)
                    UIView.appearance().semanticContentAttribute = .forceRightToLeft
                  self.switchScreen(identifier: "TabbarViewController")

                }
    //            if (self.Jeddah == false) && (self.Mekkah == false) {
    //                self.offerArray?.removeAll()
    //                self.tableView.reloadData()
    //            }
    
    
            })
            let cancelAction = UIAlertAction(title: cancel, style: .cancel, handler: {(alert: UIAlertAction!) in print("cancel")})
            alertController.addAction(ExitAction)
            alertController.addAction(cancelAction)
            DispatchQueue.main.async {
                self.present(alertController, animated: true, completion:{})
            }
    
        }
    
    
}
extension SideMenuController : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if Defaults.isArabicLanguage
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTableViewCell") as! SideMenuTableViewCell
            cell.labelOutlet.text = menuOptionArabic[indexPath.row]
            cell.imageOutlet.image = UIImage(named: images[indexPath.row])
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTableViewCell") as! SideMenuTableViewCell
            cell.labelOutlet.text = menuOptionEnglish[indexPath.row]
            cell.imageOutlet.image = UIImage(named: images[indexPath.row])
            
            return cell
            
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 5 {
            ApiCall.logout { (msg, error) in
                if error == nil {
           
                           self.presentMain()
                }
            }
        } else if indexPath.row == 4 {
            showAleert()
        }
        else
        {
            switchScreen(identifier: menuOption[indexPath.row])
        }
    }
    
}
