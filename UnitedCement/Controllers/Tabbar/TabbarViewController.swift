//
//  TabbarViewController.swift
//  UnitedCement
//
//  Created by Usama Naseer on 22/11/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit

class TabbarViewController: UITabBarController {

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UILabel.appearance().font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle(rawValue: "Arabic")).withSize(12)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
     //   let stack = self.navigationController?.viewControllers
        self.delegate = self
    }
    
}

extension TabbarViewController: UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let navigationController = viewController as! UINavigationController
        
        navigationController.topViewController?.isKind(of: OrderViewController.self)
        
        navigationController.popToRootViewController(animated: true)
            
        
    }
}
