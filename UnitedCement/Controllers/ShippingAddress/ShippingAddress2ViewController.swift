//
//  ShippingAddress2ViewController.swift
//  UnitedCement
//
//  Created by Usama Naseer on 12/11/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import SideMenu

class ShippingAddress2ViewController: UIViewController,GMSMapViewDelegate {
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation!
    var mapView: GMSMapView!
    var zoomLevel: Float = 15.0
    var delegate : passAddressDelegate?
    var marker: GMSMarker?
    
    @IBOutlet weak var arrowButton:UIButton!
    @IBOutlet weak var sideMenuButton:UIButton!
    @IBOutlet weak var saveButton:UIButton!
    @IBOutlet weak var additionalAddressLabel:UILabel!
    @IBOutlet weak var enterCompAddTextField:UITextField!
    @IBOutlet weak var cityTxtFld:UITextField!
    @IBOutlet weak var countryTxtFld:UITextField!
    @IBOutlet weak var shippingAdd: UILabel!
    
    @IBOutlet weak var locView: UIView!
    
    var cities: [City] = []
    var country: Country?
    
    
    var cityPickerView = UIPickerView()
    var countryPickerView = UIPickerView()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        ApiCall.getAllCountries { (code,countryCityArray, error) in
            if error == nil {
                
                self.cities = countryCityArray![0].city  //City.map(cities: countryCityArray![0].city)
                self.country = countryCityArray![0].country //Country.map(country: countryCityArray![0].country!)
                if Defaults.isArabicLanguage{
                     self.countryTxtFld.text = self.country!.arabicName
                }
                else{
                self.countryTxtFld.text = self.country!.name
            }
        }
        }
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        loadmap()
        setupmenu()
        engToArabic()
        
        // Do any additional setup after loading the view.
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 1 || textField.tag == 2 {
            return false
        }
        return true
    }
    
    func engToArabic()
    {
        if Defaults.isArabicLanguage
        {
            shippingAdd.text = "عنوان الشحن"
            additionalAddressLabel.text = "عنوان اضافي"
            additionalAddressLabel.textAlignment = .center
            enterCompAddTextField.placeholder = "أدخل العنوان الكامل"
            cityTxtFld.placeholder = "دخول المدينة"
            countryTxtFld.placeholder = "أدخل البلد"
            saveButton.setTitle("حفظ", for: .normal)
        }
    }
    
    @IBAction func arrowButtonTapped(_sender:UIButton)
    {
        popBack(1)
    }
    @IBAction func sideMenuButtonTapped(_sender:UIButton)
    {
        present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
        
    }
    @IBAction func saveButtonTapped(_sender:UIButton)
    {
       
        //  switchScreen(identifier: "OrderViewController")
        if (enterCompAddTextField.text?.isBlank)! || (countryTxtFld.text?.isBlank)! || (cityTxtFld.text?.isBlank)! {
            self.showAlert(title: Constants.MESSAGE, message:  "Fields cannot be empty")
        } else {
            getAddressFromLatLon(pdblLatitude: String(currentLocation.coordinate.latitude), withLongitude: String(currentLocation.coordinate.longitude)) { (add, city, country) in
                ApiCall.createAddresses(latitude: self.currentLocation.coordinate.latitude, longitude: self.currentLocation.coordinate.longitude, addressDesc: add,addressDetail: self.enterCompAddTextField.text!, locationName: "shipping", city:self.cities[0].name, country: self.country?.name ?? "", countryId: 191) { (msg, logout, addressId,error) in
                    if error == nil {
                        if logout! {
                            self.presentMain()
                            
                        } else {
                             self.delegate?.locationData(name: "shipping",addressId: addressId!, location: "\(add)")
                             self.popBack(2)
                             self.showAlert(title: "Success", message: msg)
//                            self.delegate?.locationData(name: "shipping",addressId: addressId!, location: "\(self.enterCompAddTextField.text ?? "") \(self.cityTxtFld.text ?? ""), \(self.countryTxtFld.text ?? "")")
                           
                        }
                    }
                }
            }

          
        }
        
    }
}

extension ShippingAddress2ViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == countryPickerView{
            return 1
        } else{
            return cities.count
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if Defaults.isArabicLanguage == true
        {
            if pickerView == countryPickerView {
                return country!.arabicName
            }
            else
            {
                return cities[row].arabicName
            }
        }
        else
        {
            if pickerView == countryPickerView{
                return country!.name
            }
            else
            {
                return cities[row].name
            }
            
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == countryPickerView {
            if Defaults.isArabicLanguage == false{
                countryTxtFld.text = country!.name
                self.view.endEditing(false)
            }
            else if pickerView == countryPickerView{
                if Defaults.isArabicLanguage == true
                {
                    countryTxtFld.text = country!.arabicName
                    self.view.endEditing(false)
                }
            }
        }
        else {
            if Defaults.isArabicLanguage == true
            {
                cityTxtFld.text = cities[row].arabicName
                self.view.endEditing(false)
            }
            else
            {
                cityTxtFld.text = cities[row].name
                self.view.endEditing(false)
            }
        }

    }
}

extension ShippingAddress2ViewController: CLLocationManagerDelegate, UITextFieldDelegate {
    func initViews() {
        
        cityPickerView.dataSource = self
        cityPickerView.delegate = self
        
        countryPickerView.dataSource = self
        countryPickerView.delegate = self
        
        cityTxtFld.inputView = cityPickerView
        countryTxtFld.inputView = countryPickerView
        cityTxtFld.delegate = self
        cityTxtFld.tag = 1
        countryTxtFld.tag = 2
        countryTxtFld.delegate = self
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        //        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        //        locationManager.requestAlwaysAuthorization()
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
    }
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        self.mapView.selectedMarker?.position = position.target
        currentLocation = CLLocation(latitude: position.target.latitude, longitude: position.target.longitude)
        marker?.map = nil
        marker = GMSMarker(position: position.target)
        marker?.map = mapView
        
    }
    func loadmap() {
        let camera = GMSCameraPosition.camera(withLatitude: 28.7041, longitude: 77.1025, zoom: 10.0)
        mapView = GMSMapView.map(withFrame: self.locView.bounds, camera: camera)
        // mapView.settings.myLocationButton = true
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.isMyLocationEnabled = true
        marker?.map = mapView
        mapView.delegate = self
        self.locView.addSubview(mapView)
        mapView.isHidden = true
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        currentLocation = locations.last!
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                              longitude: location.coordinate.longitude,
                                              zoom: zoomLevel)
        if mapView.isHidden {
            mapView.isHidden = false
            mapView.camera = camera
        } else {
            mapView.animate(to: camera)
        }
        
    }
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
            // Display the map using the default location.
            mapView.isHidden = false
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
}
