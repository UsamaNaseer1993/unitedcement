//
//  LoginOrRegisterViewController.swift
//  UnitedCement
//
//  Created by Usama Naseer on 12/11/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit

class LoginOrRegisterViewController: UIViewController {
    
    @IBOutlet weak var loginButton:UIButton!
    @IBOutlet weak var registerButton:UIButton!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        engToArabic()
    }
    
    func engToArabic()
    {
        if Defaults.isArabicLanguage
        {
            loginButton.setTitle("تسجيل الدخول", for: .normal)
            // loginButton.semanticContentAttribute.rawValue = 13
            registerButton.setTitle("تسجيل", for: .normal)
            // registerButton.semanticContentAttribute.rawValue = 12
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    @IBAction func loginButtonTapped(_sender:UIButton)
    {
        switchScreen(identifier: "LoginViewController")
    }
    @IBAction func registerButtonTapped(_sender:UIButton)
    {
        switchScreen(identifier: "RegisterViewController")
    }
    
    @IBAction func leftArrowAction(_ sender: Any) {
        self.popBack(1)
    }
    
}
