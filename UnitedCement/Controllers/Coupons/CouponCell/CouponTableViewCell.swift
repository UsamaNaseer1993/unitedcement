//
//  CouponTableViewCell.swift
//  UnitedCement
//
//  Created by Usama Naseer on 14/12/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit

class CouponTableViewCell: UITableViewCell {

    @IBOutlet weak var pricePercent: UILabel!
    @IBOutlet weak var couponPercent: UILabel!
    @IBOutlet weak var couponFixed: UILabel!
    @IBOutlet weak var priceFixed: UILabel!
    @IBOutlet weak var SARLabel: UILabel!
    @IBOutlet weak var OffLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
