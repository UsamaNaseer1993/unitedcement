//
//  CouponsViewController.swift
//  UnitedCement
//
//  Created by Usama Naseer on 12/11/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit

class CouponsViewController: UIViewController {
    
    @IBOutlet weak var couponTableView: UITableView!
    @IBOutlet weak var couponsLabel: UILabel!
    var couponList : [CouponModel] = []
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        ApiCall.couponList { (coupons, logout, error) in
            if error == nil {
                if logout! {
                    self.presentMain()
                } else {
                    self.couponList = coupons!
                    self.couponTableView.reloadData()
                }
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        engToArabic()
        // Do any additional setup after loading the view.
    }
    
    func engToArabic()
    {
        if Defaults.isArabicLanguage
        {
            couponsLabel.text = "كوبونات"
        }
    }
    
    @IBAction func backButtonAction(_ sender: Any) {
        popBack(1)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension CouponsViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.couponList.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if self.couponList[indexPath.row].reductionType == "fixed" {
            let cell = tableView.dequeueReusableCell(withIdentifier: "fixed") as! CouponTableViewCell
            cell.couponFixed.text = self.couponList[indexPath.row].code
            cell.priceFixed.text = self.couponList[indexPath.row].reductionAmount.converToString
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "percent") as! CouponTableViewCell
            cell.couponPercent.text = self.couponList[indexPath.row].code
            cell.pricePercent.text = "\(self.couponList[indexPath.row].reductionAmount.converToString)%"
            return cell
        }
    }
}
