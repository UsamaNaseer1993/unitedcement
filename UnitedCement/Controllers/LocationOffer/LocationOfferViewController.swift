//
//  LocationOfferViewController.swift
//  UnitedCement
//
//  Created by Usama Naseer on 18/01/2019.
//  Copyright © 2019 Usama Naseer. All rights reserved.
//


import UIKit
import GoogleMaps
import CoreLocation
import SideMenu
import GooglePlaces


class LocationOfferViewController: UIViewController,UITextFieldDelegate,GMSMapViewDelegate {
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation!
    var mapView: GMSMapView!
    var zoomLevel: Float = 15.0
    var marker : GMSMarker?
    var  getcord : ((CLLocationCoordinate2D,String)->())?
    var longPressRecognizer = UILongPressGestureRecognizer()
    var jeddahCoord: [CLLocationCoordinate2D]  = [CLLocationCoordinate2D(latitude: 21.287866,longitude: 39.44013)
        ,CLLocationCoordinate2D(latitude: 21.422161,longitude: 39.576086),CLLocationCoordinate2D(latitude: 21.520566, longitude: 39.53214),CLLocationCoordinate2D(latitude: 21.680172, longitude: 39.598058),CLLocationCoordinate2D(latitude: 21.945365, longitude: 39.401678),CLLocationCoordinate2D(latitude: 21.99376, longitude: 39.262975),CLLocationCoordinate2D(latitude: 22.000127, longitude: 39.011663),CLLocationCoordinate2D(latitude: 21.708244, longitude: 39.035009),CLLocationCoordinate2D(latitude: 21.595922, longitude: 39.10642),CLLocationCoordinate2D(latitude: 21.26995, longitude: 39.154485)]
    var mekkahCoord : [CLLocationCoordinate2D]  = [
        CLLocationCoordinate2D(latitude: 21.890274,longitude: 39.682982)
        ,CLLocationCoordinate2D(latitude: 21.503648,longitude: 39.56076),CLLocationCoordinate2D(latitude: 21.455087, longitude: 39.604705),CLLocationCoordinate2D(latitude: 21.383495, longitude: 39.56076),CLLocationCoordinate2D(latitude: 21.300352, longitude: 39.467376),CLLocationCoordinate2D(latitude: 21.20692, longitude: 39.529174),CLLocationCoordinate2D(latitude: 21.209481, longitude: 39.717315),CLLocationCoordinate2D(latitude: 21.133941, longitude: 39.962846),CLLocationCoordinate2D(latitude: 21.304206, longitude: 40.122148),CLLocationCoordinate2D(latitude: 21.39374, longitude: 40.111161),CLLocationCoordinate2D(latitude: 21.567535, longitude: 40.015031),CLLocationCoordinate2D(latitude: 21.8699, longitude: 39.793931)]
    var jazanCoord : [CLLocationCoordinate2D]  = [
        CLLocationCoordinate2D(latitude: 19.056032,longitude: 41.181235),
        CLLocationCoordinate2D(latitude: 18.806623,longitude: 41.208701),CLLocationCoordinate2D(latitude: 18.619323, longitude: 41.153769),CLLocationCoordinate2D(latitude: 17.65359, longitude: 41.736044),CLLocationCoordinate2D(latitude: 16.829927, longitude: 42.549033),CLLocationCoordinate2D(latitude: 16.814152, longitude: 42.94454),CLLocationCoordinate2D(latitude: 17.538395, longitude: 42.774252),CLLocationCoordinate2D(latitude: 17.909896, longitude: 42.428183),CLLocationCoordinate2D(latitude: 18.556843, longitude: 42.0931),CLLocationCoordinate2D(latitude: 19.071607, longitude: 41.291098),CLLocationCoordinate2D(latitude: 19.056032, longitude: 41.181235)]
    @IBOutlet weak var selectlocTextField: UITextField!
    @IBOutlet weak var sideMenuButton:UIButton!
    @IBOutlet weak var locView:UIView!
    @IBOutlet weak var startShopButton:UIButton!
    @IBOutlet weak var locationLabel: UILabel!
    
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == selectlocTextField {
            let autocompleteController = GMSAutocompleteViewController()
            autocompleteController.delegate = self
            present(autocompleteController, animated: true, completion: nil)
        }
        
        
    }
    
    func EngToArabic()
    {
        if Defaults.isArabicLanguage
        {
            locationLabel.text = "موقعك"
            selectlocTextField.placeholder = "حدد موقعك"
            selectlocTextField.textAlignment = .right
            startShopButton.setTitle("اختر منطقتك", for: .normal)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       // self.chckTrucks()
        UILabel.appearance().font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle(rawValue: "Arabic")).withSize(14)
        if UserDefaults.standard.string(forKey: "City") == "Jeddah" {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc : OffersViewController = mainStoryboard.instantiateViewController(withIdentifier: "OffersViewController") as! OffersViewController
            vc.city = "Jeddah"
            self.navigationController?.pushViewController(vc, animated: true)
        }else if UserDefaults.standard.string(forKey: "City") == "Jazan" {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc : ProductViewController = mainStoryboard.instantiateViewController(withIdentifier: "ProductViewController") as! ProductViewController
            UserDefaults.standard.set("Jazan", forKey: "City")
            vc.city = "Jazan"
            self.navigationController?.pushViewController(vc, animated: true)
        } else if UserDefaults.standard.string(forKey: "City") == "Mecca" {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc : OffersViewController = mainStoryboard.instantiateViewController(withIdentifier: "OffersViewController") as! OffersViewController
            vc.city = "Mecca"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker) {
        print("start dragging")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupmenu()
        initViews()
        loadmap()
        EngToArabic()
        chckTrucks()

        getcord = { coordinate,title in
            self.mapView.clear()
            self.addPolygon(array: self.jazanCoord)
            self.addPolygon(array: self.jeddahCoord)
            self.addPolygon(array: self.mekkahCoord)
            let camera = GMSCameraPosition.camera(withLatitude: coordinate.latitude, longitude: coordinate.longitude, zoom: 15)
            let position = CLLocationCoordinate2D(latitude: coordinate.latitude, longitude:  coordinate.longitude)
            self.currentLocation = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
            self.marker?.map = nil
            self.marker = GMSMarker(position: position)
            self.marker?.title = title
            self.marker?.map = self.mapView
            
            if self.mapView.isHidden {
                self.mapView.isHidden = false
                
                self.mapView.camera = camera }
            else {
                self.mapView.camera = camera
            }
        }
        
        
    }
    
    func chckTrucks(){
        ApiCall.trucks { (trucks, logout, error) in
            if logout == true {
                self.presentMain()
            } else {
                if trucks != 0 {
                    if let tabItems = self.tabBarController?.tabBar.items {
                        let tabItem = tabItems[1]
                        tabItem.badgeValue = trucks?.converToString
                    }
                }
            }
        }
    }
    
    //    fileprivate var polygon: GMSPolygon? = nil
    
    func addPolygon(array : [CLLocationCoordinate2D]) {
        let polygon = GMSPolygon()
        let rect = GMSMutablePath()
        
        for arr in array {
            rect.add(arr)
        }
        
        polygon.path = rect
        polygon.fillColor = UIColor(red: 0.628, green: 0.98, blue: 0.7, alpha: 0.2)
        polygon.strokeColor = .green
        polygon.strokeWidth = 2
        polygon.map = mapView
        
    }
    func checkWithin(point : CLLocationCoordinate2D,array : [CLLocationCoordinate2D]) -> Bool {
        let rect = GMSMutablePath()
        for ar in array {
            rect.add(ar)
        }
        return GMSGeometryContainsLocation(point, rect, true)
    }
    
    @IBAction func sideMenuButtonTapped(_sender:UIButton)
    {
        if let sideRightMenu = SideMenuManager.default.menuRightNavigationController {
            present(sideRightMenu, animated: true, completion: nil)
        }
        
    }
    @IBAction func startShopButtonTapped(_sender:UIButton)
    {
        // print(currentLocation.coordinate.latitude)
        //   print(checkWithin(point: currentLocation.coordinate, array: jeddahCoord))
        if checkWithin(point: currentLocation.coordinate, array: jeddahCoord)  {
            //  self.switchScreen(identifier: "ProductViewController")
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc : OffersViewController = mainStoryboard.instantiateViewController(withIdentifier: "OffersViewController") as! OffersViewController
            UserDefaults.standard.set("Jeddah", forKey: "City")
            vc.city = "Jeddah"
            self.navigationController?.pushViewController(vc, animated: true)
        } else if checkWithin(point: currentLocation.coordinate, array: mekkahCoord)  || checkWithin(point: currentLocation.coordinate, array: jazanCoord) {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc : OffersViewController = mainStoryboard.instantiateViewController(withIdentifier: "OffersViewController") as! OffersViewController
            UserDefaults.standard.set("Mecca", forKey: "City")
            vc.city = "Mecca"
            self.navigationController?.pushViewController(vc, animated: true)
        } else if checkWithin(point: currentLocation.coordinate, array: jazanCoord) {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let vc : ProductViewController = mainStoryboard.instantiateViewController(withIdentifier: "ProductViewController") as! ProductViewController
            UserDefaults.standard.set("Jazan", forKey: "City")
            vc.city = "Jazan"
            self.navigationController?.pushViewController(vc, animated: true)
        } else {
            self.showAlert(title: Constants.MESSAGE, message: "City must be Jeddah,Jazan or Makkah")
        }
        
        
        
    }
    
}

extension LocationOfferViewController: CLLocationManagerDelegate {
    func initViews() {
        locationManager.delegate = self
        selectlocTextField.delegate = self
        locationManager.requestWhenInUseAuthorization()
        //        locationManager = CLLocationManager()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        //        locationManager.requestAlwaysAuthorization()
        //        mapView.delegate = self
        locationManager.distanceFilter = 50
        locationManager.startUpdatingLocation()
        
    }
    
    func loadmap() {
        let camera = GMSCameraPosition.camera(withLatitude: 28.7041, longitude: 77.1025, zoom: 15)
        mapView = GMSMapView.map(withFrame: self.locView.bounds, camera: camera)
        mapView.settings.myLocationButton = true
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.isMyLocationEnabled = true
        marker?.map = mapView
        mapView.delegate = self
        //adding polygon
        self.addPolygon(array: jeddahCoord)
        self.addPolygon(array: mekkahCoord)
        self.addPolygon(array: jazanCoord)
        
        self.locView.addSubview(mapView)
        mapView.isHidden = true
    }
    
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        // self.mapView.selectedMarker?.position = position.target
        // let position = CLLocation(latitude: position.target.latitude, longitude: <#T##CLLocationDegrees#>)
        //        print(position.target.latitude)
        //              print(position.target.longitude)
        currentLocation = CLLocation(latitude: position.target.latitude, longitude: position.target.longitude)
        marker?.map = nil
        marker = GMSMarker(position: position.target)
        marker?.map = mapView
        
    }
    
    
    
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        
        let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude,
                                              longitude: location.coordinate.longitude,
                                              zoom: 15)
        currentLocation = locations.last
        let position = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude:  location.coordinate.longitude)
        //    marker = GMSMarker(position: position)
        //
        //
        // marker.title = title
        //   marker?.map = mapView
        if mapView.isHidden {
            mapView.isHidden = false
            
            mapView.camera = camera
        } else {
            mapView.animate(to: camera)
        }
        
    }
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
}

extension LocationOfferViewController: GMSAutocompleteViewControllerDelegate {
    
    // Handle the user's selection.
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        if let cord = getcord {
            cord(place.coordinate,place.name)
        }
        selectlocTextField.text = place.name
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
  
    
}


