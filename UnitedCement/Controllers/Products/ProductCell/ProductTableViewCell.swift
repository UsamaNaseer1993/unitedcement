//
//  ProductTableViewCell.swift
//  UnitedCement
//
//  Created by Usama Naseer on 23/10/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit
import SDWebImage

class ProductTableViewCell: UITableViewCell, UITextFieldDelegate {
    var productArray : ProductModel?
    var bagArray : [BagModel]?
    
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var productImageLb: UIImageView!
    @IBOutlet weak var totalPriceLb: UILabel!
    @IBOutlet weak var unitPriceLb: UILabel!
    @IBOutlet weak var addToCartButton: UIButton!
    @IBOutlet weak var noOfTruckLb: UILabel!
    @IBOutlet weak var productNameLb: UILabel!
   
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var noOfTrucksLabel: UILabel!
    @IBOutlet weak var unitPriceLabel: UILabel!
    @IBOutlet weak var totalPriceLabel: UILabel!
    
    @IBOutlet weak var quantityLb: UITextField!
    
    var quantities : [Int] = []
    
    var pickerView = UIPickerView()
    var plus : ((Int)->())?
    var minus : ((Int)->())?
    var quantity : ((Int)->())?
    var cart : (()->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        pickerView.dataSource = self
        pickerView.delegate = self
        quantityLb.delegate = self
        
 // pickerView.isHidden = true;
        quantityLb.endEditing(false)
        quantityLb?.inputView = pickerView
        //  pickerView.selectRow(1, inComponent:1, animated:true)
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return false
    }
    
    @IBAction func cartAction(_ sender: Any) {
        if let cart = cart?() {
            cart
        }
        
    }
    @IBAction func minusAction(_ sender: Any) {
        //  noOfTrucks.text = Int(noOfTrucks.text ?? 0) - 1
        minus?(Int(noOfTruckLb.text ?? "") ?? 1)
    }
    @IBAction func plusAction(_ sender: Any) {
        plus?(Int(noOfTruckLb.text ?? "") ?? 1)
        //  noOfTrucks.text = noOfTrucks.text + 1
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

extension ProductTableViewCell : UIPickerViewDelegate,UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return quantities.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return quantities[row].converToString
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        quantityLb.text = quantities[row].converToString
        quantity?(quantities[row])
        self.pickerView.endEditing(true)
        
    }
   
    
}
