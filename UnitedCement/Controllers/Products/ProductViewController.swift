//
//  ProductViewController.swift
//  UnitedCement
//
//  Created by Usama Naseer on 23/10/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit
import SideMenu

class ProductViewController: UIViewController {
    var productArray : [ProductModel]?
    var bagArray : [BagModel]?
    var city : String?
    @IBOutlet weak var shippingChargesLabel : UILabel!
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var ProductLabel: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        engToArabic()
        UILabel.appearance().font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle(rawValue: "Arabic")).withSize(14)
      

        ApiCall.getAllProducts(completion: { status,products,bags,error in
            if error == nil {
                if status == 200 {
                    self.productArray = products
                    self.bagArray = bags
                    self.tableView.reloadData()
                }
                else if status == 422 {
                    self.showAlert(title: Constants.MESSAGE, message: KMissingParams)
                }
                else {
                    self.showAlert(title: Constants.MESSAGE, message: KEmailExist)
                }
                
            }
            else {
                print(error!.description)
            }
            
        }
        )
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupmenu()
    }
    
    
    func engToArabic() {
        if UserDefaults.standard.string(forKey: "City") == "Jeddah" {
            self.city = "Jeddah"
        } else if UserDefaults.standard.string(forKey: "City") == "Mecca" {
            self.city = "Mecca"
        } else if UserDefaults.standard.string(forKey: "City") == "Jazan" {
            self.city = "Jazan"
        } else {
            self.popBack(1)
        }
        
        if Defaults.isArabicLanguage
            {
                shippingChargesLabel.text = "الاسعار تشمل اجور الشحن"
        }
    }
    func chckTrucks(){
        ApiCall.trucks { (trucks, logout, error) in
            if logout == true {
                self.presentMain()
            } else {
                if trucks != 0 {
                    if let tabItems = self.tabBarController?.tabBar.items {
                        let tabItem = tabItems[1]
                        tabItem.badgeValue = trucks?.converToString
                    }
                    self.tabBarController?.selectedIndex = 1
                } else {
                    if let tabItems = self.tabBarController?.tabBar.items {
                        let tabItem = tabItems[1]
                        tabItem.badgeValue = nil
                    }
                    self.tabBarController?.selectedIndex = 1
                }
            }
        }
    }
    
    @IBAction func sideMenu(_ sender: Any) {
        present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
        
    }
    @IBAction func backAction(_ sender: Any) {
//        popBack(1)
        // Create the alert controller
         var alertController = UIAlertController(title: "Message", message: "Do you want to continue? Your cart will get empty and you have to choose location again", preferredStyle: .alert)
        var ok = "OK"
        var cancel = "Cancel"
        if Defaults.isArabicLanguage {
            alertController = UIAlertController(title: Constants.MESSAGE_AR, message: "هل ترغب في المتابعة؟ ستصبح سلة التسوق فارغة ويجب عليك اختيار الموقع مرة أخرى", preferredStyle: .alert)
            ok = Constants.OK_AR
            cancel = Constants.CANCEL_AR
            
        }
        
        
        // Create the actions
        let okAction = UIAlertAction(title: ok, style: UIAlertAction.Style.default) {
            UIAlertAction in
            ApiCall.emptyCart(completion: { (msg, error) in
                if error == nil {
                    ApiCall.trucks { (trucks, logout, error) in
                        if logout == true {
                            self.presentMain()
                        } else {
                            if trucks != 0 {
                                if let tabItems = self.tabBarController?.tabBar.items {
                                    let tabItem = tabItems[1]
                                    tabItem.badgeValue = trucks?.converToString
                                    UserDefaults.standard.removeObject(forKey: "City")
//                                    UserDefaults.standard.removeObject(forKey: "order_number")
                                    self.popBack(1)
                                }
                            } else {
                                if let tabItems = self.tabBarController?.tabBar.items {
                                    let tabItem = tabItems[1]
                                    tabItem.badgeValue = nil
                                    UserDefaults.standard.removeObject(forKey: "City")
//                                    UserDefaults.standard.removeObject(forKey: "order_number")
                                    self.popBack(1)
                                }
                            }
                        }
                    }
                }
            })
            //NSLog("OK Pressed")
        }
        let cancelAction = UIAlertAction(title: cancel, style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            //NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
}
extension ProductViewController : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return productArray?.count ?? 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var quantities : [Int] = []
        var unit : Double?
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "productcell1") as? ProductTableViewCell else {return UITableViewCell.init()}
        if Defaults.isArabicLanguage
        {
          //cell = tableView.dequeueReusableCell(withIdentifier: "productcell2") as! ProductTableViewCell
            
//            if self.productArray?[indexPath.row].id == 32 {
//                cell.productNameLb.text = "اسمنت بورتلاندي عادي"
//            }
//            else if self.productArray?[indexPath.row].id == 33 {
//               cell.productNameLb.text = "اسمنت مقاوم للكبريتات"
//            }
//            else {
//                cell.productNameLb.text = "اسمنت بورتلاندي بوزولاني"
//            }
            ProductLabel.text = "منتجات"
            //cell.productNameLb.text = "الأسمنت البورتلاندي العادي (OPC)"
            cell.quantityLabel.text =  "الكمية"
            cell.quantityLabel.textAlignment = .center
            cell.noOfTrucksLabel.text = "عدد الشاحنات"
            cell.addToCartButton.setTitle("أضف إلى السلة", for: .normal)
//            cell.addToCartButton.titleLabel?.font = UIFont(name: "Arabic", size: 9)
            cell.addToCartButton.backgroundColor = UIColor(named: "Button")
            cell.addToCartButton.setImage(nil, for: .normal)
            cell.quantityLb.text = "كيس"
            cell.quantityLb.textAlignment = .center
            cell.unitPriceLabel.text = "سعر الوحدة"
            cell.totalPriceLabel.text = "سعر الإجمالي"
           
            cell.productArray = productArray?[indexPath.row]
            cell.bagArray =  bagArray
            cell.productNameLb.text = productArray?[indexPath.row].arabicName
            if self.city == "Jeddah" {
                cell.unitPriceLb.text = "\(productArray?[indexPath.row].price1.converToString ?? "XXX") ر.س"
                unit = productArray?[indexPath.row].price1
                cell.totalPriceLb.text = "0 ر.س"
            } else if self.city == "Jazan" {
                cell.unitPriceLb.text = "\(productArray?[indexPath.row].jazanPrice.converToString ?? "XXX") ر.س"
                unit = productArray?[indexPath.row].jazanPrice
                cell.totalPriceLb.text = "0 ر.س"
            } else {
                cell.unitPriceLb.text = "\(productArray?[indexPath.row].saleprice1.converToString ?? "XXX") ر.س"
                unit = productArray?[indexPath.row].saleprice1
                cell.totalPriceLb.text = "0 ر.س"
            }
            for item in  bagArray ?? []  {
                quantities.append(item.quantity)
            }
            cell.cart = {
                let quantity = Int(cell.quantityLb.text ?? "") ?? 1
                let noOfTrucks = Int(cell.noOfTruckLb.text ?? "") ?? 1
                //                if quantity * noOfTrucks > self.productArray?[indexPath.row].quantity ?? 10000 {
                //                    self.showAlert(title: Constants.MESSAGE, message: "Out Of Stock")
                //                }
                //                else {
                if cell.quantityLb.text != "Bags" && cell.quantityLb.text != "كيس" {
                    ApiCall.addtoCart(id: self.productArray![indexPath.row].id,quantity: noOfTrucks, bags: quantity,
                                      city: self.city ?? "", completion: { (msg, logout, error) in
                        if error == nil {
                            if logout! {
                                self.presentMain()
                                
                            } else {
                           //     self.showAlert(title: Constants.MESSAGE, message: msg)
                                let message  = "تمت إضافة المنتج بنجاح"
                                 self.chckTrucks()
                         //       self.showAlert(title: Constants.MESSAGE, message: msg, completion: {})
                                //  self.popBack(1)
                            }
                        }
                    })
                } else {
                    let msg = "اختيار كمية الأكياس"
                    self.showAlert(title: Constants.MESSAGE_AR, message: msg)
                }
                
                //}
            }
            
            cell.minus = { num in
                if num > 1 {
                    cell.noOfTruckLb.text = String(num - 1)
                    let quantity = (cell.quantityLb.text ?? "").converToDouble
                    let noOfTrucks = (cell.noOfTruckLb.text ?? "").converToDouble
                  //  let unitPrice = cell.unitPriceLb.text?.converToInt ?? 1
                    cell.totalPriceLb.text = "\(String(quantity * noOfTrucks * unit! )) ر.س"
                }
            }
            cell.plus = { num in
                if cell.quantityLb.text != "Bags" && cell.quantityLb.text != "كيس" {
                cell.noOfTruckLb.text = String(num + 1)
                let quantity = (cell.quantityLb.text ?? "").converToDouble
                let noOfTrucks = (cell.noOfTruckLb.text ?? "").converToDouble
             //   let unitPrice = cell.unitPriceLb.text?.converToInt ?? 1
              //  cell.totalPriceLb.text = "SAR \(String(quantity * noOfTrucks * unitPrice ))"
                    cell.totalPriceLb.text = "\(String(quantity * noOfTrucks * unit! )) ر.س" }
            }
            cell.quantity = { quantity in
                let quantity:Double = Double(quantity)
                let noOfTrucks = (cell.noOfTruckLb.text ?? "").converToDouble
//let unitPrice = cell.unitPriceLb.text?.converToInt ?? 1
             //   cell.totalPriceLb.text = "SAR \(String(quantity * noOfTrucks * unitPrice ))"
                 cell.totalPriceLb.text = "\(String(quantity * noOfTrucks * unit! )) ر.س"
            }
           
            cell.quantities = quantities
            cell.productImageLb?.sd_setImage(with: URL(string: "\(productArray?[indexPath.row].images ?? "")"), placeholderImage: UIImage(named: "placeholder.png"))
       //     cell.productNameLb.text = productArray?[indexPath.row].name
            
            
            
            if self.productArray?[indexPath.row].id == 32 {
                cell.cellView.backgroundColor = UIColor(named: "cell1")
            }
            else if self.productArray?[indexPath.row].id == 33 {
                cell.cellView.backgroundColor = UIColor(named: "cell2")
            }
            else {
                cell.cellView.backgroundColor = UIColor(named: "cell3")
            }
            return cell
        }
        else
        {
            cell.productArray = productArray?[indexPath.row]
            cell.bagArray =  bagArray
            if self.city == "Jeddah" {
                cell.unitPriceLb.text = "SAR \(productArray?[indexPath.row].price1.converToString ?? "XXX")"
                unit = (productArray?[indexPath.row].price1)
                cell.totalPriceLb.text = "SAR \(productArray?[indexPath.row].price1.converToString ?? "XXX")"
            } else if self.city == "Jazan" {
                cell.unitPriceLb.text = "\(productArray?[indexPath.row].jazanPrice.converToString ?? "XXX") ر.س"
                 unit = productArray?[indexPath.row].jazanPrice
                cell.totalPriceLb.text = "0 ر.س"
            } else {
                cell.unitPriceLb.text = "SAR \(productArray?[indexPath.row].saleprice1.converToString ?? "XXX")"
                 unit = productArray?[indexPath.row].saleprice1
                cell.totalPriceLb.text = "SAR \(productArray?[indexPath.row].saleprice1.converToString ?? "XXX")"
            }
            
            for item in  bagArray ?? []  {
                quantities.append(item.quantity)
            }
            cell.cart = {
                let quantity = Int(cell.quantityLb.text ?? "") ?? 1
                let noOfTrucks = Int(cell.noOfTruckLb.text ?? "") ?? 1
                //                if quantity * noOfTrucks > self.productArray?[indexPath.row].quantity ?? 10000 {
                //                    self.showAlert(title: Constants.MESSAGE, message: "Out Of Stock")
                //                }
                //                else {
                if cell.quantityLb.text != "Bags" {
                    ApiCall.addtoCart(id: self.productArray![indexPath.row].id,quantity: noOfTrucks, bags: quantity,city: self.city ?? "", completion: { (msg, logout, error) in
                        if error == nil {
                            if logout! {
                                self.presentMain()
                                
                            } else {
//                                self.showAlert(title: Constants.MESSAGE, message: msg, completion: {
//
//                                })
                                
                                 self.chckTrucks()
                                //  self.popBack(1)
                            }
                        }
                    })
                } else {
                    self.showAlert(title: Constants.MESSAGE, message: "Select Bags")
                }
                
                //}
            }
            
            cell.minus = { num in
                if num > 1 {
                    cell.noOfTruckLb.text = String(num - 1)
                    let quantity = (cell.quantityLb.text ?? "").converToDouble
                    let noOfTrucks = (cell.noOfTruckLb.text ?? "").converToDouble
             //       let unitPrice = cell.unitPriceLb.text?.converToInt ?? 1
                    cell.totalPriceLb.text = "SAR \(String(quantity * noOfTrucks * unit! ))"
                }
            }
            cell.plus = { num in
                       if cell.quantityLb.text != "Bags" && cell.quantityLb.text != "كيس" {
                cell.noOfTruckLb.text = String(num + 1)
                let quantity = (cell.quantityLb.text ?? "").converToDouble
                let noOfTrucks = (cell.noOfTruckLb.text ?? "").converToDouble
         //       let unitPrice = cell.unitPriceLb.text?.converToInt ?? 1
                cell.totalPriceLb.text = "SAR \(String(quantity * noOfTrucks * unit! ))"
                }
            }
            cell.quantity = { quantity in
                let quantity:Double = Double(quantity)
                let noOfTrucks = (cell.noOfTruckLb.text ?? "").converToDouble
            //    let unitPrice = cell.unitPriceLb.text?.converToInt ?? 1
                cell.totalPriceLb.text = "SAR \(String(quantity * noOfTrucks * unit! ))"
                
            }
      
            cell.quantities = quantities
            cell.productImageLb?.sd_setImage(with: URL(string: "\(productArray?[indexPath.row].images ?? "")"), placeholderImage: UIImage(named: "placeholder.png"))
            cell.productNameLb.text = productArray?[indexPath.row].name
            
            
            
            if self.productArray?[indexPath.row].id == 32 {
                cell.cellView.backgroundColor = UIColor(named: "cell1")
            }
            else if self.productArray?[indexPath.row].id == 33 {
                cell.cellView.backgroundColor = UIColor(named: "cell2")
            }
            else {
                cell.cellView.backgroundColor = UIColor(named: "cell3")
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    
}
