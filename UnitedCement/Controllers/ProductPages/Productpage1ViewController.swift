//
//  Productpage1ViewController.swift
//  UnitedCement
//
//  Created by Usama Naseer on 12/11/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit
import Auk

class Productpage1ViewController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var aboutUCICLabel: UILabel!
    @IBOutlet weak var theUnitedDescription: UILabel!
    
    @IBAction func backButton(_ sender: Any) {
        popBack(1)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        ApiCall.getBannerImages { (images, link, error) in
            if error == nil {
                for image in images!{
                    //                let a = "https://www.jeddah.space/cement/uploads/images/full/\(image.image ?? "")"
                    self.scrollView.auk.show(url:"https://www.jeddah.space/cement/uploads/images/full/\(image.image ?? "")")
                }
                self.scrollView.auk.startAutoScroll(delaySeconds: 3)
                
            }
        }
        
        //  UINavigationBar.appearance().backgroundColor = UIColor.greenColor()
        // UIBarButtonItem.appearance().tintColor = UIColor.magentaColor()
        
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        engtoArb()
    }
    func engtoArb()
    {
        if Defaults.isArabicLanguage
        {
            aboutUCICLabel.text = "حول UCIC"
            theUnitedDescription.text = "تأسست الشركة المتحدة للأسمنت الصناعية شركة سعودية عام 2013 تحت الرقم التجاري (4030256672). تمتلك الشركة المتحدة للأسمنت الصناعية أحدث مصنع تم بناؤه مؤخراً في المملكة العربية السعودية والمنطقة الغربية وقريبة من مكة المكرمة. قدرة الإنتاج هي 2 مليون طن من الأسمنت سنويا لتلبية احتياجات عملائنا."
        }
    }
    
    
}
