//
//  Productpage4ViewController.swift
//  UnitedCement
//
//  Created by Usama Naseer on 12/11/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit

class Productpage4ViewController: UIViewController {
    
    @IBOutlet weak var sulphateResistanceLabel: UILabel!
    @IBOutlet weak var thisTypeofCementLabel: UILabel!
    @IBOutlet weak var productsLabel: UILabel!
    
    @IBOutlet weak var buyNowButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        engToArabic()
    }
    
    func engToArabic()
    {
        if Defaults.isArabicLanguage
        {
            sulphateResistanceLabel.text = "الأسمنت المقاوم للكبريتات (SRC)"
            sulphateResistanceLabel.font = UIFont.boldSystemFont(ofSize: 12.0)
            thisTypeofCementLabel.text = "هذا النوع من الأسمنت يختلف عن بقية الأنواع الأخرى ، إلى التمييز بين نسبة ألومينات ثلاثي أكسيد الكالسيوم منخفض المحتوى بالألمنيوم منخفض المسئول عن التفاعل مع أيونات الكبريتات في التربة أو الماء مما يمنع زيادة حجم الخرسانة وحدوث الشقوق في المناطق التي تحتوي على التربة نسبة عالية من سلفات."
            productsLabel.text = "منتجات"
            productsLabel.font = UIFont(name: "Arabic", size: 12)
            buyNowButton.setTitle("اشتري الآن", for: .normal)
        }
    }
    
    @IBAction func leftArrowAction(_ sender: Any) {
        let pageController = self.parent as! PageViewController
        
        pageController.setViewControllers([pageController.orderedViewControllers[1]], direction: .reverse, animated: true, completion: nil)
    }
    @IBAction func rightArrowAction(_ sender: Any) {
      //  switchScreen(identifier: "productpage5")
        let pageController = self.parent as! PageViewController
        
        pageController.setViewControllers([pageController.orderedViewControllers[3]], direction: .forward, animated: true, completion: nil)
        
    }
 
    @IBAction func buyNowAcion(_ sender: Any) {
        self.presentMain()
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
