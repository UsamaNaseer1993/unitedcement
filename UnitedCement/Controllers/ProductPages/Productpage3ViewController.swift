//
//  Productpage3ViewController.swift
//  UnitedCement
//
//  Created by Usama Naseer on 12/11/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit

class Productpage3ViewController: UIViewController {
    @IBOutlet weak var productsLabel: UILabel!
    @IBOutlet weak var ordinaryCementLabel: UILabel!
    @IBOutlet weak var applicationmixesLabel: UILabel!
    @IBOutlet weak var buyNowButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        engToArabic()
    }
    
    func engToArabic()
    {
        if Defaults.isArabicLanguage
        {
            ordinaryCementLabel.text = "الاسمنت البورتلاندي العادي(OPC)"
            ordinaryCementLabel.font = UIFont.boldSystemFont(ofSize: 12.0)
            applicationmixesLabel.text = "التطبيقات تخلط الخرسانة المسلحة العادية ، والتي لا تتطلب أنواع خاصة من الأسمنت) التي تحتاج إلى تحمل قوة عالية."
            buyNowButton.setTitle("اشتري الآن", for: .normal)
            productsLabel.text = "منتجات"
               productsLabel.font = UIFont(name: "Arabic", size: 12)
            
        }
    }
    
    @IBAction func rightArrowAction(_ sender: Any) {
       // switchScreen(identifier: "productpage4")
        let pageController = self.parent as! PageViewController
        
        pageController.setViewControllers([pageController.orderedViewControllers[2]], direction: .forward, animated: true, completion: nil)
        
    }
   
    
    @IBAction func buynowAction(_ sender: Any) {
        self.presentMain()
    }
    @IBAction func leftArrowAction(_ sender: Any) {
        let pageController = self.parent as! PageViewController
        
        pageController.setViewControllers([pageController.orderedViewControllers[0]], direction: .reverse, animated: true, completion: nil)
    }
    /*
     
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
