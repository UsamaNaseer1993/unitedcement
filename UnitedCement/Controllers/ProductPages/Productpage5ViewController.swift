//
//  Productpage5ViewController.swift
//  UnitedCement
//
//  Created by Usama Naseer on 12/11/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit

class Productpage5ViewController: UIViewController {
    @IBOutlet weak var portLandPozoLabel: UILabel!
    @IBOutlet weak var buyNowButton: UIButton!
    
    @IBOutlet weak var productsLabel: UILabel!
    @IBOutlet weak var thistypeofLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        engToArabic()
    }
    func engToArabic()
    {
        if Defaults.isArabicLanguage
        {
            portLandPozoLabel.text = "أسمنت بورتلاندي بوزولاني (PPC)"
            portLandPozoLabel.font = UIFont.boldSystemFont(ofSize: 12.0)
            buyNowButton.setTitle("اشتري الآن", for: .normal)
            productsLabel.text = "منتجات"
            productsLabel.font = UIFont(name: "Arabic", size: 12)
            thistypeofLabel.text = "هذا النوع من الأسمنت المتميز بسبب مواصفاته يختلف عن أنواع أخرى من الأسمنت وهو يتكون من (70٪ -80٪) أسمنت بورتلاند زائد (15-20٪) بوتزولانا أسود (بركاني) مادة مئة بالمائة من السيليكون الطبيعي والألمنيوم ، يؤخذ من الجبال البركانية التي لا تتعرض مباشرة لأي عمليات تفاعلية أو مواد كيميائية مضافة لذلك ينصح باستخدامها حيث يوجد أي ضرر أو عيوب في الاستخدامات المختلفة والتي تعطيها قيمة كبيرة ذاتية متقدمة."
        }
    }
    
    @IBAction func leftArrowAction(_ sender: Any) {
        let pageController = self.parent as! PageViewController
        
        pageController.setViewControllers([pageController.orderedViewControllers[2]], direction: .reverse, animated: true, completion: nil)
    }
   
    @IBAction func rightArrowAction(_ sender: Any) {
//        switchScreen(identifier: "LoginOrRegisterViewController")
        
        let pageController = self.parent as! PageViewController
        
        pageController.setViewControllers([pageController.orderedViewControllers[4]], direction: .forward, animated: true, completion: nil)
    }
    @IBAction func buyNowAction(_ sender: Any) {
        self.presentMain()
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
