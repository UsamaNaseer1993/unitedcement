//
//  Productpage2ViewController.swift
//  UnitedCement
//
//  Created by Usama Naseer on 12/11/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit
protocol PageControl {
    func next(controller: UIViewController)
}
class Productpage2ViewController: UIViewController {
    
    var delegate : PageControl?
    @IBOutlet weak var TheunitedCementLabel: UILabel!
    @IBOutlet weak var productsLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        engToArabic()
    }
    
    func engToArabic()
    {
        if Defaults.isArabicLanguage
        {
            TheunitedCementLabel.text = "تتميز شركة الأسمنت المتحدة بإنتاج منتجات أسمنتية ذات جودة عالية ولا تحتوي المواد الخام على أي شيء من جميع الشوائب التي لها تأثير مباشر على منتجات الأسمنت بما في ذلك القلويات ، كلوريدات ، كبريتات ، البوتاسيوم والصوديوم بالإضافة إلى الشركة يقع المحجر في جزء من جبال السروات المعروفة بموادها الخام عالية الجودة. بالإضافة إلى ذلك ، هناك مراقبات آلية لخط الإنتاج الذي يأخذ 100 عينة في اليوم للتأكد من أن المنتجات ذات جودة عالية ، وتؤكد الشركة أنها ذات جودة عالية من خلال إرسال المنتجات إلى المعاهد المحلية والدولية لتأكيد أنها ذات جودة عالية."
            TheunitedCementLabel.textAlignment = .right
            productsLabel.text = "منتجات"
        }
    }
    
    
    @IBAction func leftArrowAction(_ sender: Any) {
        self.popBack(1)
    }
    @IBAction func rightArrowAction(_ sender: Any) {
      //  switchScreen(identifier: "productpage3")
//        delegate?.next(controller:  UIStoryboard(name: "Main", bundle: nil) .
//            instantiateViewController(withIdentifier: "productpage3") )
        let pageController = self.parent as! PageViewController
       // print(pageController.orderedViewControllers[0])
        pageController.setViewControllers([pageController.orderedViewControllers[1]], direction: .forward, animated: true, completion: nil)
      
    }
   
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
