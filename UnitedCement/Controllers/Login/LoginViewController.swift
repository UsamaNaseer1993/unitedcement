//
//  LoginViewController.swift
//  UnitedCement
//
//  Created by Usama Naseer on 12/11/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit
import SideMenu

class LoginViewController: UIViewController {
    @IBOutlet weak var emailTextField:UITextField!
    @IBOutlet weak var passTextField:UITextField!
    @IBOutlet weak var arrowButton:UIButton!
    @IBOutlet weak var loginButton:UIButton!
    @IBOutlet weak var signFacebookButton:UIButton!
    @IBOutlet weak var sideMenuButton:UIButton!
    
    @IBOutlet weak var rightLineLb: UIView!
    @IBOutlet weak var leftlineLb: UIView!
    @IBOutlet weak var orLb: UILabel!
    @IBOutlet weak var loginLabel : UILabel!
    @IBOutlet weak var forgotPassButton : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailTextField.delegate = self as? UITextFieldDelegate
        emailTextField.tag = 0
        //setupmenu()
        signFacebookButton.isHidden = true
        orLb.isHidden = true
        leftlineLb.isHidden = true
        rightLineLb.isHidden = true
        // let viewControllers: [UIViewController] = (self.navigationController?.viewControllers)!
        // Do any additional setup after loading the view.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        // Try to find next responder
        if let nextField = emailTextField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        } else {
            // Not found, so remove keyboard.
            emailTextField.resignFirstResponder()
        }
        // Do not add a line break
        return false
    }

    
    override func viewWillAppear(_ animated: Bool) {
        engToArabic()
    }
    
    func engToArabic() {
        if Defaults.isArabicLanguage
        {
            emailTextField.placeholder = "البريد الإلكتروني"
            emailTextField.textAlignment = .right
            passTextField.placeholder = "كلمه السر"
            passTextField.textAlignment = .right
            loginLabel.text = "تسجيل الدخول"
            forgotPassButton.setTitle("هل نسيت كلمة المرور؟", for: .normal)
            signFacebookButton.setTitle("التوقيع مع الفيسبوك", for: .normal)
           loginButton.setImage(UIImage(named: "login arabic"), for: .normal)
            //loginButton.setTitle("تسجيل الدخول", for: .normal)
            
        }
    }
    
    @IBAction func sideMenuButtonTapped(_sender:UIButton) {
        present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
        
    }
    @IBAction func arrowButtonTapped(_sender:UIButton){
        self.popBack(1)
    }
    @IBAction func loginButtonTapped(_sender:UIButton) {
        if (emailTextField.text?.isBlank)!  ||
            (passTextField.text?.isBlank)!
        {
            showAlert(title: Constants.MESSAGE, message: "Fields cannot be empty")
        } else {
            ApiCall.signin(email: emailTextField.text!, userPass: passTextField.text!) { (user, error) in
                if error == nil  && user != nil {
                    let loginUser : UserModel = user!
                    print(loginUser.verificationStatus)
                    
                    // print("Verified")
                    UserDefaults.standard.set(self.passTextField.text!,forKey: "password")
                    UserDefaults.standard.set(loginUser.id, forKey: "userId")
                    UserDefaults.standard.set(loginUser.firstname, forKey: "firstname")
                    UserDefaults.standard.set(loginUser.lastname, forKey: "lastname")
                    UserDefaults.standard.set(loginUser.email, forKey: "email")
                    self.switchScreen(identifier: "TabbarViewController")
                    
                    
                }
                else {
                    print(error!.description)
                }
                
            }
            
        }
        
    }
    
    @IBAction func signFacebookButtonTapped(_sender:UIButton) {
        
    }
    
    @IBAction func forgotPasswordAction(_ sender: Any) {
        switchScreen(identifier: "PasswordRecoveryViewController")
    }
}
