//
//  OfferTableViewCell.swift
//  UnitedCement
//
//  Created by Usama Naseer on 04/12/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit

class OfferTableViewCell: UITableViewCell {
    
    @IBOutlet weak var productImageLb: UIImageView!
    @IBOutlet weak var cellView: UIView!
    var productArray : OfferModel?
    var bagArray : [BagModel]?
    @IBOutlet weak var quantityPicker: UITextField!
    @IBOutlet weak var productName: UILabel!
    
    @IBOutlet weak var qntyLabel: UILabel!
    @IBOutlet weak var noOfTrucksLabel: UILabel!
    @IBOutlet weak var totalPriceLb: UILabel!
    @IBOutlet weak var unitPriceLabel: UILabel!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var unitPriceLb: UILabel!
    @IBOutlet weak var freeBagsLabel: UILabel!
    @IBOutlet weak var freeBagslb: UILabel!
    @IBOutlet weak var noOfTrucksLb: UILabel!
    @IBOutlet weak var addtoCartButton: UIButton!
    
    var quantities : [Int] = []
    
    var pickerView = UIPickerView()
    var plus : ((Int)->())?
    var minus : ((Int)->())?
    var quantity : ((Int)->())?
    var cart : (()->())?
    override func awakeFromNib() {
        super.awakeFromNib()
//        pickerView.dataSource = self
//        pickerView.delegate = self
//        quantityPicker?.inputView = pickerView
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    @IBAction func addtoCartAction(_ sender: Any) {
        if let cart = cart?() {
            cart
        }
    }
    @IBAction func add(_ sender: Any) {
        plus?(Int(noOfTrucksLb.text ?? "") ?? 1)
    }
    @IBAction func minus(_ sender: Any) {
        minus?(Int(noOfTrucksLb.text ?? "") ?? 1)
    }
  
}

//extension OfferTableViewCell : UIPickerViewDelegate,UIPickerViewDataSource {
//    func numberOfComponents(in pickerView: UIPickerView) -> Int
//    {
//        return 1
//    }
//
//    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
//        return quantities.count
//    }
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        return quantities[row].converToString
//    }
//
//    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        quantityPicker.text = quantities[row].converToString
//        quantity?(quantities[row])
//        self.pickerView.endEditing(false)
//
//    }
//
//}
