//
//  OffersViewController.swift
//  UnitedCement
///Users/usamanaseer/Documents/Projects/United Cement/unitedcement/UnitedCement/Controllers/Offers/OffersViewController.swift
//  Created by Usama Naseer on 12/11/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit
import  SideMenu
class OffersViewController: UIViewController {
    var offerArray : [OfferModel]?
    var bagArray : [BagModel]?
    @IBOutlet weak var shippingChargesLabel : UILabel!
 
    
    var city : String?
 
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var offersLabel: UILabel!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UILabel.appearance().font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle(rawValue: "Arabic")).withSize(14)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        initViews()
        if UserDefaults.standard.string(forKey: "City") == "Jeddah" {
            self.city = "Jeddah"
            self.getOffers()
        } else if UserDefaults.standard.string(forKey: "City") == "Mecca" {
            self.city = "Mecca"
            self.getOffers()
        } else if UserDefaults.standard.string(forKey: "City") == "Jazan" {
            self.city = "Jazan"
        } else {
            self.popBack(1)
        }
        
        if Defaults.isArabicLanguage
        {
            shippingChargesLabel.text = "الاسعار تشمل اجور الشحن"
            offersLabel.text = "عروض"
        }
//        } else {
//            showAleert()
//        }
        
      
    }
    
    @IBAction func sideMenu(_ sender: Any) {
        present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupmenu()
        

        // Do any additional setup after loading the view.
    }
    @IBAction func backAction(_ sender: Any) {
       // popBack(1)
        let alertController = UIAlertController(title: "Message", message: "Do you want to continue? Your cart will get empty and you have to choose location again", preferredStyle: UIAlertController.Style.alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            ApiCall.emptyCart(completion: { (msg, error) in
                if error == nil {
                    ApiCall.trucks { (trucks, logout, error) in
                        if logout == true {
                            self.presentMain()
                        } else {
                            if trucks != 0 {
                                if let tabItems = self.tabBarController?.tabBar.items {
                                    let tabItem = tabItems[1]
                                    tabItem.badgeValue = trucks?.converToString
                                    UserDefaults.standard.removeObject(forKey: "City")
                                    UserDefaults.standard.removeObject(forKey: "order_number")
                                    self.popBack(1)
                                }
                            } else {
                                if let tabItems = self.tabBarController?.tabBar.items {
                                    let tabItem = tabItems[1]
                                    tabItem.badgeValue = nil
                                    UserDefaults.standard.removeObject(forKey: "City")
                                    UserDefaults.standard.removeObject(forKey: "order_number")
                                    self.popBack(1)
                                }
                            }
                        }
                    }
                   
                }
            })
            //NSLog("OK Pressed")
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            //NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    func initViews() {
        self.offerArray?.removeAll()
        self.tableView.reloadData()
        //self.getOffers()

        }
    func chckTrucks(){
        ApiCall.trucks { (trucks, logout, error) in
            if logout == true {
                self.presentMain()
            } else {
                if trucks != 0 {
                    if let tabItems = self.tabBarController?.tabBar.items {
                        let tabItem = tabItems[1]
                        tabItem.badgeValue = trucks?.converToString
                                            }
                    self.tabBarController?.selectedIndex = 1
                } else {
                    if let tabItems = self.tabBarController?.tabBar.items {
                        let tabItem = tabItems[1]
                        tabItem.badgeValue = nil
                    }
                    self.tabBarController?.selectedIndex = 1
                }
            }
        }
    }
  
    func getOffers() {
        ApiCall.getAllOffers(completion: { status,offers,bags,error in
            if error == nil {
                if status == 200 {
                    self.offerArray = offers
                    self.bagArray = bags
                    self.tableView.reloadData()
                }
                else if status == 422 {
                    self.showAlert(title: Constants.MESSAGE, message: KMissingParams)
                }
                else {
                    self.showAlert(title: Constants.MESSAGE, message: KEmailExist)
                }
                
            }
            else {
                print(error!.description)
            }
            
        }
        )
    }

}


extension OffersViewController : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return offerArray?.count ?? 0  }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var quantities : [Int] = []
        var unit : Int?
        if Defaults.isArabicLanguage
        {
            UILabel.appearance().font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle(rawValue: "Arabic")).withSize(14)
            
            offersLabel.text = "عروض"
            let cell = tableView.dequeueReusableCell(withIdentifier: "ordercell1") as! OfferTableViewCell
            cell.qntyLabel.text = "كيس"
            cell.noOfTrucksLabel.text = "عدد الشاحنات"
            cell.freeBagsLabel.text = "أكياس مجانية"
            cell.unitPriceLabel.text = "سعر الوحدة"
            cell.totalPriceLabel.text = "السعر الإجمالي"
            cell.addtoCartButton.setImage(UIImage(named: "addArabic"), for: .normal)
            
            cell.productArray = offerArray?[indexPath.row]
            cell.bagArray =  bagArray
            if city == "Jeddah" {
                cell.unitPriceLb.text = "\(offerArray?[indexPath.row].discountPrice.converToString ?? "XXX") ر.س"
               unit = offerArray?[indexPath.row].discountPrice
                cell.totalPriceLb.text = "\(offerArray?[indexPath.row].discountPrice.converToString ?? "XXX") ر.س"
            } else if self.city == "Jazan" {
                cell.unitPriceLb.text = "\(offerArray?[indexPath.row].jazanPrice.converToString ?? "XXX") ر.س"
                unit = offerArray?[indexPath.row].jazanPrice
                cell.totalPriceLb.text = "0 ر.س"
            } else {
                cell.unitPriceLb.text = "\(offerArray?[indexPath.row].makkahPrice.converToString ?? "XXX") ر.س"
                unit = offerArray?[indexPath.row].makkahPrice
                cell.totalPriceLb.text = "\(offerArray?[indexPath.row].makkahPrice.converToString ?? "XXX") ر.س"
            }
        
            cell.freeBagslb.text = offerArray?[indexPath.row].discountQuantity.converToString
                    cell.quantityPicker.text = offerArray?[indexPath.row].minimumQuantity.converToString
            cell.freeBagslb.textAlignment = .center
            for item in  bagArray ?? []  {
                quantities.append(item.quantity)
            }
            cell.cart = {
                let quantity = Int(cell.quantityPicker.text ?? "") ?? 1
                let noOfTrucks = Int(cell.noOfTrucksLb.text ?? "") ?? 1
     
                if cell.quantityPicker.text != "Bags"  &&  cell.quantityPicker.text != "كيس" {
                    ApiCall.addtoCartOffer(id: self.offerArray![indexPath.row].id, min_quantity: self.offerArray![indexPath.row].minimumQuantity, discount_quantity: self.offerArray![indexPath.row].discountQuantity, quantity: noOfTrucks,bags: quantity,city: self.city ?? "", completion: { (msg, logout, error) in
                        if error == nil {
                            if logout! {
                                self.presentMain()
                                
                            } else {
                                self.showAlert(title: Constants.MESSAGE, message: msg, completion: {
                                    self.chckTrucks()
                                })
                                //  self.popBack(1)
                            }
                        }
                    })
                } else {
                    self.showAlert(title: Constants.MESSAGE, message: "Select Bags")
                }
  
            }
            
            cell.minus = { num in
                if num > 1 {
                    cell.noOfTrucksLb.text = String(num - 1)
                    let quantity = Int(cell.quantityPicker.text ?? "") ?? 1
                    let noOfTrucks = Int(cell.noOfTrucksLb.text ?? "") ?? 1
                 //   let unitPrice = cell.unitPriceLb.text?.converToInt ?? 1
              //      cell.totalPriceLb.text = "SAR \(String(quantity * noOfTrucks * unitPrice ))"
                    cell.totalPriceLb.text = "\(String(quantity * noOfTrucks * unit! )) ر.س"
                }
            }
            cell.plus = { num in
                if cell.qntyLabel.text != "Bags" && cell.qntyLabel.text != "كيس" {
                cell.noOfTrucksLb.text = String(num + 1)
                let quantity = Int(cell.quantityPicker.text ?? "") ?? 1
                let noOfTrucks = Int(cell.noOfTrucksLb.text ?? "") ?? 1
            //    let unitPrice = cell.unitPriceLb.text?.converToInt ?? 1
//                cell.totalPriceLb.text = "SAR \(String(quantity * noOfTrucks * unitPrice ))"
                    cell.totalPriceLb.text = "\(String(quantity * noOfTrucks * unit! )) ر.س" }
            }
            cell.quantity = { quantity in
                let quantity = quantity
                let noOfTrucks = Int(cell.noOfTrucksLb.text ?? "") ?? 1
              //  let unitPrice = cell.unitPriceLb.text?.converToInt ?? 1
                cell.totalPriceLb.text = "\(String(quantity * noOfTrucks * unit! )) ر.س"
                
            }
           
            cell.quantities = quantities
            cell.productImageLb?.sd_setImage(with: URL(string: "\(offerArray?[indexPath.row].images ?? "")"), placeholderImage: UIImage(named: "placeholder.png"))
            cell.productName.text = offerArray?[indexPath.row].name
            
            
            
            if self.offerArray?[indexPath.row].id == 33 {
                cell.cellView.backgroundColor = UIColor(named: "cell2")
            }
            else if self.offerArray?[indexPath.row].id == 32 {
                cell.cellView.backgroundColor = UIColor(named: "cell1")
            }
            else {
                cell.cellView.backgroundColor = UIColor(named: "cell3")
            }
            return cell
        }
            
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ordercell1") as! OfferTableViewCell
            cell.productArray = offerArray?[indexPath.row]
            cell.bagArray =  bagArray
            if city == "Jeddah" {
                cell.unitPriceLb.text = "SAR \(offerArray?[indexPath.row].discountPrice.converToString ?? "XXX")"
                unit = offerArray?[indexPath.row].discountPrice
                cell.totalPriceLb.text = "SAR \(offerArray?[indexPath.row].discountPrice.converToString ?? "XXX")"
            } else if self.city == "Jazan" {
                cell.unitPriceLb.text = "\(offerArray?[indexPath.row].jazanPrice.converToString ?? "XXX") ر.س"
                unit = offerArray?[indexPath.row].jazanPrice
                cell.totalPriceLb.text = "0 ر.س"
            }  else {
                cell.unitPriceLb.text = "SAR \(offerArray?[indexPath.row].makkahPrice.converToString ?? "XXX")"
                unit = offerArray?[indexPath.row].makkahPrice
                cell.totalPriceLb.text = "SAR \(offerArray?[indexPath.row].makkahPrice.converToString ?? "XXX")"
            }
            cell.freeBagslb.text = offerArray?[indexPath.row].discountQuantity.converToString
            cell.quantityPicker.text = offerArray?[indexPath.row].minimumQuantity.converToString
            
            cell.freeBagslb.textAlignment = .center
            for item in  bagArray ?? []  {
                quantities.append(item.quantity)
            }
            cell.cart = {
                let quantity = Int(cell.quantityPicker.text ?? "") ?? 1
                let noOfTrucks = Int(cell.noOfTrucksLb.text ?? "") ?? 1
                //                if quantity * noOfTrucks > self.productArray?[indexPath.row].quantity ?? 10000 {
                //                    self.showAlert(title: Constants.MESSAGE, message: "Out Of Stock")
                //                }
                //                else {
                if cell.quantityPicker.text != "Bags"  &&  cell.quantityPicker.text != "" {
                    ApiCall.addtoCartOffer(id: self.offerArray![indexPath.row].id, min_quantity: self.offerArray![indexPath.row].minimumQuantity, discount_quantity: self.offerArray![indexPath.row].discountQuantity, quantity: noOfTrucks,bags: quantity,city: self.city ?? "", completion: { (msg, logout, error) in
                        if error == nil {
                            if logout! {
                                self.presentMain()
                                
                            } else {
                                self.showAlert(title: Constants.MESSAGE, message: msg, completion: {
                                    self.chckTrucks()
                                })
                                //  self.popBack(1)
                            }
                        }
                    })
                } else {
                    self.showAlert(title: Constants.MESSAGE, message: "Select Bags")
                }
                
                //}
            }
            
            cell.minus = { num in
                if num > 1 {
                    cell.noOfTrucksLb.text = String(num - 1)
                    let quantity = Int(cell.quantityPicker.text ?? "") ?? 1
                    let noOfTrucks = Int(cell.noOfTrucksLb.text ?? "") ?? 1
                 //   let unitPrice = cell.unitPriceLb.text?.converToInt ?? 1
                    cell.totalPriceLb.text = "SAR \(String(quantity * noOfTrucks * unit! ))"
                }
            }
            cell.plus = { num in
                  if cell.qntyLabel.text != "Bags" && cell.qntyLabel.text != "كيس" {
                cell.noOfTrucksLb.text = String(num + 1)
                let quantity = Int(cell.quantityPicker.text ?? "") ?? 1
                let noOfTrucks = Int(cell.noOfTrucksLb.text ?? "") ?? 1
              //  let unitPrice = cell.unitPriceLb.text?.converToInt ?? 1
                cell.totalPriceLb.text = "SAR \(String(quantity * noOfTrucks * unit! ))"
                }
            }
            cell.quantity = { quantity in
                let quantity = quantity
                let noOfTrucks = Int(cell.noOfTrucksLb.text ?? "") ?? 1
            //    let unitPrice = cell.unitPriceLb.text?.converToInt ?? 1
                cell.totalPriceLb.text = "SAR \(String(quantity * noOfTrucks * unit! ))"
                
            }
 
            cell.quantities = quantities
            cell.productImageLb?.sd_setImage(with: URL(string: "\(offerArray?[indexPath.row].images ?? "")"), placeholderImage: UIImage(named: "placeholder.png"))
            cell.productName.text = offerArray?[indexPath.row].name
            
            
            
            if self.offerArray?[indexPath.row].id == 33 {
                cell.cellView.backgroundColor = UIColor(named: "cell2")
            }
            else if self.offerArray?[indexPath.row].id == 32 {
                cell.cellView.backgroundColor = UIColor(named: "cell1")
            }
            else {
                cell.cellView.backgroundColor = UIColor(named: "cell3")
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
}

