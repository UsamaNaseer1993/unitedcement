//
//  TermsAndConditionsViewController.swift
//  UnitedCement
//
//  Created by Muhammad Irfan on 1/19/19.
//  Copyright © 2019 Usama Naseer. All rights reserved.
//

import UIKit

class TermsAndConditionsViewController: UIViewController {
    @IBOutlet weak var thebuyerLabel: UILabel!
    @IBOutlet weak var conditionLabel: UILabel!
    @IBOutlet weak var termsLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func backButtonTapped(_ sender: Any) {
       self.popBack(1)
    }
    
    func EngToArabic()
    {
        termsLabel.text = "شروط وأحكام خدمات البيع الإلكتروني"
        conditionLabel.text = "شروط وأحكام خدمات البيع الإلكتروني"
        thebuyerLabel.text = "1يقر المشتري بأن المعلومات المقدمة منه صحيحة ودقيقة وكافية لتنفيذ طلب الشراء.2.يقر المشتري بأهليته القانونية والمعتبرة شرعاً للتعاقد والشراء من البائع.    3.يقر المشتري بأن البطاقة الإئتمانية المستخدمة في عملية الشراء مسجلة بإسمه.4.يقر المشتري بأن للبائع الحق في مراجعة الأسعار وتعديلها وفقاً لمقتضيات الأحوال.5.يجب على المشتري التأكد من توفر الوسائل اللازمة لسرعة تسليم المنتج ويكون وحده مسؤولاً عن أي أضرار أو تأخير.        6.يجب على المشتري إبلاغ البائع وعلى الفور من خلال موقعه الإلكتروني عن رغبته في إلغاء الطلبية ويحتفظ البائع بحقه في رفض الإلغاء في حالة مغادرة الناقل للمصنع أو في طريقه إلى نقطة التسليم."
    }
    
}
