//
//  RegisterViewController.swift
//  UnitedCement
//
//  Created by Usama Naseer on 12/11/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController, UITextFieldDelegate {
    
    
    @IBOutlet weak var firstNameTextField:UITextField!
    @IBOutlet weak var lastNameTextField:UITextField!
    @IBOutlet weak var mobileNameTextField:UITextField!
    @IBOutlet weak var emailNameTextField:UITextField!
    @IBOutlet weak var companyNameTextField:UITextField!
    @IBOutlet weak var passTextField:UITextField!
    @IBOutlet weak var confirmPassTextField:UITextField!
    @IBOutlet weak var arrowbackButton:UIButton!
    @IBOutlet weak var registerButton:UIButton!
    @IBOutlet weak var orLb: UILabel!
    @IBOutlet weak var signFacebookButton:UIButton!
    @IBOutlet weak var registerOutlet : UILabel!
    @IBOutlet weak var checkboxLb: UIButton!
    @IBOutlet weak var rightlineLB: UIView!
    @IBOutlet weak var leftlineLb: UIView!
    @IBOutlet weak var termsAndConditionButton : UIButton!
    @IBOutlet weak var iAgreeTerms: UILabel!
    
    var unchecked = true
    override func viewDidLoad() {
        super.viewDidLoad()
        firstNameTextField.delegate = self
        firstNameTextField.tag = 0
        lastNameTextField.tag = 1
        mobileNameTextField.tag = 2
        emailNameTextField.tag = 3
        companyNameTextField.tag = 4
        passTextField.tag = 5
        confirmPassTextField.tag = 6
        signFacebookButton.isHidden = true
        orLb.isHidden = true
        rightlineLB.isHidden = true
        leftlineLb.isHidden = true
//        termsAndConditionButton.isEnabled = false
        mobileNameTextField.delegate = self
        // Do any additional setup after loading the view.
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        // Try to find next responder
        if let nextField = firstNameTextField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            nextField.becomeFirstResponder()
        
        } else {
            // Not found, so remove keyboard.
            firstNameTextField.resignFirstResponder()
        }
        // Do not add a line break
        return false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let count = text.count + string.count - range.length
        return count <= 10
    }
    override func viewWillAppear(_ animated: Bool) {
        setUpView()
    }
    
    let yourAttributes : [NSAttributedString.Key: Any] = [
        NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14),
        NSAttributedString.Key.foregroundColor : UIColor.blue,
        NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue]
    
    func setUpView()
    {
        if Defaults.isArabicLanguage
        {
        registerOutlet.text = "تسجيل"
        firstNameTextField.placeholder = "الاسم الاول"
        firstNameTextField.textAlignment = .right
        lastNameTextField.placeholder = "اسم العائلة"
        lastNameTextField.textAlignment = .right
        mobileNameTextField.placeholder = "رقم الجوال"
        mobileNameTextField.textAlignment = .right
        emailNameTextField.placeholder = "البريد الإلكتروني"
        emailNameTextField.textAlignment = .right
        companyNameTextField.placeholder = "اسم الشركة"
        companyNameTextField.textAlignment = .right
        passTextField.placeholder = "كلمه السر"
        passTextField.textAlignment = .right
        confirmPassTextField.placeholder = "تأكيد كلمة المرور"
        confirmPassTextField.textAlignment = .right
        iAgreeTerms.text = "أنا أوافق على"
        iAgreeTerms.textAlignment = .right
            let attributeString = NSMutableAttributedString(string: "الأحكام والشروط",
                                                            attributes: yourAttributes)
            termsAndConditionButton.setAttributedTitle(attributeString, for: .normal)
        termsAndConditionButton.titleLabel?.textColor = .black
     //   termsAndConditionButton.setTitle("شروط وأحكام", for: .normal)
        termsAndConditionButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 14)
        signFacebookButton.setTitle("التوقيع مع الفيسبوك" , for: .normal)
        registerButton.titleLabel?.font = UIFont(name: "Arabic", size:12)
        registerButton.setTitle("تسجيل" , for: .normal)
        orLb.text = "أو"
        orLb.textAlignment = .center
        } else {
            registerButton.setTitle("Register", for: .normal)
        }
}
    @IBAction func arrowBackButtonTapped(_sender:UIButton)
    {
        self.popBack(1)
    }
    @IBAction func checkAction(_ sender: Any) {
        if unchecked {
            (sender as AnyObject).setImage(UIImage(named:"checked.png"), for: .normal)
            unchecked = false
        } else {
            (sender as AnyObject).setImage( UIImage(named:"unchecked.png"), for: .normal)
            unchecked = true
        }
    }
    @IBAction func registerButtonTapped(_sender:UIButton)
    {
        if (firstNameTextField.text?.isBlank)!  ||
            (lastNameTextField.text?.isBlank)! ||
            (mobileNameTextField.text?.isBlank)!  ||                                                                    (emailNameTextField.text?.isBlank)!  ||
            (passTextField.text?.isBlank)! ||
            (confirmPassTextField.text?.isBlank)! {
            showAlert(title: Constants.MESSAGE, message: "Fields cannot be empty")
        } else if passTextField.text != confirmPassTextField.text {
            showAlert(title: Constants.MESSAGE, message: "Password didnt match")
        } else if unchecked {
            showAlert(title: Constants.MESSAGE, message: "Please agree the terms and conditions")
        }
        else {
            ApiCall.register(firstName: firstNameTextField.text!, lastName: lastNameTextField.text!, password: passTextField.text!, email: emailNameTextField.text!, mobile: mobileNameTextField.text!, company_name: companyNameTextField.text!, c_password: confirmPassTextField.text!, completion: { (user, error) in
                if error == nil  && user != nil {
                    let loginUser : UserModel = user!
                  var message = ""
                   message = Defaults.isArabicLanguage ? "المستخدم مسجل بنجاح" : "User Successfully Registered"
                    self.showAlert(title: Constants.MESSAGE, message: message, completion: {
                        UserDefaults.standard.set(loginUser.id, forKey: "userId")
                        UserDefaults.standard.set(loginUser.firstname, forKey: "firstname")
                        UserDefaults.standard.set(loginUser.lastname, forKey: "lastname")
                        UserDefaults.standard.set(loginUser.email, forKey: "email")
                        UserDefaults.standard.set(loginUser.password, forKey: "password")
                        
                        self.switchScreen(identifier: "TabbarViewController") })
                    
                }
                else {
                    print(error!.description)
                }
                
            })
            
        }
    }
    @IBAction func signFacbookButtonTapped(_sender:UIButton)
    {}
    
    @IBAction func readTermsButtonTapped(_ sender: UIButton) {
       switchScreen(identifier:"TermsAndConditionsViewController")
    }
}
