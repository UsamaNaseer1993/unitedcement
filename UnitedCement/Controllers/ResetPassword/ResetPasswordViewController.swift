//
//  ResetPasswordViewController.swift
//  UnitedCement
//
//  Created by Usama Naseer on 12/11/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit

class ResetPasswordViewController: UIViewController {
    var email, code : String?
    @IBOutlet weak var arrowButton:UIButton!
    @IBOutlet weak var sideMenuButton:UIButton!
    @IBOutlet weak var continueButton:UIButton!
    @IBOutlet weak var newPassTextField:UITextField!
    @IBOutlet weak var confPassTextField:UITextField!
    @IBOutlet weak var ResetpassLabel: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        engToArabic()
    }
    func engToArabic()
    {
        if Defaults.isArabicLanguage
        {
            continueButton.setTitle("استمر", for: .normal)
            newPassTextField.placeholder = "كلمة السر الجديدة"
            newPassTextField.textAlignment = .right
            confPassTextField.placeholder = "تأكيد كلمة المرور"
            confPassTextField.textAlignment = .right
            ResetpassLabel.text = "إعادة تعيين كلمة المرور"
            
        }
    }
    
    @IBAction func arrowButtonTapped(_sender:UIButton) {
        if let navigator = self.navigationController {
            navigator.popViewController(animated: true)
        }
    }
    @IBAction func sideMenuButtonTapped(_sender:UIButton) {
        
    }
    @IBAction func continueButtonTapped(_sender:UIButton) {
        if newPassTextField.text?.isBlank == false && confPassTextField.text?.isBlank == false {
            if newPassTextField.text == confPassTextField.text {
                ApiCall.resetPassword(email: email!, code: code!, password: newPassTextField.text!) { (code,msg, error) in
                    if error == nil {
                        if code == 200 {
                            self.showAlert(title: Constants.MESSAGE, message: msg, completion: {
                                self.popBack(3)
                            })
                        }
                        else {
                            self.showAlert(title: Constants.MESSAGE, message: msg, completion: {
                                self.popBack(1)
                            })
                        }
                        
                        
                    }
                }
                
            }
            else {
                self.showAlert(title: Constants.MESSAGE, message: KMatchpass)
            }
        }
        else {
            showAlert(title: Constants.MESSAGE, message: KFieldsNotEmpty)
        }
    }
}
