//
//  OrderTableViewCell.swift
//  UnitedCement
//
//  Created by Usama Naseer on 25/11/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//


import UIKit

class OrderTableViewCell: UITableViewCell,UITextFieldDelegate {
    
    var productArray : ProductModel?
    var bagArray : [BagModel]?
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var productImageLb: UIImageView!
    @IBOutlet weak var totalPriceLb: UILabel!
    @IBOutlet weak var unitPriceLb: UILabel!
    
    @IBOutlet weak var qtyLabelArabic: UILabel!
    @IBOutlet weak var noOfTrucksArabic: UILabel!
    @IBOutlet weak var totalArabic: UILabel!
    @IBOutlet weak var unitArbic: UILabel!
    @IBOutlet weak var freeBagsQuantityLb: UILabel!
    @IBOutlet weak var freeBagsLb: UILabel!
    @IBOutlet weak var noOfTruckLb: UILabel!
    @IBOutlet weak var productNameLb: UILabel!
    @IBOutlet weak var quantityLb: UITextField!
    @IBOutlet weak var updateCartButton: UIButton!
    
    
    var quantities : [Int] = []
    
    var pickerView = UIPickerView()
    var plus : ((Int)->())?
    var minus : ((Int)->())?
    var quantity : ((Int)->())?
    var cart : (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        pickerView.dataSource = self
        quantityLb.delegate = self
        pickerView.delegate = self
        quantityLb?.inputView = pickerView
        // Initialization code
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return false
    }
    
    @IBAction func cartAction(_ sender: Any) {
        if let cart = cart?() {
            cart
        }
        
    }
    @IBAction func minusAction(_ sender: Any) {
        //  noOfTrucks.text = Int(noOfTrucks.text ?? 0) - 1
        minus?(Int(noOfTruckLb.text ?? "") ?? 1)
    }
    @IBAction func plusAction(_ sender: Any) {
        plus?(Int(noOfTruckLb.text ?? "") ?? 1)
        //  noOfTrucks.text = noOfTrucks.text + 1
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
extension OrderTableViewCell : UIPickerViewDelegate,UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return quantities.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return quantities[row].converToString
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        quantityLb.text = quantities[row].converToString
        quantity?(quantities[row])
        self.pickerView.endEditing(false)
        
    }
    
}

