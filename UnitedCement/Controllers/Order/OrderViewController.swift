//
//  OrderViewController.swift
//  UnitedCement
//
//  Created by Usama Naseer on 12/11/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit
import SideMenu
import Alamofire
import NVActivityIndicatorView
import SafariServices

protocol passAddressDelegate {
    func locationData(name: String,addressId : Int,location : String)
}

class OrderViewController: UIViewController , passAddressDelegate,UITextFieldDelegate ,SFSafariViewControllerDelegate{
    var error : String = "error"
    var safariVC: SFSafariViewController?
    var checkoutID : String?
    let provider = OPPPaymentProvider(mode: OPPProviderMode.test)
    var transaction: OPPTransaction?
    @IBOutlet weak var sideMenuButton:UIButton!
    var cartProductsArray : [cartProductModel] = []
    var bagsArray : [BagModel] = []
    var billingAddId, shippingAddId : Int?
    
    @IBOutlet weak var dateLb: UILabel!
    @IBOutlet weak var ShippingAddButton: UIButton!
    @IBOutlet weak var billingAddButton: UIButton!
    @IBOutlet weak var checkOutButton:UIButton!
    @IBOutlet weak var shippingChargesLabel : UILabel!
    @IBOutlet weak var couponImage: UIImageView!
    @IBOutlet weak var VAT: UILabel!
    @IBOutlet weak var subTotal: UILabel!
    @IBOutlet weak var heightforTableView: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    let datePicker = UIDatePicker()
    var city: String?
    var total : String?
    var dtPicker : Bool?
    @IBOutlet weak var orderLabel: UILabel!
    @IBOutlet weak var billingAddLabel: UILabel!
    @IBOutlet weak var shippingAddLabel: UILabel!
    @IBOutlet weak var tytPaymentType: UITextField!
    @IBOutlet weak var txtDatePicker: UITextField!
    @IBOutlet weak var deliveryDate: UILabel!
    @IBOutlet weak var subTotalLabel: UILabel!
    @IBOutlet weak var addCouponLabel: UILabel!
    @IBOutlet weak var grandTotal: UILabel!
    @IBOutlet weak var checkOutLabel: UILabel!
    @IBOutlet weak var couponTextField: UITextField!
    @IBOutlet weak var paymentTypeTextField: UITextField!
    @IBOutlet weak var grandTotalLabel: UILabel!
     var addressArray : [AddressModel]?
   
    @IBOutlet weak var leftStackLeft: NSLayoutConstraint!
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var leftStackRight: NSLayoutConstraint!
    @IBOutlet weak var VatLabel: UILabel!
    let types = [ "BANK TRANSFER", "Credit\\Debit Card Payment"]
//    let typesArabic = ["الدفع عن الاستلام","تحويل مصرفي"]
    let typesArabic = ["التحويل المصرفي","بطاقة الائتمانية"]
    
    var pickerView = UIPickerView()
    
    @IBAction func topButton(_ sender: Any) {
        var msg = "Do you want to continue? Your cart will get empty and you have to choose location again"
        var title = "Message"
        var ok = "OK"
        var cancel = "Cancel"
        if Defaults.isArabicLanguage {
            msg = "هل ترغب في المتابعة؟ ستصبح سلة التسوق فارغة ويجب عليك اختيار الموقع مرة أخرى"
            title = "رسالة"
            ok = "حسنا"
            cancel = "إلغاء"
        }
        let alertController = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: ok, style: UIAlertAction.Style.default) {
            UIAlertAction in
            ApiCall.emptyCart(completion: { (msg, error) in
                if error == nil {
                    ApiCall.trucks { (trucks, logout, error) in
                        if logout == true {
                            self.presentMain()
                        } else {
                           
                            if trucks != 0 {
                                if let tabItems = self.tabBarController?.tabBar.items {
                                    let tabItem = tabItems[1]
                                    tabItem.badgeValue = trucks?.converToString
                                    UserDefaults.standard.removeObject(forKey: "City")
                                    UserDefaults.standard.removeObject(forKey: "order_number")
                                     self.tabBarController?.selectedIndex = 0
                                }
                            } else {
                                if let tabItems = self.tabBarController?.tabBar.items {
                                    let tabItem = tabItems[1]
                                    tabItem.badgeValue = nil
                                    UserDefaults.standard.removeObject(forKey: "City")
                                    UserDefaults.standard.removeObject(forKey: "order_number")
                                     self.tabBarController?.selectedIndex = 0
                                }
                            }
                        }
                    }
                }
            })
            //NSLog("OK Pressed")
        }
        let cancelAction = UIAlertAction(title: cancel, style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            //NSLog("Cancel Pressed")
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        UILabel.appearance().font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle(rawValue: "Arabic")).withSize(14)
        if transaction?.type == .asynchronous {
           // print(transaction?.paymentParams)
           transaction = nil
           self.checkout(Check: true)
        
        } else {
            
            if let text = self.billingAddButton.titleLabel?.text ,
                let text1 = self.ShippingAddButton.titleLabel?.text {
                if text == "CHOOSE ADDRESS"  || text == "اختر العنوان" || text1 == "CHOOSE ADDRESS"  || text1 == "اختر العنوان" {
                  addresses()
                } else {
                shoppingCart()
                }
                
            }
         
        }

        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        super.viewWillAppear(animated)
    }
    
    func addresses() {
        ApiCall.getAddresses { (addresses,logout, error)  in
            if error == nil {
                if logout! {
                    self.presentMain()
                } else {
                    self.shoppingCart()
                    if (addresses?.isEmpty)!  {
                        if Defaults.isArabicLanguage{
                            self.ShippingAddButton.setTitle("اختر العنوان", for: .normal)
                            self.shippingAddId = 0
                            self.billingAddButton.setTitle("اختر العنوان", for: .normal)
                            self.billingAddId = 0
                        }
                        else{
                        self.ShippingAddButton.setTitle("CHOOSE ADDRESS", for: .normal)
                        self.shippingAddId = 0
                        self.billingAddButton.setTitle("CHOOSE ADDRESS", for: .normal)
                        self.billingAddId = 0
                        }
                    }
                    else {
                        self.addressArray = addresses
                        self.billingAddId = self.addressArray?[0].id
                        self.billingAddButton.setTitle("\(self.addressArray?[0].address1 ?? "")",for: .normal)
                        self.shippingAddId = self.addressArray?[0].id
                        self.ShippingAddButton.setTitle("\(self.addressArray?[0].address1 ?? "")", for: .normal)
                    }            
                }
            }
        }
    }
    
    func shoppingCart(){
        ApiCall.shoppingCart(shippingId: shippingAddId?.converToString ?? "") { (carts, logout, startDate, endDate,bags,last, error) in
            if error == nil {
                if logout! {
                    self.presentMain()
                } else {
                    if startDate != "Empty Cart" && startDate != "cart is empty" {
                        self.datePicker.minimumDate = self.convertToDate(isoDate: endDate)
                        self.cartProductsArray = carts ?? []
                        self.bagsArray = bags ?? []
                        self.subTotal.text = last?.subtotal.stringValue
                        self.total = last?.total
                        self.VAT.text = "\(Double(truncating: last?.subtotal ?? 0) * 0.05)"
                        self.grandTotal.text = last?.total
                        self.city = last?.city
                        self.subTotal.textAlignment = .center
                        self.VAT.textAlignment = .center
                        self.grandTotal.textAlignment = .center
                        self.dtPicker = true
                        self.couponTextField.isEnabled = true
                        self.heightforTableView.constant = self.cartProductsArray.count == 1 ? CGFloat(160) : CGFloat(314)
                    } else {
                        self.cartProductsArray = carts ?? []
                        self.bagsArray = bags ?? []
                        self.subTotal.text = "SAR XXX"
                        self.VAT.text = "SAR XXX"
                        self.grandTotal.text = "SAR XXX"
                        self.subTotal.textAlignment = .center
                        self.VAT.textAlignment = .center
                        self.grandTotal.textAlignment = .center
                        self.txtDatePicker.text = Defaults.isArabicLanguage ? "اختر تاريخ التسليم" : "Select Delivery Date"
                        self.couponImage.isHidden = true
                        self.couponTextField.text = ""
                        self.dtPicker = false
                        self.couponTextField.isEnabled = false
                        self.heightforTableView.constant = CGFloat(0)
                    }
                    self.tableView.reloadData()
                    self.tableView.layoutIfNeeded()
                }
            }
        }
        
    }
    
    func engToArabic()
    {
        if Defaults.isArabicLanguage
        {
            
            //leftStackRight.constant = 0
           
//            stackView.alignment = .trailing
//             stackView.layoutIfNeeded()
            UILabel.appearance().font = UIFont(name: "Arabic", size: 12)
            orderLabel.text = "طلب"
            shippingChargesLabel.text = "الاسعار تشمل اجور الشحن"
            //   dateLb.text = "الأربعاء ، 10 أكتوبر ، 2018"
            billingAddLabel.text = "عنوان الفواتير"
            billingAddButton.setTitle("اختر العنوان", for: .normal)
            shippingAddLabel.text = "عنوان الشحن"
            ShippingAddButton.setTitle("اختر العنوان", for: .normal)
            deliveryDate.text = "تاريخ التسليم"
         //   deliveryDate.textAlignment = .right
            subTotalLabel.text = "المجموع"
            addCouponLabel.text = "كوبون"
            checkOutLabel.text = "طريقة الدفع"
            VatLabel.text = "الضريبة المضافة"
            grandTotalLabel.text =  "الإجمالي"
            checkOutButton.setImage(UIImage(named: "checkout1 copy copy"), for: .normal)
            checkOutButton.setImage(nil, for: .normal)
            checkOutButton.setTitle("دفع", for: .normal)
            checkOutButton.backgroundColor = UIColor(named: "DarkBlue")
            tytPaymentType.text = "نوع الدفع"
            couponTextField.placeholder = "ادخل رمز"
            couponTextField.textAlignment = .center
            tytPaymentType.textAlignment = .center    
        }
    }
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initViews()
        setupmenu()
        loginSession()
        engToArabic()
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == 1 {
            return false
        }
        return true
     }
    public func loginSession()
    {
        ApiCall.signin(email: UserDefaults.standard.value(forKey: "email") as! String, userPass: UserDefaults.standard.value(forKey: "password") as! String) { (user, error) in
            if error == nil  && user != nil {
                let loginUser : UserModel = user!
                print(loginUser.verificationStatus)
                
                // print("Verified")
                UserDefaults.standard.value(forKey: "password")
                UserDefaults.standard.value(forKey: "userId")
                UserDefaults.standard.value(forKey: "firstname")
                UserDefaults.standard.value(forKey: "lastname")
                UserDefaults.standard.value(forKey: "email")
                //                UserDefaults.standard.set(loginUser.id, forKey: "userId")
                //                UserDefaults.standard.set(loginUser.firstname, forKey: "firstname")
                //                UserDefaults.standard.set(loginUser.lastname, forKey: "lastname")
                //                UserDefaults.standard.set(loginUser.email, forKey: "email")
            }
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        //textField code
        
        self.couponTextField.resignFirstResponder()  //if desired
        ApiCall.redeemCoupon(coupon: self.couponTextField.text ?? "") { (msg, data, logout, error) in
            if error == nil {
                if logout! {
                    self.presentMain()
                } else {
                    if msg == "Coupun matched percent" {
                        self.couponImage.isHidden = false
                        ApiCall.shoppingCart(shippingId: self.shippingAddId?.converToString ?? "") { (carts, logout, startDate, endDate,bags,last, error) in
                            if error == nil {
                                if logout! {
                                    self.presentMain()
                                } else {
                                    if startDate != "Empty Cart" {
                                         self.city = last?.city
                                        self.subTotal.text = last?.subtotal.stringValue
                                   //     self.VAT.text = last?.Vat
                                      self.VAT.text = "\(Double(truncating: last?.subtotal ?? 0) * 0.05)"
                                        self.total = last?.total
                                        self.grandTotal.text = last?.total
                                        self.subTotal.textAlignment = .center
                                        self.VAT.textAlignment = .center
                                        self.grandTotal.textAlignment = .center
                                        
                                        
                                    }
                                }
                                
                            }
                            
                        }
                        
                    } else {
                        self.showAlert(title: Constants.MESSAGE, message: msg)
                    }
                }
            }
        }
        return true
    }
    func initViews() {
        showDatePicker()
        dateLb.text = self.getCurrentDate()
        pickerView.dataSource = self
        pickerView.delegate = self
        tytPaymentType.tag = 1
        self.couponImage.isHidden = true
        if Defaults.isArabicLanguage
        {
            txtDatePicker.text = "اختر تاريخ التسليم"
        }
        else
        {
        txtDatePicker.text = "Select Delivery Date"
        }
        txtDatePicker.textAlignment = .center
      //  txtDatePicker.isEnabled = false
        self.dtPicker = false
        couponTextField.isEnabled = false
        tytPaymentType.inputView = pickerView
        tytPaymentType.delegate = self
        self.couponTextField.delegate = self
    }
    
    func locationData(name: String,addressId: Int, location: String) {
        if name == "billing" {
            billingAddButton.setTitle(location, for: .normal)
            billingAddId = addressId
            //  billingAddButton.isEnabled = false
        } else if name == "shipping" {
            ShippingAddButton.setTitle(location, for: .normal)
            shippingAddId = addressId
            // ShippingAddButton.isEnabled = false
        }
//        else {

//        }
    }
    
    
    @objc func donedatePicker(){
       
            let formatter = DateFormatter()
            // formatter.dateFormat = "EEEE, MMM d, yyyy"
            formatter.dateFormat = "yyyy-MM-dd"
            txtDatePicker.text = formatter.string(from: datePicker.date)
            self.view.endEditing(true)
       

    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    func chckTrucks(){
        ApiCall.trucks { (trucks, logout, error) in
            if logout == true {
                self.presentMain()
            } else {
                if trucks != 0 {
                    if let tabItems = self.tabBarController?.tabBar.items {
                        let tabItem = tabItems[1]
                        tabItem.badgeValue = trucks?.converToString
                    }
                } else {
                    if let tabItems = self.tabBarController?.tabBar.items {
                        let tabItem = tabItems[1]
                        tabItem.badgeValue = nil
                    }
                }
             
            }
        }
    }
    func chckTrucks(orderId: Int, paymentId: Int){
        ApiCall.trucks { (trucks, logout, error) in
            if logout == true {
                self.presentMain()
            } else {
                if trucks != 0 {
                    if let tabItems = self.tabBarController?.tabBar.items {
                        let tabItem = tabItems[1]
                        tabItem.badgeValue = trucks?.converToString
                    }
                } else {
                    if let tabItems = self.tabBarController?.tabBar.items {
                        let tabItem = tabItems[1]
                        tabItem.badgeValue = nil
                    }
                }
                    let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                    let vc : BankTransferViewController = mainStoryboard.instantiateViewController(withIdentifier: "BankTransferViewController") as! BankTransferViewController
                    vc.orderId = orderId
                    vc.order = true
                    vc.amount = self.grandTotal.text?.converToDouble
                    vc.paymentId = paymentId
                    self.navigationController?.pushViewController(vc, animated: true)
                
            }
        }
    }
    
  
    func checkout() {
        var status : Int?
        if Defaults.isArabicLanguage {
            status = 1
        } else {
            status = 0
        }
        if (UserDefaults.standard.object(forKey: "City") == nil)
        {
            self.showAlert(title: "Error", message: "Select City First")
        }
        guard let city = UserDefaults.standard.object(forKey: "City") else {return}
        ApiCall.checkout(addressId: billingAddId!, type:  tytPaymentType.text!, shippingId: shippingAddId!, deliveryDate: txtDatePicker.text!,status: status!, orderCity: city as! String ) { (msg,paymentId,orderId, logout, error) in
            if logout! {
                self.presentMain()
            } else {
                if msg == "Cannot proceed with empty cart" {
                    var title = Constants.MESSAGE
                    var msg = "Cannot proceed with empty cart"
                    if Defaults.isArabicLanguage {
                        title = Constants.MESSAGE_AR
                        msg = "لا يمكن المتابعة مع عربة فارغة"
                    }
                    self.showAlert(title: title, message: msg)
                } else {
                    
                    self.chckTrucks(orderId: orderId!,paymentId: paymentId!)
                }
             
//                self.showAlert(title: "Message", message: msg, completion: {
//                    self.chckTrucks()
//                    self.tabBarController?.selectedIndex = 0
//                })
                
            }
        }
    }
    
    
    
    func checkout(Check: Bool) {
        var status : Int?
        if Defaults.isArabicLanguage {
            status = 1
        } else {
            status = 0
        }
        ApiCall.checkout(addressId: billingAddId!, type:  tytPaymentType.text!, shippingId: shippingAddId!, deliveryDate: txtDatePicker.text!,status: status!, orderCity: UserDefaults.standard.object(forKey: "City") as! String ) { (msg,paymentId,orderId, logout, error)  in
            if logout! {
                self.presentMain()
            } else {
                if Check {
                    if let tabItems = self.tabBarController?.tabBar.items {
                        let tabItem = tabItems[1]
                        tabItem.badgeValue = nil
                    }
                    self.switchScreen(identifier: "myorders")
            }
                
            }
        }
    }
    
    @IBAction func shippingAddressAction(_ sender: Any) {
        
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc : ShippingAddress3ViewController = mainStoryboard.instantiateViewController(withIdentifier: "ShippingAddress3ViewController") as! ShippingAddress3ViewController
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func billingAddressAction(_ sender: Any) {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc : BillingAddress3ViewController = mainStoryboard.instantiateViewController(withIdentifier: "BillingAddress3ViewController") as! BillingAddress3ViewController
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func sideMenuButtonTapped(_sender:UIButton)
    {
        present(SideMenuManager.default.menuRightNavigationController!, animated: true, completion: nil)
        
    }
    @IBAction func checkOutButtonTapped(_sender:UIButton)
    {
        
        if billingAddButton.titleLabel!.text != "CHOOSE ADDRESS" && ShippingAddButton.titleLabel!.text != "CHOOSE ADDRESS" && tytPaymentType.text != "  PAYMENT TYPE" && (txtDatePicker.text != "Select Delivery Date" && txtDatePicker.text != "اختر تاريخ التسليم") && billingAddId != nil && shippingAddId != nil {
            if tytPaymentType.text != "Credit\\Debit Card Payment" && tytPaymentType.text != "بطاقة الائتمانية" {
                bankTransfer()
            } else {
                onlineTransfer()
            }
        
           
        } else {
            var msg =  "Please fill out all information"
            var title = Constants.MESSAGE
            if Defaults.isArabicLanguage {
                msg = "يرجى ملء جميع المعلومات"
                title = Constants.MESSAGE_AR
            }
           self.showAlert(title: title, message: msg)
           
        }

    }
    func onlineTransfer() {
        sdkCall { (checkoutId) in
            self.checkoutID = checkoutId
   
            let checkoutSettings = OPPCheckoutSettings()
            
            // Set available payment brands for your shop
            checkoutSettings.paymentBrands = ["VISA", "MASTER","MADA"]
            
            if Defaults.isArabicLanguage {
            checkoutSettings.language = "ar"
            } else {
                 checkoutSettings.language = "en"
            }
            //    checkoutSettings.shopperResultURL = "hyperpay://unitedcement"
            let checkoutProvider = OPPCheckoutProvider(paymentProvider: self.provider, checkoutID: self.checkoutID!, settings: checkoutSettings)
            
            checkoutProvider?.presentCheckout(forSubmittingTransactionCompletionHandler: { (transaction, error) in
                
                guard transaction == transaction else {
                    // Handle invalid transaction, check error
                    return
                }
                self.transaction = transaction
                if error == nil {
                    if transaction?.type == .synchronous {
                        self.checkout(Check: true)
                    } else if transaction?.type == .asynchronous {
                        print("Async")
                        //                             NotificationCenter.default.addObserver(self, selector: #selector(self.didReceiveAsynchronousPaymentCallback), name: Notification.Name(rawValue: "AsyncPaymentCompletedNotificationKey"), object: nil)
                    } else {
                        print("Error")
                        // Executed in case of failure of the transaction for any reason
                    }
                    
                }
            }, cancelHandler: {
                // Executed if the shopper closes the payment page prematurely
            })

        }
    }
    
    func bankTransfer() {
        var msg = ""
        if Defaults.isArabicLanguage {
            msg = "بالنسبة للتحويل المصرفي ، يمكنك عرض التفاصيل المصرفية عن طريق الانتقال من القائمة الجانبية إلى الطلبات الخاصة بي إلى تفاصيل الطلب ثم المتابعة إلى الخروج. يرجى إيداع المبلغ في حسابنا المصرفي وإدخال المعلومات المطلوبة لطلبك لكي تتم معالجته"
        } else {
            msg = "For bank transfer, you can view bank details by navigating from Side Menu to My Orders Details and then Proceed to Checkout. Kindly deposit the amount in our Bank Account and enter required information for your order to be processed"
        }
        let alertController = UIAlertController(title: "Message", message: msg, preferredStyle: UIAlertController.Style.alert)

        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) {
            UIAlertAction in
            self.checkout()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) {
            UIAlertAction in
            //NSLog("Cancel Pressed")
        }
  
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true, completion: nil)
    }
    
    func getstatus(transaction:OPPTransaction) {
        
        let resourcepath = transaction.resourcePath?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        let url = String(format: "https://unitedcement.org/uc/oppwa/mobile-payment/notify?resourcePath=%@", transaction.resourcePath!.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
//        let url = "https://unitedcement.org/uc/oppwa/mobile-payment/notify/paymentStatus?resourcePath=%@\(resourcepath ?? "")"
        print(url)
        let merchantServerRequest = NSURLRequest(url: URL(string: url)!)
        URLSession.shared.dataTask(with: merchantServerRequest as URLRequest) { data, response, error in
            DispatchQueue.main.async {
                
                if let data = data {
                    if let json = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                    
                    print(json ?? "")
                    
                    
                    
                }
                }

            }
            }.resume()
        
    }
  
    
    @objc func didReceiveAsynchronousPaymentCallback() {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "AsyncPaymentCompletedNotificationKey"), object: nil)
      
    }

    
    func sdkCall(completion: @escaping (String)->()){
//        let amount = self.total
//        print(Int(self.total!.converToDouble))
        guard let amount = self.total else {return}
        
        let params = [
            "amount" : amount ,
            "email" : UserDefaults.standard.object(forKey: "email") as! String,
            "firstname" : UserDefaults.standard.object(forKey: "firstname") as! String ,
            "lastname" : UserDefaults.standard.object(forKey: "lastname") as! String,
            "billing.street1" : UserDefaults.standard.object(forKey: "Street1") as! String,
            "billing.city" : UserDefaults.standard.object(forKey: "City") as! String,
            "billing.state" : UserDefaults.standard.object(forKey: "State") as! String,
            "billing.country" : UserDefaults.standard.object(forKey: "Country") as! String,
            "billing.postcode" : UserDefaults.standard.object(forKey: "PostalCode") as! String,
            ] as [String: Any]
        let size = CGSize(width: 50, height:50)
        let presenter = PresenterViewControllerr()
        
        presenter.startAnimating(size, message: "Loading...", type: NVActivityIndicatorType(rawValue: 6)!)
        
        Alamofire.request("https://unitedcement.org/uc/oppwa/mobile-payment", method: .post, parameters: params, encoding: URLEncoding.httpBody, headers: nil)
            .responseJSON { response in
                presenter.stopAnimating()
                guard let data = response.result.value as? NSDictionary else {
                    return
                }
                guard let result = data["result"] as? [String:Any]  else{
                    return
                }
                if result["code"] as? String == "000.200.100" {
                    completion((data["id"] as? String)!)
                } else {
                    self.showAlert(title: Constants.MESSAGE, message: data["description"] as? String)
                }
                
              //  self.checkoutID = data["id"] as? String
                
        }
    }
    
}



extension OrderViewController : UITableViewDataSource,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cartProductsArray.count
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var quantities : [Int] = []
        var cell = tableView.dequeueReusableCell(withIdentifier: "productcell1") as! OrderTableViewCell
        if Defaults.isArabicLanguage
        {
          
           //  UILabel.appearance().font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle(rawValue: "Arabic")).withSize(16)
            cell.qtyLabelArabic.text = "الكمية"
            cell.quantityLb.textAlignment = .center
            cell.noOfTrucksArabic.text = "عدد الشاحنات"
            cell.freeBagsLb.text = "أكياس مجانية"
            cell.unitArbic.text = "سعر الوحدة"
            cell.totalArabic.text = "سعر الإجمالي"
            cell.updateCartButton.setImage(nil, for: .normal)
            cell.updateCartButton.setTitle("تحديث عربة", for: .normal)
                cell.updateCartButton.backgroundColor = UIColor(named: "Button")
            cell.bagArray =  self.bagsArray
            cell.totalPriceLb.text = "\(String(self.cartProductsArray[indexPath.row].price)) ر.س"
            if self.city == "Jeddah" {
                cell.unitPriceLb.text = "\(self.cartProductsArray[indexPath.row].item.price1.converToString ) ر.س"
            } else {
                cell.unitPriceLb.text = "\(self.cartProductsArray[indexPath.row].item.salePrice.converToString ) ر.س"
            }
            
            for item in  bagsArray  {
                quantities.append(item.quantity)
            }
            cell.cart = {
                let quantity = Int(cell.quantityLb.text ?? "") ?? 1
                let noOfTrucks = Int(cell.noOfTruckLb.text ?? "") ?? 1
                
                if cell.quantityLb.text != "Quantity" && cell.quantityLb.text != "" {
                    ApiCall.editCart(id: self.cartProductsArray[indexPath.row].item.id, quantity: noOfTrucks, bags: quantity,city: self.city ?? "", completion: { (msg, logout, error) in
                        if error == nil {
                            if logout! {
                                self.presentMain()
                            } else {
                                var msg = ""
                                var title = Constants.MESSAGE
                                if Defaults.isArabicLanguage {
                                    msg = "تحرير المنتج بنجاح"
                                    title = Constants.MESSAGE_AR
                                }
                                self.showAlert(title: title, message: msg, completion: {
                                    self.chckTrucks()
                                    ApiCall.shoppingCart(shippingId: self.shippingAddId?.converToString ?? "") { (carts, logout, startDate, endDate,bags,last, error) in
                                        if error == nil {
                                            if logout! {
                                                self.presentMain()
                                            } else {
                                                if startDate != "Empty Cart" {
                                                    self.datePicker.minimumDate = self.convertToDate(isoDate: endDate)
                                                    //                                                self.datePicker.maximumDate = self.convertToDate(isoDate: endDate)
                                                    self.cartProductsArray = carts ?? []
                                                    self.bagsArray = bags ?? []
                                                    self.city = last?.city
                                                    self.subTotal.text = last?.subtotal.stringValue
                                                //    self.VAT.text = last?.Vat
                                                     self.VAT.text = "\(Double(truncating: last?.subtotal ?? 0) * 0.05)"
                                                    self.grandTotal.text = last?.total
                                                    self.subTotal.textAlignment = .center
                                                    self.VAT.textAlignment = .center
                                                    
                                                    self.total = last?.total;   self.grandTotal.textAlignment = .center
                                                    self.tableView.reloadData()
                                                    self.tableView.layoutIfNeeded()
                                                    if self.cartProductsArray.count == 1 {
                                                        self.heightforTableView.constant = CGFloat(160)
                                                    } else {
                                                        self.heightforTableView.constant = CGFloat(314)
                                                    }
                                                } else {
                                                    self.tableView.reloadData()
                                                    self.tableView.layoutIfNeeded()
                                                    self.heightforTableView.constant = CGFloat(0)
                                                }
                                                
                                                
                                                
                                            }
                                            
                                        }
                                        
                                    }
                                })
                                
                            }
                        }
                    })
                } else {
                    var msg =  "Select Quantity"
                    var title = Constants.MESSAGE
                    if Defaults.isArabicLanguage {
                        msg = "اختر الكمية"
                        title = Constants.MESSAGE_AR
                    }
                    self.showAlert(title: title, message: msg)
                }
                
            }
            if let discountQuantity = self.cartProductsArray[indexPath.row].discountQuantity {
                cell.freeBagsLb.isHidden = false
                cell.freeBagsQuantityLb.isHidden = false
                cell.freeBagsQuantityLb.text = discountQuantity.converToString
            } else {
                cell.freeBagsLb.isHidden = true
                cell.freeBagsQuantityLb.isHidden = true
            }
            cell.minus = { num in
                if num > 1 {
                    cell.noOfTruckLb.text = String(num - 1)
                    let quantity = Int(cell.quantityLb.text ?? "") ?? 1
                    let noOfTrucks = Int(cell.noOfTruckLb.text ?? "") ?? 1
                    let unitPrice = self.cartProductsArray[indexPath.row].item.price1 ?? 1
               //     cell.totalPriceLb.text = "SAR \(String(quantity * noOfTrucks * unitPrice ))"
                      cell.totalPriceLb.text = "\(String(quantity * noOfTrucks * unitPrice )) ر.س"
                }
            }
            cell.plus = { num in
                cell.noOfTruckLb.text = String(num + 1)
                let quantity = Int(cell.quantityLb.text ?? "") ?? 1
                let noOfTrucks = Int(cell.noOfTruckLb.text ?? "") ?? 1
                let unitPrice = self.cartProductsArray[indexPath.row].item.price1 ?? 1
            //    cell.totalPriceLb.text = "SAR \(String(quantity * noOfTrucks * unitPrice ))"
                cell.totalPriceLb.text = "\(String(quantity * noOfTrucks * unitPrice )) ر.س"
            }
            cell.quantity = { quantity in
                let quantity = quantity
                let noOfTrucks = Int(cell.noOfTruckLb.text ?? "") ?? 1
                let unitPrice = self.cartProductsArray[indexPath.row].item.price1 ?? 1
                cell.totalPriceLb.text = "SAR \(String(quantity * noOfTrucks * unitPrice ))"
                
            }
            cell.quantityLb.text = self.cartProductsArray[indexPath.row].bags?.converToString
            cell.noOfTruckLb.text =  self.cartProductsArray[indexPath.row].trucks?.converToString
            
            cell.quantities = quantities
            cell.productImageLb?.sd_setImage(with: URL(string: "\(self.cartProductsArray[indexPath.row].item.images ?? "")"), placeholderImage: UIImage(named: "placeholder.png"))
            cell.productNameLb.text = self.cartProductsArray[indexPath.row].item.arabicName
            
            
            
            if self.cartProductsArray[indexPath.row].item.id == 33 {
                cell.cellView.backgroundColor = UIColor(named: "cell2")
            }
            else if self.cartProductsArray[indexPath.row].item.id == 32 {
                cell.cellView.backgroundColor = UIColor(named: "cell1")
            }
            else {
                cell.cellView.backgroundColor = UIColor(named: "cell3")
            }
            return cell
            
        }
        else
        {
        //     cell.productArray = productArray?[indexPath.row]
        cell.bagArray =  self.bagsArray
        if self.city == "Jeddah" {
            cell.unitPriceLb.text = "SAR \(self.cartProductsArray[indexPath.row].item.price1.converToString )"
        } else {
             cell.unitPriceLb.text = "SAR \(self.cartProductsArray[indexPath.row].item.salePrice.converToString )"
        }
        
        for item in  bagsArray  {
            quantities.append(item.quantity)
        }
        cell.cart = {
            let quantity = Int(cell.quantityLb.text ?? "") ?? 1
            let noOfTrucks = Int(cell.noOfTruckLb.text ?? "") ?? 1
            
            if cell.quantityLb.text != "Quantity" && cell.quantityLb.text != "" {
                ApiCall.editCart(id: self.cartProductsArray[indexPath.row].item.id, quantity: noOfTrucks, bags: quantity,city: self.city ?? "", completion: { (msg, logout, error) in
                    if error == nil {
                        if logout! {
                            self.presentMain()
                        } else {
                            self.showAlert(title: Constants.MESSAGE, message: msg, completion: {
                                self.chckTrucks()
                                ApiCall.shoppingCart(shippingId: self.shippingAddId?.converToString ?? "") { (carts, logout, startDate, endDate,bags,last, error) in
                                    if error == nil {
                                        if logout! {
                                            self.presentMain()
                                        } else {
                                            if startDate != "Empty Cart" {
                                                self.datePicker.minimumDate = self.convertToDate(isoDate: endDate)
//                                                self.datePicker.maximumDate = self.convertToDate(isoDate: endDate)
                                                self.cartProductsArray = carts ?? []
                                                self.bagsArray = bags ?? []
                                                self.city = last?.city
                                                self.subTotal.text = last?.subtotal.stringValue
                                               // self.VAT.text = last?.Vat
                                                 self.VAT.text = "\(Double(truncating: last?.subtotal ?? 0) * 0.05)"
                                                self.grandTotal.text = last?.total
                                                self.subTotal.textAlignment = .center
                                                self.VAT.textAlignment = .center
                                               
                                                self.total = last?.total;   self.grandTotal.textAlignment = .center
                                                self.tableView.reloadData()
                                                self.tableView.layoutIfNeeded()
                                                if self.cartProductsArray.count == 1 {
                                                    self.heightforTableView.constant = CGFloat(160)
                                                } else {
                                                    self.heightforTableView.constant = CGFloat(314)
                                                }
                                            } else {
                                                self.tableView.reloadData()
                                                self.tableView.layoutIfNeeded()
                                                self.heightforTableView.constant = CGFloat(0)
                                            }
                                            
                                            
                                            
                                        }
                                        
                                    }
                                    
                                }
                            })
                            
                        }
                    }
                })
            } else {
                var msg =  "Select Quantity"
                var title = Constants.MESSAGE
                if Defaults.isArabicLanguage {
                    msg = "اختر الكمية"
                    title = Constants.MESSAGE_AR
                }
                self.showAlert(title: title, message: msg)
            }
            
        }
        if let discountQuantity = self.cartProductsArray[indexPath.row].discountQuantity {
            cell.freeBagsLb.isHidden = false
            cell.freeBagsQuantityLb.isHidden = false
            cell.freeBagsQuantityLb.text = discountQuantity.converToString
        } else {
            cell.freeBagsLb.isHidden = true
            cell.freeBagsQuantityLb.isHidden = true
        }
        cell.minus = { num in
            if num > 1 {
                cell.noOfTruckLb.text = String(num - 1)
                let quantity = Int(cell.quantityLb.text ?? "") ?? 1
                let noOfTrucks = Int(cell.noOfTruckLb.text ?? "") ?? 1
                let unitPrice = self.cartProductsArray[indexPath.row].item.price1 ?? 1
                cell.totalPriceLb.text = "SAR \(String(quantity * noOfTrucks * unitPrice ))"
            }
        }
        cell.plus = { num in
            cell.noOfTruckLb.text = String(num + 1)
            let quantity = Int(cell.quantityLb.text ?? "") ?? 1
            let noOfTrucks = Int(cell.noOfTruckLb.text ?? "") ?? 1
            let unitPrice = self.cartProductsArray[indexPath.row].item.price1 ?? 1
            cell.totalPriceLb.text = "SAR \(String(quantity * noOfTrucks * unitPrice ))"
        }
        cell.quantity = { quantity in
            let quantity = quantity
            let noOfTrucks = Int(cell.noOfTruckLb.text ?? "") ?? 1
            let unitPrice = self.cartProductsArray[indexPath.row].item.price1 ?? 1
            cell.totalPriceLb.text = "SAR \(String(quantity * noOfTrucks * unitPrice ))"
            
        }
        cell.quantityLb.text = self.cartProductsArray[indexPath.row].bags?.converToString
        cell.noOfTruckLb.text =  self.cartProductsArray[indexPath.row].trucks?.converToString
        cell.totalPriceLb.text = "SAR \(String(self.cartProductsArray[indexPath.row].price))"
        cell.quantities = quantities
        cell.productImageLb?.sd_setImage(with: URL(string: "\(self.cartProductsArray[indexPath.row].item.images ?? "")"), placeholderImage: UIImage(named: "placeholder.png"))
        cell.productNameLb.text = self.cartProductsArray[indexPath.row].item.name
        
        
        
        if self.cartProductsArray[indexPath.row].item.id == 33 {
            cell.cellView.backgroundColor = UIColor(named: "cell2")
        }
        else if self.cartProductsArray[indexPath.row].item.id == 32 {
            cell.cellView.backgroundColor = UIColor(named: "cell1")
        }
        else {
            cell.cellView.backgroundColor = UIColor(named: "cell3")
        }
        return cell
        
    }
}
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
        
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            //self.tableArray.remove(at: indexPath.row)
            // print("Deleted \(indexPath.row)")
            ApiCall.deleteFromCart(id: self.cartProductsArray[indexPath.row].item.id) { (msg, logout, error) in
                if logout! {
                    self.presentMain()
                } else {
                    var msg =  "Item Deleted"
                    var title = Constants.MESSAGE
                    if Defaults.isArabicLanguage {
                        msg = "تم حذف العنصر"
                        title = Constants.MESSAGE_AR
                    }
                    self.showAlert(title: title, message: msg, completion: {
                        self.chckTrucks()
                        self.cartProductsArray.remove(at: indexPath.row)
                        self.tableView.reloadData()
                        self.tableView.layoutIfNeeded()
                        self.heightforTableView.constant = self.heightforTableView.constant - CGFloat(160)
                    })
                }
            }
            // tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
}

extension OrderViewController : UIPickerViewDelegate,UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return types.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if Defaults.isArabicLanguage
        {
            return typesArabic[row]
        }
        else
        {
        return types[row]
    }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if Defaults.isArabicLanguage
        {
            tytPaymentType.text = typesArabic[row]
        }
        else
        {
           tytPaymentType.text = types[row]
        }
       
        self.view.endEditing(false)
        
    }
    
}

extension OrderViewController {
    func showDatePicker(){
        //Formate Date
       
        datePicker.datePickerMode = .date
//            datePicker.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
            //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        txtDatePicker.inputAccessoryView = toolbar
        txtDatePicker.inputView = datePicker

        
    }
    
//    @objc func dateChanged(_ sender: UIDatePicker) {
//        if self.dtPicker == true {
//                    } else {
//            self.showAlert(title: Constants.MESSAGE, message: "No products exist")
//        }
//    }
}
