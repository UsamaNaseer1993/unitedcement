//
//  AppConstansts.swift
//  UnitedCement
//
//  Created by Usama Naseer on 18/11/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import Foundation

let KInternetCheck = "Please Check Your Internet Connection"
let KLanguageKey = "language"
var KLanguageSelect: Int  {
    get{
        return UserDefaults.standard.integer(forKey: KLanguageKey)
    }
}
let KMissingParams = "Please fill all fields"
let KEmailExist = "Email id already exists"
let KEmailOrPass = "Email or Password is incorrect"
let KFieldsNotEmpty =  "Fields cannot be empty"
let KEnterValidEmail = "Please enter valid email address"
let KMatchpass = "Password didn't match"
let KCodeSent = "Your code has been sent to specified email address"
let KImageUrl = "https://jeddah.space/cement/uploads/images/medium/"

