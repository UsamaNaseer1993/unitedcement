//
//  UserModel.swift
//  UnitedCement
//
//  Created by Usama Naseer on 19/11/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import Foundation

class UserModel {
    var email : String!
    var firstname : String!
    var id : Int!
    var lastname : String!
    var password : String!
    var phone : String!
    var verificationCode : String!
    var verificationStatus : String!
    

    init(fromDictionary dictionary: NSDictionary){
        email = dictionary["email"] as? String
        firstname = dictionary["firstname"] as? String
        id = dictionary["id"] as? Int
        lastname = dictionary["lastname"] as? String
        password = dictionary["password"] as? String
        phone = dictionary["phone"] as? String
        verificationCode = dictionary["verification_code"] as? String
        verificationStatus = dictionary["verification_status"] as? String
    }
}
