//
//  AddressModel.swift
//  UnitedCement
//
//  Created by Usama Naseer on 08/12/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import Foundation

class AddressModel {
    var address1 : String!
    var address2 : String!
    var addressDesc : String!
    var city : String!
    var country : String!
    var countryId : Int!
    var id : Int!
    var latitude : String!
    var longitude : String!
    

    init(fromDictionary dictionary: NSDictionary){
        address1 = dictionary["address1"] as? String
        addressDesc = dictionary["addressDesc"] as? String
        city = dictionary["city"] as? String
        country = dictionary["country"] as? String
        countryId = dictionary["country_id"] as? Int
        id = dictionary["id"] as? Int
        latitude = dictionary["latitude"] as? String
        longitude = dictionary["longitude"] as? String
        address2 = dictionary["address2"] as? String
    }
    
    static func map(dictionary:[NSDictionary]) -> [AddressModel] {
        var arrayAddress : [AddressModel] = []
        for dic in dictionary {
            arrayAddress.append(AddressModel(fromDictionary: dic))
        }
        return arrayAddress
    }
}
