//
//  ItemModel.swift
//  UnitedCement
//
//  Created by Usama Naseer on 12/12/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import Foundation

class ItemModel {
    
    var arabicDescription : String!
    var arabicName : String!
    var descriptionField : String!
    var id : Int!
    var images : String!
    var name : String!
    var price1 : Int!
    var salePrice : Int!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: NSDictionary){
        arabicDescription = dictionary["arabic_description"] as? String
        arabicName = dictionary["arabic_name"] as? String
        descriptionField = dictionary["description"] as? String
        id = dictionary["id"] as? Int
        images = dictionary["images"] as? String
        name = dictionary["name"] as? String
        price1 = dictionary["price_1"] as? Int
        salePrice = dictionary["saleprice_1"] as? Int
    }

}
