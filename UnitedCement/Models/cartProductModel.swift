//
//  cartProductModel.swift
//  UnitedCement
//
//  Created by Usama Naseer on 12/12/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import Foundation

class cartProductModel {
    
    var item : ItemModel!
    var price : Int!
    var qty : Int!
    var totalQuantity : Int!
    var bags : Int!
    var trucks : Int!
    var type : String!
    var discountQuantity : Int!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: NSDictionary){
        price = dictionary["price"] as? Int
        qty = dictionary["qty"] as? Int
        totalQuantity = dictionary["total_quantity"] as? Int
        discountQuantity = dictionary["discount_quantity"] as? Int
        type = dictionary["type"] as? String
        bags = dictionary["bags"] as? Int
        trucks = dictionary["trucks"] as? Int
        if let itemData = dictionary["item"] as? NSDictionary{
            item = ItemModel(fromDictionary: itemData)
        }
    }
    static func map(cartProducts : [NSDictionary]) -> [cartProductModel] {
        var cartProductsArray : [cartProductModel] = []
        for cartProduct in cartProducts {
    cartProductsArray.append(cartProductModel(fromDictionary: cartProduct))
        }
        return cartProductsArray
    }
    
}
