//
//  OrderDetailModel.swift
//  UnitedCement
//
//  Created by Usama Naseer on 19/12/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import Foundation

class OrderDetailModel {
    var order_status : String!
    var arabic_status : String!
    var address : String!
    var arabicDescription : String!
    var arabicName : String!
    var billingAddressId : Int!
    var descriptionField : String!
    var grandTotal : Double!
    var images : String!
    var name : String!
    var orderDeliveryDate : String!
    var orderNumber : String!
    var orderStarted : String!
    var productPrice : Int!
    var productQuantity : Int!
    var productTotal : Int!
    var shippingAddressId : Int!
    var shippingAmount : Int!
    var subtotal : Double!
    var type : String!
    var vAT : Int!
    var trucks,bags : Int!
    var paymentId : Int!
    var coupon: String?
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: NSDictionary){
        address = dictionary["address"] as? String
        coupon = dictionary["coupon"] as? String
        arabicDescription = dictionary["arabic_description"] as? String
        arabicName = dictionary["arabic_name"] as? String
        billingAddressId = dictionary["billing_address_id"] as? Int
        descriptionField = dictionary["description"] as? String
        grandTotal = dictionary["Grand_total"] as? Double
        images = dictionary["images"] as? String
        name = dictionary["name"] as? String
        orderDeliveryDate = dictionary["order_delivery_date"] as? String
        orderNumber = dictionary["order_number"] as? String
        orderStarted = dictionary["order_started"] as? String
        productPrice = dictionary["product_price"] as? Int
        productQuantity = dictionary["product_quantity"] as? Int
        productTotal = dictionary["product_total"] as? Int
        shippingAddressId = dictionary["shipping_address_id"] as? Int
        shippingAmount = dictionary["ShippingAmount"] as? Int
        subtotal = dictionary["subtotal"] as? Double
        type = dictionary["Type"] as? String
        vAT = dictionary["VAT"] as? Int
        trucks = dictionary["trucks"] as? Int
        bags = dictionary["bags"] as? Int
        paymentId = dictionary["payment_id"] as? Int
        order_status = dictionary["order_status"] as? String
        arabic_status = dictionary["arabic_status"] as? String
    }
    static func map(cartProducts : [NSDictionary]) -> [OrderDetailModel] {
        var cartProductsArray : [OrderDetailModel] = []
        for cartProduct in cartProducts {
            cartProductsArray.append(OrderDetailModel(fromDictionary: cartProduct))
        }
        return cartProductsArray
    }
}
