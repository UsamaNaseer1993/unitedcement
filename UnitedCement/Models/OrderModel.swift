//
//  OrderModel.swift
//  UnitedCement
//
//  Created by Usama Naseer on 17/12/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import Foundation
class OrderModel {
    var id : Int!
    var orderAmount : Double!
    var orderNumber : String!
    var orderStarted : String!
    var orderStatus : String!
    var orderStatus_Ar : String!
    var status : String!
    
    
    init(fromDictionary dictionary: NSDictionary){
        id = dictionary["id"] as? Int
        orderAmount = dictionary["order_amount"] as? Double
        orderNumber = dictionary["order_number"] as? String
        orderStarted = dictionary["order_started"] as? String
        orderStatus = dictionary["order_status"] as? String
        status = dictionary["status"] as? String
        orderStatus_Ar = dictionary["arabic_status"] as? String
    }
    
    static func map(orders : [NSDictionary]) -> [OrderModel] {
        var orderArray : [OrderModel] = []
        for order in orders {
            orderArray.append(OrderModel(fromDictionary: order))
        }
        return orderArray
    }
}
