//
//  bankDetailModel.swift
//  UnitedCement
//
//  Created by Usama Naseer on 25/12/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import Foundation

class bankDetailModel {
    
    var accountName : String!
    var accountName_Ar: String!
    var bankName_Ar: String!
    var accountNumber : String!
    var bankName : String!
    var iban : String!
    
    init(fromDictionary dictionary: [String:Any]){
        accountName = dictionary["account_name"] as? String
        accountName_Ar = dictionary["account_name_ar"] as? String
        bankName_Ar = dictionary["bank_name_ar"] as? String
        accountNumber = dictionary["account_number"] as? String
        bankName = dictionary["bank_name"] as? String
        iban = dictionary["iban"] as? String
    }
    static func map(orders : [[String:Any]]) -> [bankDetailModel] {
        var orderArray : [bankDetailModel] = []
        for order in orders {
            orderArray.append(bankDetailModel(fromDictionary: order))
        }
        return orderArray
    }

}
