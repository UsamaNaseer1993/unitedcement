//
//  File.swift
//  UnitedCement
//
//  Created by Usama Naseer on 13/12/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import Foundation

class CouponModel {
    
    var code : String!
    var reductionAmount : Int!
    var reductionType : String!
    
    init(fromDictionary dictionary: NSDictionary){
        code = dictionary["code"] as? String
        reductionAmount = dictionary["reduction_amount"] as? Int
        reductionType = dictionary["reduction_type"] as? String
    }
    
    static func map(coupons : [NSDictionary]) -> [CouponModel] {
        var couponArray : [CouponModel] = []
        for coupon in coupons {
            couponArray.append(CouponModel(fromDictionary: coupon))
        }
        return couponArray
    }
}
