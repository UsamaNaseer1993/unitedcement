//
//  OfferModel.swift
//  UnitedCement
//
//  Created by Usama Naseer on 16/12/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import Foundation

class OfferModel  {
    
    var actualPrice : Int!
    var arabicDescription : String!
    var arabicImages : String!
    var arabicName : String!
    var days : Int!
    var descriptionField : String!
    var discountPrice : Int!
    var makkahPrice : Int!
    var discountQuantity : Int!
    var hours : Int!
    var id : Int!
    var images : String!
    var minimumQuantity : Int!
    var minutes : Int!
    var name : String!
    var quantity : Int!
    var reductionPercentage : String!
    var seconds : Int!
    var slug : String!
    var jazanPrice : Int!

    init(fromDictionary dictionary: NSDictionary){
        actualPrice = dictionary["actual_price"] as? Int
        arabicDescription = dictionary["arabic_description"] as? String
        arabicImages = dictionary["arabic_images"] as? String
        arabicName = dictionary["arabic_name"] as? String
        days = dictionary["days"] as? Int
        descriptionField = dictionary["description"] as? String
        discountPrice = dictionary["discount_price"] as? Int
        makkahPrice = dictionary["makkah_price"] as? Int
        
        discountQuantity = dictionary["discount_quantity"] as? Int
        hours = dictionary["Hours"] as? Int
        id = dictionary["id"] as? Int
        images = dictionary["images"] as? String
        minimumQuantity = dictionary["minimum_quantity"] as? Int
        minutes = dictionary["Minutes"] as? Int
        name = dictionary["name"] as? String
        quantity = dictionary["quantity"] as? Int
        reductionPercentage = dictionary["Reduction_Percentage"] as? String
        seconds = dictionary["seconds"] as? Int
        slug = dictionary["slug"] as? String
        jazanPrice = dictionary["jazanprice_1"] as? Int
    }
    static func map(offers : [[NSDictionary]]) -> [OfferModel] {
        var offersArray : [OfferModel] = []
     //   var offerDictionary : [[OfferModel]] = []
        for offer in offers {
            for offermodel in offer{
                offersArray.append(OfferModel(fromDictionary: offermodel))
            }
        }
       
        return offersArray
    }
    
    //static func map(offers :   [NSDictionary]) ->

}
