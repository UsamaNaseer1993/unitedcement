//
//  ProductModel.swift
//  UnitedCement
//
//  Created by Usama Naseer on 14/11/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import Foundation

class ProductModel {
    
    var adminId : Int!
    var arabicDescription : String!
    var arabicImages : String!
    var arabicName : String!
    var brand : String!
    var descriptionField : String!
    var disableDate : String!
    var enableDate : String!
    var enabled1 : Int!
    var excerpt : String!
    var fixedQuantity : Int!
    var freeShipping : String!
    var id : Int!
    var images : String!
    var isGiftcard : Int!
    var meta : String!
    var model : String!
    var name : String!
    var price1 : Double!
    var jazanPrice : Double!
    var primaryCategory : Int!
    var quantity : Int!
    var relatedProducts : String!
    var saleprice1 : Double!
    var secondaryCategory : Int!
    var seoTitle : String!
    var sequence : String!
    var shippable : Int!
    var sku : String!
    var slug : String!
    var taxable : Int!
    var trackStock : Int!
    var weight : String!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: NSDictionary){
        adminId = dictionary["admin_id"] as? Int
        arabicDescription = dictionary["arabic_description"] as? String
        arabicImages = dictionary["arabic_images"] as? String
        arabicName = dictionary["arabic_name"] as? String
        brand = dictionary["brand"] as? String
        descriptionField = dictionary["description"] as? String
        disableDate = dictionary["disable_date"] as? String
        enableDate = dictionary["enable_date"] as? String
        enabled1 = dictionary["enabled_1"] as? Int
        excerpt = dictionary["excerpt"] as? String
        fixedQuantity = dictionary["fixed_quantity"] as? Int
        freeShipping = dictionary["free_shipping"] as? String
        id = dictionary["id"] as? Int
        images = dictionary["images"] as? String
        isGiftcard = dictionary["is_giftcard"] as? Int
        meta = dictionary["meta"] as? String
        model = dictionary["model"] as? String
        name = dictionary["name"] as? String
        price1 = dictionary["price_1"] as? Double
        primaryCategory = dictionary["primary_category"] as? Int
        quantity = dictionary["quantity"] as? Int
        relatedProducts = dictionary["related_products"] as? String
        saleprice1 = dictionary["saleprice_1"] as? Double
        secondaryCategory = dictionary["secondary_category"] as? Int
        seoTitle = dictionary["seo_title"] as? String
        sequence = dictionary["sequence"] as? String
        shippable = dictionary["shippable"] as? Int
        sku = dictionary["sku"] as? String
        slug = dictionary["slug"] as? String
        taxable = dictionary["taxable"] as? Int
        trackStock = dictionary["track_stock"] as? Int
        weight = dictionary["weight"] as? String
        jazanPrice = dictionary["jazanprice_1"] as? Double
    }
    static func map(products : [NSDictionary]) -> [ProductModel] {
        var productsArray : [ProductModel] = []
        for product in products {
            
            let product : ProductModel = ProductModel(fromDictionary: product)
            productsArray.append(product)
        }
        return productsArray
    }

}
