//
//  ImageModel.swift
//  UnitedCement
//
//  Created by Usama Naseer on 07/12/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import Foundation
class ImageModel {
    var image : String!
    var arabicImage : String!

    init(fromDictionary dictionary: NSDictionary){
        image = dictionary["image"] as? String
        arabicImage = dictionary["arabic_image"] as? String
        
    }
    
    static func map(images : [NSDictionary]) -> Array<ImageModel> {
        var imageArray : Array<ImageModel> = []
        for image in images {
            
            let image : ImageModel = ImageModel(fromDictionary: image)
            imageArray.append(image)
        }
        return imageArray
    }
    
}

