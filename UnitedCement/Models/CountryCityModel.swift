//
//  CountryCityModel.swift
//  UnitedCement
//
//  Created by Usama Naseer on 19/11/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import Foundation

class CountryCityModel {

var city : Array<City> = []
var country : Country?

init(fromDictionary dictionary: NSDictionary){
    if let countryData = dictionary["Country"] as? NSDictionary{
        country = Country(fromDictionary: countryData)
    }
    if let cityArray = dictionary["City"] as? [NSDictionary]{
        for dic in cityArray{
            let value = City(fromDictionary: dic)
            city.append(value)
        }
    }
}
    static func map(conCityDictionary : [NSDictionary]) -> [CountryCityModel] {
        var concityArray : [CountryCityModel] = []
        for conCityModel in conCityDictionary {
            let conCity  = CountryCityModel(fromDictionary: conCityModel)
            concityArray.append(conCity)
        }
        return concityArray
    }
    
}

class Country {
    var id : Int!
    var name : String!
    var tax : Int!
    var arabicName : String!
    
    init(fromDictionary dictionary: NSDictionary){
        id = dictionary["id"] as? Int
        name = dictionary["name"] as? String
        tax = dictionary["tax"] as? Int
        arabicName = dictionary["arabic_name"] as? String
    }
    
    static func map(country : Country) -> [String] {
        var countryString: [String] = []
        countryString.append(country.name)
        return countryString
    }
}

class City{
    var name : String!
    var tax : Int!
    var arabicName : String!
    
    init(fromDictionary dictionary: NSDictionary){
        name = dictionary["name"] as? String
        tax = dictionary["tax"] as? Int
        arabicName = dictionary["arabic_name"] as? String
    }
    static func map(cities: [City]) -> [String] {
        var citiesString: [String] = []
        for city in cities {
         citiesString.append(city.name)
        }
        return citiesString
    }
}
