//
//  ProfileModel.swift
//  UnitedCement
//
//  Created by Usama Naseer on 18/12/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import Foundation

class ProfileModel {
    
    var email : String!
    var firstname : String!
    var id : Int!
    var lastname : String!
    var phone : String!
    var company : String!
    
    init(fromDictionary dictionary: NSDictionary){
        email = dictionary["email"] as? String
        firstname = dictionary["firstname"] as? String
        id = dictionary["id"] as? Int
        lastname = dictionary["lastname"] as? String
        phone = dictionary["phone"] as? String
        company = dictionary["company"] as? String
    }
    
    static func map(profiles : [NSDictionary]) -> [ProfileModel] {
        var profileArray : [ProfileModel] = []
        for profile in profiles {
            profileArray.append(ProfileModel(fromDictionary: profile))
        }
        return profileArray
    }
}
