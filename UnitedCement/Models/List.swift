//
//  List.swift
//
//  Created by Muhammad Irfan on 5/31/19
//  Copyright (c) . All rights reserved.
//

import Foundation

public class List {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let city = "City"
    static let country = "Country"
  }

  // MARK: Properties
  public var city: [City]?
  public var country: Country?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    if let items = json[SerializationKeys.city].array { city = items.map { City(json: $0) } }
    country = Country(json: json[SerializationKeys.country])
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = city { dictionary[SerializationKeys.city] = value.map { $0.dictionaryRepresentation() } }
    if let value = country { dictionary[SerializationKeys.country] = value.dictionaryRepresentation() }
    return dictionary
  }

}
