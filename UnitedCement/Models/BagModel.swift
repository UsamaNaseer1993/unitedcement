//
//  BagModel.swift
//  UnitedCement
//
//  Created by Usama Naseer on 14/11/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import Foundation
import SwiftyJSON


class BagModel {
    var id : Int!
    var quantity : Int!
    var status : Int!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: NSDictionary){
        id = dictionary["id"] as? Int
        quantity = dictionary["quantity"] as? Int
        status = dictionary["status"] as? Int
    }
    
   static func map(bags : [NSDictionary]) -> Array<BagModel> {
        var bagsArray : Array<BagModel> = []
        for bag in bags {
            
            let Bag : BagModel = BagModel(fromDictionary: bag)
            bagsArray.append(Bag)
        }
        return bagsArray
    }

}
