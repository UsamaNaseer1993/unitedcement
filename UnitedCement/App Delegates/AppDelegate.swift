//
//  AppDelegate.swift
//  UnitedCement
//
//  Created by Usama Naseer on 16/10/2018.
//  Copyright © 2018 Usama Naseer. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
let apikey = "AIzaSyA_DRXDicRfkm1f27bstel-Z3cHhzl6tq8"

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
      GMSServices.provideAPIKey(apikey)
      GMSPlacesClient.provideAPIKey(apikey)

        // Override point for customization after application launch.
        self.window = UIWindow(frame: UIScreen.main.bounds)
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        UILabel.appearance().font = UIFont.preferredFont(forTextStyle: UIFont.TextStyle(rawValue: "Arabic")).withSize(12)

 
        
        if UserDefaults.standard.integer(forKey: "userId") == 0 {
                 let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "LanguageSelectViewController") as UIViewController
            let navigationController = UINavigationController.init(rootViewController: vc)
            self.window?.rootViewController = navigationController
        }
        else {
            if Defaults.isArabicLanguage {
                UIView.appearance().semanticContentAttribute = .forceRightToLeft
            } else {
                UIView.appearance().semanticContentAttribute = .forceLeftToRight
            }
            let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "TabbarViewController") as! UITabBarController
            let navigationController = UINavigationController.init(rootViewController: vc)
            self.window?.rootViewController = navigationController
        }
   
//         let navigationController = UINavigationController.init(rootViewController: vc)
//        self.window?.rootViewController = navigationController
        return true
     
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        
        print("urlscheme:" + (url.scheme)!)
        
        var handler:Bool = false
        
        
        if url.scheme?.caseInsensitiveCompare("hyperpay") == .orderedSame {
//              let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
//            let vc : UIViewController = mainStoryboard.instantiateViewController(withIdentifier: "TabbarViewController") as! UITabBarController
//            vc.tabBarController?.selectedIndex = 1
//            let navigationController = UINavigationController.init(rootViewController: vc)
//            self.window?.rootViewController = navigationController
//            NotificationCenter.default.post(name: Notification.Name(rawValue: "AsyncPaymentCompletedNotificationKey"), object: nil)
            UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: true, completion: {
                DispatchQueue.main.async {
                    //self.getstatus(transaction: self.transaction!)
                    //  self.checkout(Check: true)
                    
                }
            })
           
            handler = true
        }
        
        
        return handler
    }
}

